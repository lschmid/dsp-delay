-- Generated from Simulink block dsp_delay_struct
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity dsp_delay_struct is
  port (
    data_i : in std_logic_vector( 30-1 downto 0 );
    rst_i : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    data_direct_globalen_o : out std_logic_vector( 48-1 downto 0 );
    data_direct_indven_o : out std_logic_vector( 48-1 downto 0 );
    data_macro_globalen_o : out std_logic_vector( 34-1 downto 0 );
    data_macro_indven_o : out std_logic_vector( 34-1 downto 0 )
  );
end dsp_delay_struct;
architecture structural of dsp_delay_struct is 
  signal delay1_q_net : std_logic_vector( 48-1 downto 0 );
  signal dsp48_macro_3_0_1_p_net : std_logic_vector( 34-1 downto 0 );
  signal rst_i_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal delay_q_net : std_logic_vector( 48-1 downto 0 );
  signal data_i_net : std_logic_vector( 30-1 downto 0 );
  signal dsp48_macro_3_0_p_net : std_logic_vector( 34-1 downto 0 );
  signal constant_op_net : std_logic_vector( 18-1 downto 0 );
  signal unsignedtobool_dout_net : std_logic;
  signal ce_net : std_logic;
  signal convert_dout_net : std_logic_vector( 16-1 downto 0 );
  signal convert1_dout_net : std_logic_vector( 16-1 downto 0 );
  signal dsp48e2_2_p_net : std_logic_vector( 48-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 48-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 18-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 48-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 30-1 downto 0 );
  signal enable_toggling_op_net : std_logic_vector( 1-1 downto 0 );
  signal opmode_op_net : std_logic_vector( 22-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 30-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 18-1 downto 0 );
  signal dsp48e2_1_p_net : std_logic_vector( 48-1 downto 0 );
begin
  data_direct_globalen_o <= delay_q_net;
  data_direct_indven_o <= delay1_q_net;
  data_i_net <= data_i;
  data_macro_globalen_o <= dsp48_macro_3_0_p_net;
  data_macro_indven_o <= dsp48_macro_3_0_1_p_net;
  rst_i_net <= rst_i;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant_x0 : entity xil_defaultlib.sysgen_constant_f6cb58e566 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  convert : entity xil_defaultlib.dsp_delay_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 26,
    din_width => 30,
    dout_arith => 2,
    dout_bin_pt => 15,
    dout_width => 16,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => data_i_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net
  );
  convert1 : entity xil_defaultlib.dsp_delay_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 26,
    din_width => 30,
    dout_arith => 2,
    dout_bin_pt => 15,
    dout_width => 16,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => data_i_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert1_dout_net
  );
  dsp48_macro_3_0 : entity xil_defaultlib.xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e 
  port map (
    a => convert_dout_net,
    b => constant_op_net,
    rst => rst_i_net(0),
    en => unsignedtobool_dout_net,
    clk => clk_net,
    ce => ce_net,
    p => dsp48_macro_3_0_p_net
  );
  dsp48_macro_3_0_1 : entity xil_defaultlib.xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2 
  port map (
    a => convert1_dout_net,
    b => constant_op_net,
    cea3 => unsignedtobool_dout_net,
    cea4 => unsignedtobool_dout_net,
    ceb3 => unsignedtobool_dout_net,
    ceb4 => unsignedtobool_dout_net,
    cem => unsignedtobool_dout_net,
    cep => unsignedtobool_dout_net,
    rst => rst_i_net(0),
    clk => clk_net,
    ce => ce_net,
    p => dsp48_macro_3_0_1_p_net
  );
  dsp48e2_1 : entity xil_defaultlib.dsp_delay_xldsp48e2 
  generic map (
    a_input => "DIRECT",
    acascreg => 1,
    adreg => 0,
    alumodereg => 1,
    amultsel => "A",
    areg => 1,
    autoreset_pattern_detect => "NO_RESET",
    autoreset_priority => "RESET",
    b_input => "DIRECT",
    bcascreg => 1,
    bmultsel => "B",
    breg => 1,
    carryinreg => 1,
    carryinselreg => 1,
    carryout_width => 4,
    creg => 0,
    dreg => 0,
    inmodereg => 0,
    is_alumode_inverted => "0000",
    is_carryin_inverted => '0',
    is_clk_inverted => '0',
    is_inmode_inverted => "00000",
    is_opmode_inverted => "000000000",
    is_rsta_inverted => '0',
    is_rstallcarryin_inverted => '0',
    is_rstalumode_inverted => '0',
    is_rstb_inverted => '0',
    is_rstc_inverted => '0',
    is_rstctrl_inverted => '0',
    is_rstd_inverted => '0',
    is_rstinmode_inverted => '0',
    is_rstm_inverted => '0',
    is_rstp_inverted => '0',
    mreg => 1,
    opmodereg => 1,
    preaddinsel => "A",
    preg => 1,
    rnd => X"000000000000",
    sel_mask => "C",
    sel_pattern => "C",
    use_c_port => 0,
    use_mult => "MULTIPLY",
    use_op => 1,
    use_pattern_detect => "NO_PATDET",
    use_simd => "ONE48",
    use_widexor => "FALSE",
    xorsimd => "XOR12"
  )
  port map (
    carryin => "0",
    cea1 => "1",
    cea2 => "1",
    ceb1 => "1",
    ceb2 => "1",
    cec => "1",
    cem => "1",
    cealumode => "1",
    cemultcarryin => "1",
    cectrl => "1",
    cecarryin => "1",
    cep => "1",
    ced => "1",
    cead => "1",
    ceinmode => "1",
    alumode => "0000",
    rsta => "0",
    rstb => "0",
    rstc => "0",
    rstm => "0",
    rstctrl => "0",
    rstcarryin => "0",
    rstalumode => "0",
    rstp => "0",
    rstd => "0",
    rstinmode => "0",
    a => reinterpret_output_port_net,
    b => reinterpret1_output_port_net,
    op => opmode_op_net,
    en(0) => unsignedtobool_dout_net,
    rst => rst_i_net,
    clk => clk_net,
    ce => ce_net,
    p => dsp48e2_1_p_net
  );
  dsp48e2_2 : entity xil_defaultlib.dsp_delay_xldsp48e2 
  generic map (
    a_input => "DIRECT",
    acascreg => 1,
    adreg => 0,
    alumodereg => 1,
    amultsel => "A",
    areg => 1,
    autoreset_pattern_detect => "NO_RESET",
    autoreset_priority => "RESET",
    b_input => "DIRECT",
    bcascreg => 1,
    bmultsel => "B",
    breg => 1,
    carryinreg => 1,
    carryinselreg => 1,
    carryout_width => 4,
    creg => 0,
    dreg => 0,
    inmodereg => 0,
    is_alumode_inverted => "0000",
    is_carryin_inverted => '0',
    is_clk_inverted => '0',
    is_inmode_inverted => "00000",
    is_opmode_inverted => "000000000",
    is_rsta_inverted => '0',
    is_rstallcarryin_inverted => '0',
    is_rstalumode_inverted => '0',
    is_rstb_inverted => '0',
    is_rstc_inverted => '0',
    is_rstctrl_inverted => '0',
    is_rstd_inverted => '0',
    is_rstinmode_inverted => '0',
    is_rstm_inverted => '0',
    is_rstp_inverted => '0',
    mreg => 1,
    opmodereg => 1,
    preaddinsel => "A",
    preg => 1,
    rnd => X"000000000000",
    sel_mask => "C",
    sel_pattern => "C",
    use_c_port => 0,
    use_mult => "MULTIPLY",
    use_op => 1,
    use_pattern_detect => "NO_PATDET",
    use_simd => "ONE48",
    use_widexor => "FALSE",
    xorsimd => "XOR12"
  )
  port map (
    carryin => "0",
    en => "1",
    cea2 => "1",
    ceb2 => "1",
    cec => "1",
    cealumode => "1",
    cemultcarryin => "1",
    cectrl => "1",
    cecarryin => "1",
    ced => "1",
    cead => "1",
    ceinmode => "1",
    alumode => "0000",
    rsta => "0",
    rstb => "0",
    rstc => "0",
    rstm => "0",
    rstctrl => "0",
    rstcarryin => "0",
    rstalumode => "0",
    rstp => "0",
    rstd => "0",
    rstinmode => "0",
    a => reinterpret2_output_port_net,
    b => reinterpret3_output_port_net,
    op => opmode_op_net,
    cea1(0) => unsignedtobool_dout_net,
    ceb1(0) => unsignedtobool_dout_net,
    cem(0) => unsignedtobool_dout_net,
    cep(0) => unsignedtobool_dout_net,
    rst => rst_i_net,
    clk => clk_net,
    ce => ce_net,
    p => dsp48e2_2_p_net
  );
  delay : entity xil_defaultlib.sysgen_delay_2eda9975a6 
  port map (
    clr => '0',
    d => reinterpret6_output_port_net,
    rst => rst_i_net,
    en(0) => unsignedtobool_dout_net,
    clk => clk_net,
    ce => ce_net,
    q => delay_q_net
  );
  delay1 : entity xil_defaultlib.sysgen_delay_13814cb9b3 
  port map (
    clr => '0',
    d => reinterpret7_output_port_net,
    rst => rst_i_net,
    en(0) => unsignedtobool_dout_net,
    clk => clk_net,
    ce => ce_net,
    q => delay1_q_net
  );
  opmode : entity xil_defaultlib.sysgen_opmode_cb47ba9e33 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => opmode_op_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_8f84bb3709 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => data_i_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_f9221eebd3 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => constant_op_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_8f84bb3709 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => data_i_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_f9221eebd3 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => constant_op_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_c6d11883da 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => dsp48e2_1_p_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_c6d11883da 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => dsp48e2_2_p_net,
    output_port => reinterpret7_output_port_net
  );
  enable_toggling : entity xil_defaultlib.sysgen_counter_3f6b639172 
  port map (
    clr => '0',
    rst => rst_i_net,
    clk => clk_net,
    ce => ce_net,
    op => enable_toggling_op_net
  );
  unsignedtobool : entity xil_defaultlib.dsp_delay_xlconvert 
  generic map (
    bool_conversion => 1,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 1,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 1,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => enable_toggling_op_net,
    clk => clk_net,
    ce => ce_net,
    dout(0) => unsignedtobool_dout_net
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity dsp_delay_default_clock_driver is
  port (
    dsp_delay_sysclk : in std_logic;
    dsp_delay_sysce : in std_logic;
    dsp_delay_sysclr : in std_logic;
    dsp_delay_clk1 : out std_logic;
    dsp_delay_ce1 : out std_logic
  );
end dsp_delay_default_clock_driver;
architecture structural of dsp_delay_default_clock_driver is 
begin
  clockdriver : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => dsp_delay_sysclk,
    sysce => dsp_delay_sysce,
    sysclr => dsp_delay_sysclr,
    clk => dsp_delay_clk1,
    ce => dsp_delay_ce1
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity dsp_delay is
  port (
    data_i : in std_logic_vector( 30-1 downto 0 );
    rst_i : in std_logic_vector( 1-1 downto 0 );
    clk : in std_logic;
    data_direct_globalen_o : out std_logic_vector( 48-1 downto 0 );
    data_direct_indven_o : out std_logic_vector( 48-1 downto 0 );
    data_macro_globalen_o : out std_logic_vector( 34-1 downto 0 );
    data_macro_indven_o : out std_logic_vector( 34-1 downto 0 )
  );
end dsp_delay;
architecture structural of dsp_delay is 
  attribute core_generation_info : string;
  attribute core_generation_info of structural : architecture is "dsp_delay,sysgen_core_2018_2,{,compilation=IP Catalog,block_icon_display=Default,family=kintexu,part=xcku040,speed=-1-i,package=ffva1156,synthesis_language=vhdl,hdl_library=xil_defaultlib,synthesis_strategy=Vivado Synthesis Defaults,implementation_strategy=Vivado Implementation Defaults,testbench=1,interface_doc=1,ce_clr=0,clock_period=8,system_simulink_period=8e-09,waveform_viewer=0,axilite_interface=0,ip_catalog_plugin=0,hwcosim_burst_mode=0,simulation_time=8e-07,constant=1,convert=3,counter=1,delay=2,dsp48e2=2,opmode=1,reinterpret=6,xbip_dsp48_macro_v3_0=2,}";
  signal clk_1_net : std_logic;
  signal ce_1_net : std_logic;
begin
  dsp_delay_default_clock_driver : entity xil_defaultlib.dsp_delay_default_clock_driver 
  port map (
    dsp_delay_sysclk => clk,
    dsp_delay_sysce => '1',
    dsp_delay_sysclr => '0',
    dsp_delay_clk1 => clk_1_net,
    dsp_delay_ce1 => ce_1_net
  );
  dsp_delay_struct : entity xil_defaultlib.dsp_delay_struct 
  port map (
    data_i => data_i,
    rst_i => rst_i,
    clk_1 => clk_1_net,
    ce_1 => ce_1_net,
    data_direct_globalen_o => data_direct_globalen_o,
    data_direct_indven_o => data_direct_indven_o,
    data_macro_globalen_o => data_macro_globalen_o,
    data_macro_indven_o => data_macro_indven_o
  );
end structural;
