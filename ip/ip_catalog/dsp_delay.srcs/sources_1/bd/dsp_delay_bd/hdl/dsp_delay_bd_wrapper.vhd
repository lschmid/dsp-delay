--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Wed Jun 12 10:03:58 2019
--Host        : PCBE15327 running 64-bit major release  (build 9200)
--Command     : generate_target dsp_delay_bd_wrapper.bd
--Design      : dsp_delay_bd_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dsp_delay_bd_wrapper is
  port (
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    rst_i : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end dsp_delay_bd_wrapper;

architecture STRUCTURE of dsp_delay_bd_wrapper is
  component dsp_delay_bd is
  port (
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    rst_i : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component dsp_delay_bd;
begin
dsp_delay_bd_i: component dsp_delay_bd
     port map (
      clk => clk,
      data_direct_globalen_o(47 downto 0) => data_direct_globalen_o(47 downto 0),
      data_direct_indven_o(47 downto 0) => data_direct_indven_o(47 downto 0),
      data_i(29 downto 0) => data_i(29 downto 0),
      data_macro_globalen_o(33 downto 0) => data_macro_globalen_o(33 downto 0),
      data_macro_indven_o(33 downto 0) => data_macro_indven_o(33 downto 0),
      rst_i(0) => rst_i(0)
    );
end STRUCTURE;
