--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Wed Jun 12 10:03:58 2019
--Host        : PCBE15327 running 64-bit major release  (build 9200)
--Command     : generate_target dsp_delay_bd.bd
--Design      : dsp_delay_bd
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dsp_delay_bd is
  port (
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    rst_i : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of dsp_delay_bd : entity is "dsp_delay_bd,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=dsp_delay_bd,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=1,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=SYSGEN,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of dsp_delay_bd : entity is "dsp_delay_bd.hwdef";
end dsp_delay_bd;

architecture STRUCTURE of dsp_delay_bd is
  component dsp_delay_bd_dsp_delay_1_0 is
  port (
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    rst_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 )
  );
  end component dsp_delay_bd_dsp_delay_1_0;
  signal clk_1 : STD_LOGIC;
  signal data_i_1 : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal dsp_delay_1_data_direct_globalen_o : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal dsp_delay_1_data_direct_indven_o : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal dsp_delay_1_data_macro_globalen_o : STD_LOGIC_VECTOR ( 33 downto 0 );
  signal dsp_delay_1_data_macro_indven_o : STD_LOGIC_VECTOR ( 33 downto 0 );
  signal rst_i_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 CLK.CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME CLK.CLK, CLK_DOMAIN dsp_delay_bd_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of data_direct_globalen_o : signal is "xilinx.com:signal:data:1.0 DATA.DATA_DIRECT_GLOBALEN_O DATA";
  attribute X_INTERFACE_PARAMETER of data_direct_globalen_o : signal is "XIL_INTERFACENAME DATA.DATA_DIRECT_GLOBALEN_O, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of data_direct_indven_o : signal is "xilinx.com:signal:data:1.0 DATA.DATA_DIRECT_INDVEN_O DATA";
  attribute X_INTERFACE_PARAMETER of data_direct_indven_o : signal is "XIL_INTERFACENAME DATA.DATA_DIRECT_INDVEN_O, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of data_i : signal is "xilinx.com:signal:data:1.0 DATA.DATA_I DATA";
  attribute X_INTERFACE_PARAMETER of data_i : signal is "XIL_INTERFACENAME DATA.DATA_I, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of data_macro_globalen_o : signal is "xilinx.com:signal:data:1.0 DATA.DATA_MACRO_GLOBALEN_O DATA";
  attribute X_INTERFACE_PARAMETER of data_macro_globalen_o : signal is "XIL_INTERFACENAME DATA.DATA_MACRO_GLOBALEN_O, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of data_macro_indven_o : signal is "xilinx.com:signal:data:1.0 DATA.DATA_MACRO_INDVEN_O DATA";
  attribute X_INTERFACE_PARAMETER of data_macro_indven_o : signal is "XIL_INTERFACENAME DATA.DATA_MACRO_INDVEN_O, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of rst_i : signal is "xilinx.com:signal:data:1.0 DATA.RST_I DATA";
  attribute X_INTERFACE_PARAMETER of rst_i : signal is "XIL_INTERFACENAME DATA.RST_I, LAYERED_METADATA undef";
begin
  clk_1 <= clk;
  data_direct_globalen_o(47 downto 0) <= dsp_delay_1_data_direct_globalen_o(47 downto 0);
  data_direct_indven_o(47 downto 0) <= dsp_delay_1_data_direct_indven_o(47 downto 0);
  data_i_1(29 downto 0) <= data_i(29 downto 0);
  data_macro_globalen_o(33 downto 0) <= dsp_delay_1_data_macro_globalen_o(33 downto 0);
  data_macro_indven_o(33 downto 0) <= dsp_delay_1_data_macro_indven_o(33 downto 0);
  rst_i_1(0) <= rst_i(0);
dsp_delay_1: component dsp_delay_bd_dsp_delay_1_0
     port map (
      clk => clk_1,
      data_direct_globalen_o(47 downto 0) => dsp_delay_1_data_direct_globalen_o(47 downto 0),
      data_direct_indven_o(47 downto 0) => dsp_delay_1_data_direct_indven_o(47 downto 0),
      data_i(29 downto 0) => data_i_1(29 downto 0),
      data_macro_globalen_o(33 downto 0) => dsp_delay_1_data_macro_globalen_o(33 downto 0),
      data_macro_indven_o(33 downto 0) => dsp_delay_1_data_macro_indven_o(33 downto 0),
      rst_i(0) => rst_i_1(0)
    );
end STRUCTURE;
