-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
entity dsp_delay_stub is
  port (
    data_i : in std_logic_vector( 30-1 downto 0 );
    rst_i : in std_logic_vector( 1-1 downto 0 );
    clk : in std_logic;
    data_direct_globalen_o : out std_logic_vector( 48-1 downto 0 );
    data_direct_indven_o : out std_logic_vector( 48-1 downto 0 );
    data_macro_globalen_o : out std_logic_vector( 34-1 downto 0 );
    data_macro_indven_o : out std_logic_vector( 34-1 downto 0 )
  );
end dsp_delay_stub;
architecture structural of dsp_delay_stub is 
begin
  sysgen_dut : entity xil_defaultlib.dsp_delay 
  port map (
    data_i => data_i,
    rst_i => rst_i,
    clk => clk,
    data_direct_globalen_o => data_direct_globalen_o,
    data_direct_indven_o => data_direct_indven_o,
    data_macro_globalen_o => data_macro_globalen_o,
    data_macro_indven_o => data_macro_indven_o
  );
end structural;
