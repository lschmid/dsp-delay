  sysgen_dut : entity xil_defaultlib.dsp_delay 
  port map (
    data_i => data_i,
    rst_i => rst_i,
    clk => clk,
    data_direct_globalen_o => data_direct_globalen_o,
    data_direct_indven_o => data_direct_indven_o,
    data_macro_globalen_o => data_macro_globalen_o,
    data_macro_indven_o => data_macro_indven_o
  );
