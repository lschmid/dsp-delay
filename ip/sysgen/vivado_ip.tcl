#-----------------------------------------------------------------
# System Generator version 2018.2 IP Tcl source file.
#
# Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
# text/file contains proprietary, confidential information of Xilinx,
# Inc., is distributed under license from Xilinx, Inc., and may be used,
# copied and/or disclosed only pursuant to the terms of a valid license
# agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
# this text/file solely for design, simulation, implementation and
# creation of design files limited to Xilinx devices or technologies.
# Use with non-Xilinx devices or technologies is expressly prohibited
# and immediately terminates your license unless covered by a separate
# agreement.
#
# Xilinx is providing this design, code, or information "as is" solely
# for use in developing programs and solutions for Xilinx devices.  By
# providing this design, code, or information as one possible
# implementation of this feature, application or standard, Xilinx is
# making no representation that this implementation is free from any
# claims of infringement.  You are responsible for obtaining any rights
# you may require for your implementation.  Xilinx expressly disclaims
# any warranty whatsoever with respect to the adequacy of the
# implementation, including but not limited to warranties of
# merchantability or fitness for a particular purpose.
#
# Xilinx products are not intended for use in life support appliances,
# devices, or systems.  Use in such applications is expressly prohibited.
#
# Any modifications that are made to the source code are done at the user's
# sole risk and will be unsupported.
#
# This copyright and support notice must be retained as part of this
# text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
# reserved.
#-----------------------------------------------------------------

set existingipslist [get_ips]
if {[lsearch $existingipslist dsp_delay_xbip_dsp48_macro_v3_0_i0] < 0} {
create_ip -name xbip_dsp48_macro -version 3.0 -vendor xilinx.com -library ip -module_name dsp_delay_xbip_dsp48_macro_v3_0_i0
set params_list [list]
lappend params_list CONFIG.Component_Name {dsp_delay_xbip_dsp48_macro_v3_0_i0}
lappend params_list CONFIG.GUI_Behaviour {Sysgen}
lappend params_list CONFIG.a_binarywidth {15}
lappend params_list CONFIG.a_width {16}
lappend params_list CONFIG.areg_1 {false}
lappend params_list CONFIG.areg_2 {false}
lappend params_list CONFIG.areg_3 {true}
lappend params_list CONFIG.areg_4 {true}
lappend params_list CONFIG.b_binarywidth {17}
lappend params_list CONFIG.b_width {18}
lappend params_list CONFIG.breg_1 {false}
lappend params_list CONFIG.breg_2 {false}
lappend params_list CONFIG.breg_3 {true}
lappend params_list CONFIG.breg_4 {true}
lappend params_list CONFIG.c_binarywidth {32}
lappend params_list CONFIG.c_width {48}
lappend params_list CONFIG.cinreg_1 {false}
lappend params_list CONFIG.cinreg_2 {false}
lappend params_list CONFIG.cinreg_3 {false}
lappend params_list CONFIG.cinreg_4 {false}
lappend params_list CONFIG.cinreg_5 {false}
lappend params_list CONFIG.concat_binarywidth {32}
lappend params_list CONFIG.concat_width {48}
lappend params_list CONFIG.concatreg_3 {false}
lappend params_list CONFIG.concatreg_4 {false}
lappend params_list CONFIG.concatreg_5 {false}
lappend params_list CONFIG.creg_1 {false}
lappend params_list CONFIG.creg_2 {false}
lappend params_list CONFIG.creg_3 {false}
lappend params_list CONFIG.creg_4 {false}
lappend params_list CONFIG.creg_5 {false}
lappend params_list CONFIG.d_binarywidth {0}
lappend params_list CONFIG.d_width {18}
lappend params_list CONFIG.dreg_1 {false}
lappend params_list CONFIG.dreg_2 {false}
lappend params_list CONFIG.dreg_3 {false}
lappend params_list CONFIG.has_a_ce {false}
lappend params_list CONFIG.has_a_sclr {false}
lappend params_list CONFIG.has_acout {false}
lappend params_list CONFIG.has_b_ce {false}
lappend params_list CONFIG.has_b_sclr {false}
lappend params_list CONFIG.has_bcout {false}
lappend params_list CONFIG.has_c_ce {false}
lappend params_list CONFIG.has_c_sclr {false}
lappend params_list CONFIG.has_carrycascout {false}
lappend params_list CONFIG.has_carryout {false}
lappend params_list CONFIG.has_ce {true}
lappend params_list CONFIG.has_concat_ce {false}
lappend params_list CONFIG.has_concat_sclr {false}
lappend params_list CONFIG.has_d_ce {false}
lappend params_list CONFIG.has_d_sclr {false}
lappend params_list CONFIG.has_m_ce {false}
lappend params_list CONFIG.has_m_sclr {false}
lappend params_list CONFIG.has_p_ce {false}
lappend params_list CONFIG.has_p_sclr {false}
lappend params_list CONFIG.has_pcout {false}
lappend params_list CONFIG.has_sclr {true}
lappend params_list CONFIG.has_sel_ce {false}
lappend params_list CONFIG.has_sel_sclr {false}
lappend params_list CONFIG.instruction1 {#}
lappend params_list CONFIG.instruction2 {#}
lappend params_list CONFIG.instruction3 {#}
lappend params_list CONFIG.instruction4 {#}
lappend params_list CONFIG.instruction5 {#}
lappend params_list CONFIG.instruction6 {#}
lappend params_list CONFIG.instruction7 {#}
lappend params_list CONFIG.instruction8 {#}
lappend params_list CONFIG.instruction_list {A*B}
lappend params_list CONFIG.mreg_5 {true}
lappend params_list CONFIG.opreg_1 {false}
lappend params_list CONFIG.opreg_2 {false}
lappend params_list CONFIG.opreg_3 {false}
lappend params_list CONFIG.opreg_4 {false}
lappend params_list CONFIG.opreg_5 {false}
lappend params_list CONFIG.output_properties {Full_Precision}
lappend params_list CONFIG.p_binarywidth {32}
lappend params_list CONFIG.p_full_width {34}
lappend params_list CONFIG.p_width {34}
lappend params_list CONFIG.pcin_binarywidth {32}
lappend params_list CONFIG.pipeline_options {Automatic}
lappend params_list CONFIG.preg_6 {true}
lappend params_list CONFIG.show_filtered {false}
lappend params_list CONFIG.tier_1 {false}
lappend params_list CONFIG.tier_2 {false}
lappend params_list CONFIG.tier_3 {false}
lappend params_list CONFIG.tier_4 {false}
lappend params_list CONFIG.tier_5 {false}
lappend params_list CONFIG.tier_6 {false}
lappend params_list CONFIG.use_dsp48 {true}

set_property -dict $params_list [get_ips dsp_delay_xbip_dsp48_macro_v3_0_i0]
}


set existingipslist [get_ips]
if {[lsearch $existingipslist dsp_delay_xbip_dsp48_macro_v3_0_i1] < 0} {
create_ip -name xbip_dsp48_macro -version 3.0 -vendor xilinx.com -library ip -module_name dsp_delay_xbip_dsp48_macro_v3_0_i1
set params_list [list]
lappend params_list CONFIG.Component_Name {dsp_delay_xbip_dsp48_macro_v3_0_i1}
lappend params_list CONFIG.GUI_Behaviour {Sysgen}
lappend params_list CONFIG.a_binarywidth {15}
lappend params_list CONFIG.a_width {16}
lappend params_list CONFIG.areg_1 {false}
lappend params_list CONFIG.areg_2 {false}
lappend params_list CONFIG.areg_3 {true}
lappend params_list CONFIG.areg_4 {true}
lappend params_list CONFIG.b_binarywidth {17}
lappend params_list CONFIG.b_width {18}
lappend params_list CONFIG.breg_1 {false}
lappend params_list CONFIG.breg_2 {false}
lappend params_list CONFIG.breg_3 {true}
lappend params_list CONFIG.breg_4 {true}
lappend params_list CONFIG.c_binarywidth {32}
lappend params_list CONFIG.c_width {48}
lappend params_list CONFIG.cinreg_1 {false}
lappend params_list CONFIG.cinreg_2 {false}
lappend params_list CONFIG.cinreg_3 {false}
lappend params_list CONFIG.cinreg_4 {false}
lappend params_list CONFIG.cinreg_5 {false}
lappend params_list CONFIG.concat_binarywidth {32}
lappend params_list CONFIG.concat_width {48}
lappend params_list CONFIG.concatreg_3 {false}
lappend params_list CONFIG.concatreg_4 {false}
lappend params_list CONFIG.concatreg_5 {false}
lappend params_list CONFIG.creg_1 {false}
lappend params_list CONFIG.creg_2 {false}
lappend params_list CONFIG.creg_3 {false}
lappend params_list CONFIG.creg_4 {false}
lappend params_list CONFIG.creg_5 {false}
lappend params_list CONFIG.d_binarywidth {0}
lappend params_list CONFIG.d_width {18}
lappend params_list CONFIG.dreg_1 {false}
lappend params_list CONFIG.dreg_2 {false}
lappend params_list CONFIG.dreg_3 {false}
lappend params_list CONFIG.has_a_ce {true}
lappend params_list CONFIG.has_a_sclr {false}
lappend params_list CONFIG.has_acout {false}
lappend params_list CONFIG.has_b_ce {true}
lappend params_list CONFIG.has_b_sclr {false}
lappend params_list CONFIG.has_bcout {false}
lappend params_list CONFIG.has_c_ce {false}
lappend params_list CONFIG.has_c_sclr {false}
lappend params_list CONFIG.has_carrycascout {false}
lappend params_list CONFIG.has_carryout {false}
lappend params_list CONFIG.has_ce {false}
lappend params_list CONFIG.has_concat_ce {false}
lappend params_list CONFIG.has_concat_sclr {false}
lappend params_list CONFIG.has_d_ce {false}
lappend params_list CONFIG.has_d_sclr {false}
lappend params_list CONFIG.has_m_ce {true}
lappend params_list CONFIG.has_m_sclr {false}
lappend params_list CONFIG.has_p_ce {true}
lappend params_list CONFIG.has_p_sclr {false}
lappend params_list CONFIG.has_pcout {false}
lappend params_list CONFIG.has_sclr {true}
lappend params_list CONFIG.has_sel_ce {false}
lappend params_list CONFIG.has_sel_sclr {false}
lappend params_list CONFIG.instruction1 {#}
lappend params_list CONFIG.instruction2 {#}
lappend params_list CONFIG.instruction3 {#}
lappend params_list CONFIG.instruction4 {#}
lappend params_list CONFIG.instruction5 {#}
lappend params_list CONFIG.instruction6 {#}
lappend params_list CONFIG.instruction7 {#}
lappend params_list CONFIG.instruction8 {#}
lappend params_list CONFIG.instruction_list {A*B}
lappend params_list CONFIG.mreg_5 {true}
lappend params_list CONFIG.opreg_1 {false}
lappend params_list CONFIG.opreg_2 {false}
lappend params_list CONFIG.opreg_3 {false}
lappend params_list CONFIG.opreg_4 {false}
lappend params_list CONFIG.opreg_5 {false}
lappend params_list CONFIG.output_properties {Full_Precision}
lappend params_list CONFIG.p_binarywidth {32}
lappend params_list CONFIG.p_full_width {34}
lappend params_list CONFIG.p_width {34}
lappend params_list CONFIG.pcin_binarywidth {32}
lappend params_list CONFIG.pipeline_options {Automatic}
lappend params_list CONFIG.preg_6 {true}
lappend params_list CONFIG.show_filtered {false}
lappend params_list CONFIG.tier_1 {false}
lappend params_list CONFIG.tier_2 {false}
lappend params_list CONFIG.tier_3 {false}
lappend params_list CONFIG.tier_4 {false}
lappend params_list CONFIG.tier_5 {false}
lappend params_list CONFIG.tier_6 {false}
lappend params_list CONFIG.use_dsp48 {true}

set_property -dict $params_list [get_ips dsp_delay_xbip_dsp48_macro_v3_0_i1]
}


validate_ip [get_ips]
