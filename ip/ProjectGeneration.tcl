# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator. 
#


namespace eval ::xilinx::dsp::planaheaddriver {
	set Compilation {IP Catalog}
	set CompilationFlow {IP}
	set CreateInterfaceDocument {on}
	set DSPDevice {xcku040}
	set DSPFamily {kintexu}
	set DSPPackage {ffva1156}
	set DSPSpeed {-1-i}
	set FPGAClockPeriod 8
	set GenerateTestBench 1
	set HDLLanguage {vhdl}
	set IPOOCCacheRootPath {C:/Users/lschmid/AppData/Local/Xilinx/Sysgen/SysgenVivado/win64.o/ip}
	set IP_Auto_Infer {1}
	set IP_Categories_Text {System Generator for DSP}
	set IP_Common_Repos {0}
	set IP_Description {}
	set IP_Dir {}
	set IP_Library_Text {SPSCavityLoops}
	set IP_LifeCycle_Menu {2}
	set IP_Logo {sysgen_icon_100.png}
	set IP_Name {hcomb_tb}
	set IP_Revision {191239416}
	set IP_Socket_IP {0}
	set IP_Socket_IP_Proj_Path {}
	set IP_Vendor_Text {CERN}
	set IP_Version_Text {0.1}
	set ImplStrategyName {Vivado Implementation Defaults}
	set PostProjectCreationProc {dsp_package_for_vivado_ip_integrator}
	set Project {dsp_delay}
	set ProjectFiles {
		{{conv_pkg.vhd} -lib {xil_defaultlib}}
		{{synth_reg.vhd} -lib {xil_defaultlib}}
		{{synth_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{srl17e.vhd} -lib {xil_defaultlib}}
		{{srl33e.vhd} -lib {xil_defaultlib}}
		{{synth_reg_reg.vhd} -lib {xil_defaultlib}}
		{{single_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{xlclockdriver_rd.vhd} -lib {xil_defaultlib}}
		{{vivado_ip.tcl}}
		{{dsp_delay_entity_declarations.vhd} -lib {xil_defaultlib}}
		{{dsp_delay.vhd} -lib {xil_defaultlib}}
		{{dsp_delay_tb.vhd}}
		{{dsp_delay_clock.xdc}}
		{{dsp_delay.xdc}}
		{{dsp_delay.htm}}
	}
	set SimPeriod 8e-09
	set SimTime 8e-07
	set SimulationTime {1008.00000000 ns}
	set SynthStrategyName {Vivado Synthesis Defaults}
	set SynthesisTool {Vivado}
	set TargetDir {C:/work/dsp-delay/ip}
	set TestBenchModule {dsp_delay_tb}
	set TopLevelModule {dsp_delay}
	set TopLevelSimulinkHandle 2.00049
	set VHDLLib {xil_defaultlib}
	set TopLevelPortInterface {}
	dict set TopLevelPortInterface rst_i Name {rst_i}
	dict set TopLevelPortInterface rst_i Type Bool
	dict set TopLevelPortInterface rst_i ArithmeticType xlUnsigned
	dict set TopLevelPortInterface rst_i BinaryPoint 0
	dict set TopLevelPortInterface rst_i Width 1
	dict set TopLevelPortInterface rst_i DatFile {dsp_delay_rst_i.dat}
	dict set TopLevelPortInterface rst_i IconText {rst_i}
	dict set TopLevelPortInterface rst_i Direction in
	dict set TopLevelPortInterface rst_i Period 1
	dict set TopLevelPortInterface rst_i Interface 0
	dict set TopLevelPortInterface rst_i InterfaceName {}
	dict set TopLevelPortInterface rst_i InterfaceString {DATA}
	dict set TopLevelPortInterface rst_i ClockDomain {dsp_delay}
	dict set TopLevelPortInterface rst_i Locs {}
	dict set TopLevelPortInterface rst_i IOStandard {}
	dict set TopLevelPortInterface data_i Name {data_i}
	dict set TopLevelPortInterface data_i Type Fix_30_26
	dict set TopLevelPortInterface data_i ArithmeticType xlSigned
	dict set TopLevelPortInterface data_i BinaryPoint 26
	dict set TopLevelPortInterface data_i Width 30
	dict set TopLevelPortInterface data_i DatFile {dsp_delay_data_i.dat}
	dict set TopLevelPortInterface data_i IconText {data_i}
	dict set TopLevelPortInterface data_i Direction in
	dict set TopLevelPortInterface data_i Period 1
	dict set TopLevelPortInterface data_i Interface 0
	dict set TopLevelPortInterface data_i InterfaceName {}
	dict set TopLevelPortInterface data_i InterfaceString {DATA}
	dict set TopLevelPortInterface data_i ClockDomain {dsp_delay}
	dict set TopLevelPortInterface data_i Locs {}
	dict set TopLevelPortInterface data_i IOStandard {}
	dict set TopLevelPortInterface data_direct_globalen_o Name {data_direct_globalen_o}
	dict set TopLevelPortInterface data_direct_globalen_o Type Fix_48_43
	dict set TopLevelPortInterface data_direct_globalen_o ArithmeticType xlSigned
	dict set TopLevelPortInterface data_direct_globalen_o BinaryPoint 43
	dict set TopLevelPortInterface data_direct_globalen_o Width 48
	dict set TopLevelPortInterface data_direct_globalen_o DatFile {dsp_delay_data_direct_globalen_o.dat}
	dict set TopLevelPortInterface data_direct_globalen_o IconText {data_direct_globalen_o}
	dict set TopLevelPortInterface data_direct_globalen_o Direction out
	dict set TopLevelPortInterface data_direct_globalen_o Period 1
	dict set TopLevelPortInterface data_direct_globalen_o Interface 0
	dict set TopLevelPortInterface data_direct_globalen_o InterfaceName {}
	dict set TopLevelPortInterface data_direct_globalen_o InterfaceString {DATA}
	dict set TopLevelPortInterface data_direct_globalen_o ClockDomain {dsp_delay}
	dict set TopLevelPortInterface data_direct_globalen_o Locs {}
	dict set TopLevelPortInterface data_direct_globalen_o IOStandard {}
	dict set TopLevelPortInterface data_direct_indven_o Name {data_direct_indven_o}
	dict set TopLevelPortInterface data_direct_indven_o Type Fix_48_43
	dict set TopLevelPortInterface data_direct_indven_o ArithmeticType xlSigned
	dict set TopLevelPortInterface data_direct_indven_o BinaryPoint 43
	dict set TopLevelPortInterface data_direct_indven_o Width 48
	dict set TopLevelPortInterface data_direct_indven_o DatFile {dsp_delay_data_direct_indven_o.dat}
	dict set TopLevelPortInterface data_direct_indven_o IconText {data_direct_indven_o}
	dict set TopLevelPortInterface data_direct_indven_o Direction out
	dict set TopLevelPortInterface data_direct_indven_o Period 1
	dict set TopLevelPortInterface data_direct_indven_o Interface 0
	dict set TopLevelPortInterface data_direct_indven_o InterfaceName {}
	dict set TopLevelPortInterface data_direct_indven_o InterfaceString {DATA}
	dict set TopLevelPortInterface data_direct_indven_o ClockDomain {dsp_delay}
	dict set TopLevelPortInterface data_direct_indven_o Locs {}
	dict set TopLevelPortInterface data_direct_indven_o IOStandard {}
	dict set TopLevelPortInterface data_macro_globalen_o Name {data_macro_globalen_o}
	dict set TopLevelPortInterface data_macro_globalen_o Type Fix_34_32
	dict set TopLevelPortInterface data_macro_globalen_o ArithmeticType xlSigned
	dict set TopLevelPortInterface data_macro_globalen_o BinaryPoint 32
	dict set TopLevelPortInterface data_macro_globalen_o Width 34
	dict set TopLevelPortInterface data_macro_globalen_o DatFile {dsp_delay_data_macro_globalen_o.dat}
	dict set TopLevelPortInterface data_macro_globalen_o IconText {data_macro_globalen_o}
	dict set TopLevelPortInterface data_macro_globalen_o Direction out
	dict set TopLevelPortInterface data_macro_globalen_o Period 1
	dict set TopLevelPortInterface data_macro_globalen_o Interface 0
	dict set TopLevelPortInterface data_macro_globalen_o InterfaceName {}
	dict set TopLevelPortInterface data_macro_globalen_o InterfaceString {DATA}
	dict set TopLevelPortInterface data_macro_globalen_o ClockDomain {dsp_delay}
	dict set TopLevelPortInterface data_macro_globalen_o Locs {}
	dict set TopLevelPortInterface data_macro_globalen_o IOStandard {}
	dict set TopLevelPortInterface data_macro_indven_o Name {data_macro_indven_o}
	dict set TopLevelPortInterface data_macro_indven_o Type Fix_34_32
	dict set TopLevelPortInterface data_macro_indven_o ArithmeticType xlSigned
	dict set TopLevelPortInterface data_macro_indven_o BinaryPoint 32
	dict set TopLevelPortInterface data_macro_indven_o Width 34
	dict set TopLevelPortInterface data_macro_indven_o DatFile {dsp_delay_data_macro_indven_o.dat}
	dict set TopLevelPortInterface data_macro_indven_o IconText {data_macro_indven_o}
	dict set TopLevelPortInterface data_macro_indven_o Direction out
	dict set TopLevelPortInterface data_macro_indven_o Period 1
	dict set TopLevelPortInterface data_macro_indven_o Interface 0
	dict set TopLevelPortInterface data_macro_indven_o InterfaceName {}
	dict set TopLevelPortInterface data_macro_indven_o InterfaceString {DATA}
	dict set TopLevelPortInterface data_macro_indven_o ClockDomain {dsp_delay}
	dict set TopLevelPortInterface data_macro_indven_o Locs {}
	dict set TopLevelPortInterface data_macro_indven_o IOStandard {}
	dict set TopLevelPortInterface clk Name {clk}
	dict set TopLevelPortInterface clk Type -
	dict set TopLevelPortInterface clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface clk BinaryPoint 0
	dict set TopLevelPortInterface clk Width 1
	dict set TopLevelPortInterface clk DatFile {}
	dict set TopLevelPortInterface clk Direction in
	dict set TopLevelPortInterface clk Period 1
	dict set TopLevelPortInterface clk Interface 6
	dict set TopLevelPortInterface clk InterfaceName {}
	dict set TopLevelPortInterface clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface clk ClockDomain {dsp_delay}
	dict set TopLevelPortInterface clk Locs {}
	dict set TopLevelPortInterface clk IOStandard {}
	dict set TopLevelPortInterface clk AssociatedInterfaces {}
	dict set TopLevelPortInterface clk AssociatedResets {}
	set MemoryMappedPort {}
}

source SgPaProject.tcl
::xilinx::dsp::planaheadworker::dsp_create_project