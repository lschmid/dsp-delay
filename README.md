# DSP Delay Bug in SysGen

The following project demonstrates how a DSP block instantiated as System Generator model is simulated differently in System Generator than (once exported as an IP Core) in Vivado. The output values of the two simulation are actually corresponding but the latency of the operation is not the same. So, the results of the two simulations differentiate in their output delay leading to the conclusion that System Generator does not correctly simulate the project (i.e. not corresponding to a possible hardware implementation).

Software versions used:
- MATLAB 9.3 (R2017b), with System Generator
- Vivado 2018.2

## Structure

- `dsp_delay.slx`: System Generator model of the different DSP instantiations
- `ip/`: Folder containing the exported IP core from System Generator
- `vivado/`: Folder containing a simple Vivado project making use of the exported IP core
- `simulation-outputs/`: Folder containing the output waveforms of the different simulations

The `dsp_delay.slx` file instantiates four DSP models which are slightly differently configured but should all lead to a similar output. The following configurations are used:
- DSP48E2 block with global enable signal
- DSP48E2 block with individual enable signals
- DSP macro with global enable signal
- DSP macro with individual enable signals

The proposed test consists of a toggling enable signal (effectively reducing the data rate by a factor of 2). As DSP input values a constant of 1 is multiplied with an impulse signal. The output of the four models should thus lead to an impulse as well and the testbench is adapted in a way that all four output impulses should occur at the same instance in time.

## Simulation

1. Open the `dsp_delay.slx` file in System Generator
2. Launch the System Generator simulation by clicking on run
3. Verify by opening the model's scope that all impulses propagate with the same latency (i.e. arrive at the same moment)
4. Open the Vivado project by opening the `vivado/vivado.xpr` project file
5. Verify that the IP's output products are generated or the synthesis completed
6. Launch the behavioral simulation
7. Apply the tcl commands from the `vivado/vivado_stimulus.tcl` files to condition the simulation
8. Notice how one of the DSP model outputs has a latency of an additional clock cycle compared to the System Generator simulation

## Conclusion

The DSP48E2 block model in System Generator is erroneous if individual enable signals are selected. This is very likely linked to a wrong modeling of the `P` output flip-flop and becomes only apparent if one uses a toggling (i.e. non-constant) enable signal.
It could also be observed that choosing different settings for the "Pipeline p" option while relying on the `Pcout` output, which should be unaffected by the pipelining option of `P`, can (wrongly) alter the latency of the `Pcout` output once simulated in Vivado.