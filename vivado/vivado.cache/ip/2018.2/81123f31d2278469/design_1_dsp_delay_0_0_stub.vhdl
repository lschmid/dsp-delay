-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Wed Jun 12 10:17:36 2019
-- Host        : PCBE15327 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_dsp_delay_0_0_stub.vhdl
-- Design      : design_1_dsp_delay_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku040-ffva1156-1-i
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    rst_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "data_i[29:0],rst_i[0:0],clk,data_direct_globalen_o[47:0],data_direct_indven_o[47:0],data_macro_globalen_o[33:0],data_macro_indven_o[33:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "dsp_delay,Vivado 2018.2";
begin
end;
