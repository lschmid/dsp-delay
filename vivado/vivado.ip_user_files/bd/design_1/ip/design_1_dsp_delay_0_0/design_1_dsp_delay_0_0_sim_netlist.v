// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Wed Jun 12 10:17:37 2019
// Host        : PCBE15327 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/work/dsp-delay/vivado/vivado.srcs/sources_1/bd/design_1/ip/design_1_dsp_delay_0_0/design_1_dsp_delay_0_0_sim_netlist.v
// Design      : design_1_dsp_delay_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku040-ffva1156-1-i
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_dsp_delay_0_0,dsp_delay,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "sysgen" *) 
(* x_core_info = "dsp_delay,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_dsp_delay_0_0
   (data_i,
    rst_i,
    clk,
    data_direct_globalen_o,
    data_direct_indven_o,
    data_macro_globalen_o,
    data_macro_indven_o);
  (* x_interface_info = "xilinx.com:signal:data:1.0 data_i DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME data_i, LAYERED_METADATA undef" *) input [29:0]data_i;
  (* x_interface_info = "xilinx.com:signal:data:1.0 rst_i DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME rst_i, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}" *) input [0:0]rst_i;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF data_direct_globalen_o:data_direct_indven_o:data_i:data_macro_globalen_o:data_macro_indven_o, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_clk_0" *) input clk;
  (* x_interface_info = "xilinx.com:signal:data:1.0 data_direct_globalen_o DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME data_direct_globalen_o, LAYERED_METADATA undef" *) output [47:0]data_direct_globalen_o;
  (* x_interface_info = "xilinx.com:signal:data:1.0 data_direct_indven_o DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME data_direct_indven_o, LAYERED_METADATA undef" *) output [47:0]data_direct_indven_o;
  (* x_interface_info = "xilinx.com:signal:data:1.0 data_macro_globalen_o DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME data_macro_globalen_o, LAYERED_METADATA undef" *) output [33:0]data_macro_globalen_o;
  (* x_interface_info = "xilinx.com:signal:data:1.0 data_macro_indven_o DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME data_macro_indven_o, LAYERED_METADATA undef" *) output [33:0]data_macro_indven_o;

  wire clk;
  wire [47:0]data_direct_globalen_o;
  wire [47:0]data_direct_indven_o;
  wire [29:0]data_i;
  wire [33:0]data_macro_globalen_o;
  wire [33:0]data_macro_indven_o;
  wire [0:0]rst_i;

  design_1_dsp_delay_0_0_dsp_delay U0
       (.clk(clk),
        .data_direct_globalen_o(data_direct_globalen_o),
        .data_direct_indven_o(data_direct_indven_o),
        .data_i(data_i),
        .data_macro_globalen_o(data_macro_globalen_o),
        .data_macro_indven_o(data_macro_indven_o),
        .rst_i(rst_i));
endmodule

(* ORIG_REF_NAME = "dsp_delay" *) 
module design_1_dsp_delay_0_0_dsp_delay
   (data_i,
    rst_i,
    clk,
    data_direct_globalen_o,
    data_direct_indven_o,
    data_macro_globalen_o,
    data_macro_indven_o);
  input [29:0]data_i;
  input [0:0]rst_i;
  input clk;
  output [47:0]data_direct_globalen_o;
  output [47:0]data_direct_indven_o;
  output [33:0]data_macro_globalen_o;
  output [33:0]data_macro_indven_o;

  wire clk;
  wire [47:0]data_direct_globalen_o;
  wire [47:0]data_direct_indven_o;
  wire [29:0]data_i;
  wire [33:0]data_macro_globalen_o;
  wire [33:0]data_macro_indven_o;
  wire [0:0]rst_i;

  design_1_dsp_delay_0_0_dsp_delay_struct dsp_delay_struct
       (.clk(clk),
        .data_direct_globalen_o(data_direct_globalen_o),
        .data_direct_indven_o(data_direct_indven_o),
        .data_i(data_i),
        .data_macro_globalen_o(data_macro_globalen_o),
        .data_macro_indven_o(data_macro_indven_o),
        .rst_i(rst_i));
endmodule

(* ORIG_REF_NAME = "dsp_delay_struct" *) 
module design_1_dsp_delay_0_0_dsp_delay_struct
   (data_macro_globalen_o,
    data_macro_indven_o,
    data_direct_globalen_o,
    data_direct_indven_o,
    clk,
    rst_i,
    data_i);
  output [33:0]data_macro_globalen_o;
  output [33:0]data_macro_indven_o;
  output [47:0]data_direct_globalen_o;
  output [47:0]data_direct_indven_o;
  input clk;
  input [0:0]rst_i;
  input [29:0]data_i;

  wire clk;
  wire [47:0]data_direct_globalen_o;
  wire [47:0]data_direct_indven_o;
  wire [29:0]data_i;
  wire [33:0]data_macro_globalen_o;
  wire [33:0]data_macro_indven_o;
  wire [47:0]dsp48e2_1_p_net;
  wire [47:0]dsp48e2_2_p_net;
  wire enable_toggling_op_net;
  wire [0:0]rst_i;

  design_1_dsp_delay_0_0_sysgen_delay_2eda9975a6 delay
       (.D(dsp48e2_1_p_net),
        .clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .data_direct_globalen_o(data_direct_globalen_o),
        .rst_i(rst_i));
  design_1_dsp_delay_0_0_sysgen_delay_13814cb9b3 delay1
       (.D(dsp48e2_2_p_net),
        .clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .data_direct_indven_o(data_direct_indven_o),
        .rst_i(rst_i));
  design_1_dsp_delay_0_0_xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e dsp48_macro_3_0
       (.clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .data_i(data_i[26:11]),
        .data_macro_globalen_o(data_macro_globalen_o),
        .rst_i(rst_i));
  design_1_dsp_delay_0_0_xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2 dsp48_macro_3_0_1
       (.clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .data_i(data_i[26:11]),
        .data_macro_indven_o(data_macro_indven_o),
        .rst_i(rst_i));
  design_1_dsp_delay_0_0_dsp_delay_xldsp48e2 dsp48e2_1
       (.clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .d(dsp48e2_1_p_net),
        .data_i(data_i),
        .rst_i(rst_i));
  design_1_dsp_delay_0_0_dsp_delay_xldsp48e2_0 dsp48e2_2
       (.I1(dsp48e2_2_p_net),
        .clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .data_i(data_i),
        .rst_i(rst_i));
  design_1_dsp_delay_0_0_sysgen_counter_3f6b639172 enable_toggling
       (.clk(clk),
        .count_reg_20_23(enable_toggling_op_net),
        .rst_i(rst_i));
endmodule

(* CHECK_LICENSE_TYPE = "dsp_delay_xbip_dsp48_macro_v3_0_i0,xbip_dsp48_macro_v3_0_16,{}" *) (* ORIG_REF_NAME = "dsp_delay_xbip_dsp48_macro_v3_0_i0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
module design_1_dsp_delay_0_0_dsp_delay_xbip_dsp48_macro_v3_0_i0
   (CLK,
    CE,
    SCLR,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF p_intf:pcout_intf:carrycascout_intf:carryout_intf:bcout_intf:acout_intf:concat_intf:d_intf:c_intf:b_intf:a_intf:bcin_intf:acin_intf:pcin_intf:carryin_intf:carrycascin_intf:sel_intf, ASSOCIATED_RESET SCLR:SCLRD:SCLRA:SCLRB:SCLRCONCAT:SCLRC:SCLRM:SCLRP:SCLRSEL, ASSOCIATED_CLKEN CE:CED:CED1:CED2:CED3:CEA:CEA1:CEA2:CEA3:CEA4:CEB:CEB1:CEB2:CEB3:CEB4:CECONCAT:CECONCAT3:CECONCAT4:CECONCAT5:CEC:CEC1:CEC2:CEC3:CEC4:CEC5:CEM:CEP:CESEL:CESEL1:CESEL2:CESEL3:CESEL4:CESEL5, FREQ_HZ 100000000, PHASE 0.000" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [17:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME p_intf, LAYERED_METADATA undef" *) output [33:0]P;

  wire [15:0]A;
  wire [17:0]B;
  wire CE;
  wire CLK;
  wire [33:0]P;
  wire SCLR;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "131072" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "33" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_dsp_delay_0_0_xbip_dsp48_macro_v3_0_16 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(CE),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(1'b1),
        .CEA4(1'b1),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(1'b1),
        .CEB4(1'b1),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(1'b1),
        .CEP(1'b1),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(SCLR),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "dsp_delay_xbip_dsp48_macro_v3_0_i1,xbip_dsp48_macro_v3_0_16,{}" *) (* ORIG_REF_NAME = "dsp_delay_xbip_dsp48_macro_v3_0_i1" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
module design_1_dsp_delay_0_0_dsp_delay_xbip_dsp48_macro_v3_0_i1
   (CLK,
    SCLR,
    A,
    B,
    P,
    CEA3,
    CEA4,
    CEB3,
    CEB4,
    CEM,
    CEP);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF p_intf:pcout_intf:carrycascout_intf:carryout_intf:bcout_intf:acout_intf:concat_intf:d_intf:c_intf:b_intf:a_intf:bcin_intf:acin_intf:pcin_intf:carryin_intf:carrycascin_intf:sel_intf, ASSOCIATED_RESET SCLR:SCLRD:SCLRA:SCLRB:SCLRCONCAT:SCLRC:SCLRM:SCLRP:SCLRSEL, ASSOCIATED_CLKEN CE:CED:CED1:CED2:CED3:CEA:CEA1:CEA2:CEA3:CEA4:CEB:CEB1:CEB2:CEB3:CEB4:CECONCAT:CECONCAT3:CECONCAT4:CECONCAT5:CEC:CEC1:CEC2:CEC3:CEC4:CEC5:CEM:CEP:CESEL:CESEL1:CESEL2:CESEL3:CESEL4:CESEL5, FREQ_HZ 100000000, PHASE 0.000" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [17:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME p_intf, LAYERED_METADATA undef" *) output [33:0]P;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 cea3_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME cea3_intf, POLARITY ACTIVE_LOW" *) input CEA3;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 cea4_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME cea4_intf, POLARITY ACTIVE_LOW" *) input CEA4;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ceb3_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ceb3_intf, POLARITY ACTIVE_LOW" *) input CEB3;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ceb4_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ceb4_intf, POLARITY ACTIVE_LOW" *) input CEB4;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 cem_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME cem_intf, POLARITY ACTIVE_LOW" *) input CEM;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 cep_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME cep_intf, POLARITY ACTIVE_LOW" *) input CEP;

  wire [15:0]A;
  wire [17:0]B;
  wire CEA3;
  wire CEA4;
  wire CEB3;
  wire CEB4;
  wire CEM;
  wire CEP;
  wire CLK;
  wire [33:0]P;
  wire SCLR;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "131072" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_CEA = "12" *) 
  (* C_HAS_CEB = "12" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "1" *) 
  (* C_HAS_CEP = "1" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "2" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "33" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_dsp_delay_0_0_xbip_dsp48_macro_v3_0_16__parameterized1 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(1'b1),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(CEA3),
        .CEA4(CEA4),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(CEB3),
        .CEB4(CEB4),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(CEM),
        .CEP(CEP),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(SCLR),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* ORIG_REF_NAME = "dsp_delay_xldsp48e2" *) 
module design_1_dsp_delay_0_0_dsp_delay_xldsp48e2
   (d,
    count_reg_20_23,
    clk,
    rst_i,
    data_i);
  output [47:0]d;
  input [0:0]count_reg_20_23;
  input clk;
  input [0:0]rst_i;
  input [29:0]data_i;

  wire clk;
  wire [0:0]count_reg_20_23;
  wire [47:0]d;
  wire [29:0]data_i;
  wire dsp48e2_inst_n_0;
  wire dsp48e2_inst_n_1;
  wire dsp48e2_inst_n_10;
  wire dsp48e2_inst_n_106;
  wire dsp48e2_inst_n_107;
  wire dsp48e2_inst_n_108;
  wire dsp48e2_inst_n_109;
  wire dsp48e2_inst_n_11;
  wire dsp48e2_inst_n_110;
  wire dsp48e2_inst_n_111;
  wire dsp48e2_inst_n_112;
  wire dsp48e2_inst_n_113;
  wire dsp48e2_inst_n_114;
  wire dsp48e2_inst_n_115;
  wire dsp48e2_inst_n_116;
  wire dsp48e2_inst_n_117;
  wire dsp48e2_inst_n_118;
  wire dsp48e2_inst_n_119;
  wire dsp48e2_inst_n_12;
  wire dsp48e2_inst_n_120;
  wire dsp48e2_inst_n_121;
  wire dsp48e2_inst_n_122;
  wire dsp48e2_inst_n_123;
  wire dsp48e2_inst_n_124;
  wire dsp48e2_inst_n_125;
  wire dsp48e2_inst_n_126;
  wire dsp48e2_inst_n_127;
  wire dsp48e2_inst_n_128;
  wire dsp48e2_inst_n_129;
  wire dsp48e2_inst_n_13;
  wire dsp48e2_inst_n_130;
  wire dsp48e2_inst_n_131;
  wire dsp48e2_inst_n_132;
  wire dsp48e2_inst_n_133;
  wire dsp48e2_inst_n_134;
  wire dsp48e2_inst_n_135;
  wire dsp48e2_inst_n_136;
  wire dsp48e2_inst_n_137;
  wire dsp48e2_inst_n_138;
  wire dsp48e2_inst_n_139;
  wire dsp48e2_inst_n_14;
  wire dsp48e2_inst_n_140;
  wire dsp48e2_inst_n_141;
  wire dsp48e2_inst_n_142;
  wire dsp48e2_inst_n_143;
  wire dsp48e2_inst_n_144;
  wire dsp48e2_inst_n_145;
  wire dsp48e2_inst_n_146;
  wire dsp48e2_inst_n_147;
  wire dsp48e2_inst_n_148;
  wire dsp48e2_inst_n_149;
  wire dsp48e2_inst_n_15;
  wire dsp48e2_inst_n_150;
  wire dsp48e2_inst_n_151;
  wire dsp48e2_inst_n_152;
  wire dsp48e2_inst_n_153;
  wire dsp48e2_inst_n_154;
  wire dsp48e2_inst_n_155;
  wire dsp48e2_inst_n_156;
  wire dsp48e2_inst_n_157;
  wire dsp48e2_inst_n_158;
  wire dsp48e2_inst_n_159;
  wire dsp48e2_inst_n_16;
  wire dsp48e2_inst_n_160;
  wire dsp48e2_inst_n_161;
  wire dsp48e2_inst_n_17;
  wire dsp48e2_inst_n_18;
  wire dsp48e2_inst_n_19;
  wire dsp48e2_inst_n_2;
  wire dsp48e2_inst_n_20;
  wire dsp48e2_inst_n_21;
  wire dsp48e2_inst_n_22;
  wire dsp48e2_inst_n_23;
  wire dsp48e2_inst_n_24;
  wire dsp48e2_inst_n_25;
  wire dsp48e2_inst_n_26;
  wire dsp48e2_inst_n_27;
  wire dsp48e2_inst_n_28;
  wire dsp48e2_inst_n_29;
  wire dsp48e2_inst_n_3;
  wire dsp48e2_inst_n_30;
  wire dsp48e2_inst_n_31;
  wire dsp48e2_inst_n_32;
  wire dsp48e2_inst_n_33;
  wire dsp48e2_inst_n_34;
  wire dsp48e2_inst_n_35;
  wire dsp48e2_inst_n_36;
  wire dsp48e2_inst_n_37;
  wire dsp48e2_inst_n_38;
  wire dsp48e2_inst_n_39;
  wire dsp48e2_inst_n_4;
  wire dsp48e2_inst_n_40;
  wire dsp48e2_inst_n_41;
  wire dsp48e2_inst_n_42;
  wire dsp48e2_inst_n_43;
  wire dsp48e2_inst_n_44;
  wire dsp48e2_inst_n_45;
  wire dsp48e2_inst_n_46;
  wire dsp48e2_inst_n_47;
  wire dsp48e2_inst_n_48;
  wire dsp48e2_inst_n_49;
  wire dsp48e2_inst_n_5;
  wire dsp48e2_inst_n_50;
  wire dsp48e2_inst_n_51;
  wire dsp48e2_inst_n_52;
  wire dsp48e2_inst_n_53;
  wire dsp48e2_inst_n_54;
  wire dsp48e2_inst_n_6;
  wire dsp48e2_inst_n_7;
  wire dsp48e2_inst_n_8;
  wire dsp48e2_inst_n_9;
  wire [0:0]rst_i;
  wire [2:0]NLW_dsp48e2_inst_CARRYOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  DSP48E2 #(
    .ACASCREG(1),
    .ADREG(0),
    .ALUMODEREG(1),
    .AMULTSEL("A"),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .AUTORESET_PRIORITY("RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BMULTSEL("B"),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(1),
    .CARRYINSELREG(1),
    .CREG(0),
    .DREG(0),
    .INMODEREG(0),
    .IS_ALUMODE_INVERTED(4'b0000),
    .IS_CARRYIN_INVERTED(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_INMODE_INVERTED(5'b00000),
    .IS_OPMODE_INVERTED(9'b000000000),
    .IS_RSTALLCARRYIN_INVERTED(1'b0),
    .IS_RSTALUMODE_INVERTED(1'b0),
    .IS_RSTA_INVERTED(1'b0),
    .IS_RSTB_INVERTED(1'b0),
    .IS_RSTCTRL_INVERTED(1'b0),
    .IS_RSTC_INVERTED(1'b0),
    .IS_RSTD_INVERTED(1'b0),
    .IS_RSTINMODE_INVERTED(1'b0),
    .IS_RSTM_INVERTED(1'b0),
    .IS_RSTP_INVERTED(1'b0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(1),
    .OPMODEREG(1),
    .PATTERN(48'h000000000000),
    .PREADDINSEL("A"),
    .PREG(1),
    .RND(48'h000000000000),
    .SEL_MASK("C"),
    .SEL_PATTERN("C"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48"),
    .USE_WIDEXOR("FALSE"),
    .XORSIMD("XOR12")) 
    dsp48e2_inst
       (.A(data_i),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT({dsp48e2_inst_n_24,dsp48e2_inst_n_25,dsp48e2_inst_n_26,dsp48e2_inst_n_27,dsp48e2_inst_n_28,dsp48e2_inst_n_29,dsp48e2_inst_n_30,dsp48e2_inst_n_31,dsp48e2_inst_n_32,dsp48e2_inst_n_33,dsp48e2_inst_n_34,dsp48e2_inst_n_35,dsp48e2_inst_n_36,dsp48e2_inst_n_37,dsp48e2_inst_n_38,dsp48e2_inst_n_39,dsp48e2_inst_n_40,dsp48e2_inst_n_41,dsp48e2_inst_n_42,dsp48e2_inst_n_43,dsp48e2_inst_n_44,dsp48e2_inst_n_45,dsp48e2_inst_n_46,dsp48e2_inst_n_47,dsp48e2_inst_n_48,dsp48e2_inst_n_49,dsp48e2_inst_n_50,dsp48e2_inst_n_51,dsp48e2_inst_n_52,dsp48e2_inst_n_53}),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT({dsp48e2_inst_n_6,dsp48e2_inst_n_7,dsp48e2_inst_n_8,dsp48e2_inst_n_9,dsp48e2_inst_n_10,dsp48e2_inst_n_11,dsp48e2_inst_n_12,dsp48e2_inst_n_13,dsp48e2_inst_n_14,dsp48e2_inst_n_15,dsp48e2_inst_n_16,dsp48e2_inst_n_17,dsp48e2_inst_n_18,dsp48e2_inst_n_19,dsp48e2_inst_n_20,dsp48e2_inst_n_21,dsp48e2_inst_n_22,dsp48e2_inst_n_23}),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(dsp48e2_inst_n_0),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT({dsp48e2_inst_n_54,NLW_dsp48e2_inst_CARRYOUT_UNCONNECTED[2:0]}),
        .CEA1(1'b1),
        .CEA2(count_reg_20_23),
        .CEAD(count_reg_20_23),
        .CEALUMODE(count_reg_20_23),
        .CEB1(1'b1),
        .CEB2(count_reg_20_23),
        .CEC(1'b0),
        .CECARRYIN(count_reg_20_23),
        .CECTRL(count_reg_20_23),
        .CED(count_reg_20_23),
        .CEINMODE(count_reg_20_23),
        .CEM(count_reg_20_23),
        .CEP(count_reg_20_23),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(dsp48e2_inst_n_1),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(dsp48e2_inst_n_2),
        .P(d),
        .PATTERNBDETECT(dsp48e2_inst_n_3),
        .PATTERNDETECT(dsp48e2_inst_n_4),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({dsp48e2_inst_n_106,dsp48e2_inst_n_107,dsp48e2_inst_n_108,dsp48e2_inst_n_109,dsp48e2_inst_n_110,dsp48e2_inst_n_111,dsp48e2_inst_n_112,dsp48e2_inst_n_113,dsp48e2_inst_n_114,dsp48e2_inst_n_115,dsp48e2_inst_n_116,dsp48e2_inst_n_117,dsp48e2_inst_n_118,dsp48e2_inst_n_119,dsp48e2_inst_n_120,dsp48e2_inst_n_121,dsp48e2_inst_n_122,dsp48e2_inst_n_123,dsp48e2_inst_n_124,dsp48e2_inst_n_125,dsp48e2_inst_n_126,dsp48e2_inst_n_127,dsp48e2_inst_n_128,dsp48e2_inst_n_129,dsp48e2_inst_n_130,dsp48e2_inst_n_131,dsp48e2_inst_n_132,dsp48e2_inst_n_133,dsp48e2_inst_n_134,dsp48e2_inst_n_135,dsp48e2_inst_n_136,dsp48e2_inst_n_137,dsp48e2_inst_n_138,dsp48e2_inst_n_139,dsp48e2_inst_n_140,dsp48e2_inst_n_141,dsp48e2_inst_n_142,dsp48e2_inst_n_143,dsp48e2_inst_n_144,dsp48e2_inst_n_145,dsp48e2_inst_n_146,dsp48e2_inst_n_147,dsp48e2_inst_n_148,dsp48e2_inst_n_149,dsp48e2_inst_n_150,dsp48e2_inst_n_151,dsp48e2_inst_n_152,dsp48e2_inst_n_153}),
        .RSTA(rst_i),
        .RSTALLCARRYIN(rst_i),
        .RSTALUMODE(rst_i),
        .RSTB(rst_i),
        .RSTC(1'b1),
        .RSTCTRL(rst_i),
        .RSTD(rst_i),
        .RSTINMODE(rst_i),
        .RSTM(rst_i),
        .RSTP(rst_i),
        .UNDERFLOW(dsp48e2_inst_n_5),
        .XOROUT({dsp48e2_inst_n_154,dsp48e2_inst_n_155,dsp48e2_inst_n_156,dsp48e2_inst_n_157,dsp48e2_inst_n_158,dsp48e2_inst_n_159,dsp48e2_inst_n_160,dsp48e2_inst_n_161}));
endmodule

(* ORIG_REF_NAME = "dsp_delay_xldsp48e2" *) 
module design_1_dsp_delay_0_0_dsp_delay_xldsp48e2_0
   (I1,
    count_reg_20_23,
    clk,
    rst_i,
    data_i);
  output [47:0]I1;
  input [0:0]count_reg_20_23;
  input clk;
  input [0:0]rst_i;
  input [29:0]data_i;

  wire [47:0]I1;
  wire clk;
  wire [0:0]count_reg_20_23;
  wire [29:0]data_i;
  wire dsp48e2_inst_n_0;
  wire dsp48e2_inst_n_1;
  wire dsp48e2_inst_n_10;
  wire dsp48e2_inst_n_106;
  wire dsp48e2_inst_n_107;
  wire dsp48e2_inst_n_108;
  wire dsp48e2_inst_n_109;
  wire dsp48e2_inst_n_11;
  wire dsp48e2_inst_n_110;
  wire dsp48e2_inst_n_111;
  wire dsp48e2_inst_n_112;
  wire dsp48e2_inst_n_113;
  wire dsp48e2_inst_n_114;
  wire dsp48e2_inst_n_115;
  wire dsp48e2_inst_n_116;
  wire dsp48e2_inst_n_117;
  wire dsp48e2_inst_n_118;
  wire dsp48e2_inst_n_119;
  wire dsp48e2_inst_n_12;
  wire dsp48e2_inst_n_120;
  wire dsp48e2_inst_n_121;
  wire dsp48e2_inst_n_122;
  wire dsp48e2_inst_n_123;
  wire dsp48e2_inst_n_124;
  wire dsp48e2_inst_n_125;
  wire dsp48e2_inst_n_126;
  wire dsp48e2_inst_n_127;
  wire dsp48e2_inst_n_128;
  wire dsp48e2_inst_n_129;
  wire dsp48e2_inst_n_13;
  wire dsp48e2_inst_n_130;
  wire dsp48e2_inst_n_131;
  wire dsp48e2_inst_n_132;
  wire dsp48e2_inst_n_133;
  wire dsp48e2_inst_n_134;
  wire dsp48e2_inst_n_135;
  wire dsp48e2_inst_n_136;
  wire dsp48e2_inst_n_137;
  wire dsp48e2_inst_n_138;
  wire dsp48e2_inst_n_139;
  wire dsp48e2_inst_n_14;
  wire dsp48e2_inst_n_140;
  wire dsp48e2_inst_n_141;
  wire dsp48e2_inst_n_142;
  wire dsp48e2_inst_n_143;
  wire dsp48e2_inst_n_144;
  wire dsp48e2_inst_n_145;
  wire dsp48e2_inst_n_146;
  wire dsp48e2_inst_n_147;
  wire dsp48e2_inst_n_148;
  wire dsp48e2_inst_n_149;
  wire dsp48e2_inst_n_15;
  wire dsp48e2_inst_n_150;
  wire dsp48e2_inst_n_151;
  wire dsp48e2_inst_n_152;
  wire dsp48e2_inst_n_153;
  wire dsp48e2_inst_n_154;
  wire dsp48e2_inst_n_155;
  wire dsp48e2_inst_n_156;
  wire dsp48e2_inst_n_157;
  wire dsp48e2_inst_n_158;
  wire dsp48e2_inst_n_159;
  wire dsp48e2_inst_n_16;
  wire dsp48e2_inst_n_160;
  wire dsp48e2_inst_n_161;
  wire dsp48e2_inst_n_17;
  wire dsp48e2_inst_n_18;
  wire dsp48e2_inst_n_19;
  wire dsp48e2_inst_n_2;
  wire dsp48e2_inst_n_20;
  wire dsp48e2_inst_n_21;
  wire dsp48e2_inst_n_22;
  wire dsp48e2_inst_n_23;
  wire dsp48e2_inst_n_24;
  wire dsp48e2_inst_n_25;
  wire dsp48e2_inst_n_26;
  wire dsp48e2_inst_n_27;
  wire dsp48e2_inst_n_28;
  wire dsp48e2_inst_n_29;
  wire dsp48e2_inst_n_3;
  wire dsp48e2_inst_n_30;
  wire dsp48e2_inst_n_31;
  wire dsp48e2_inst_n_32;
  wire dsp48e2_inst_n_33;
  wire dsp48e2_inst_n_34;
  wire dsp48e2_inst_n_35;
  wire dsp48e2_inst_n_36;
  wire dsp48e2_inst_n_37;
  wire dsp48e2_inst_n_38;
  wire dsp48e2_inst_n_39;
  wire dsp48e2_inst_n_4;
  wire dsp48e2_inst_n_40;
  wire dsp48e2_inst_n_41;
  wire dsp48e2_inst_n_42;
  wire dsp48e2_inst_n_43;
  wire dsp48e2_inst_n_44;
  wire dsp48e2_inst_n_45;
  wire dsp48e2_inst_n_46;
  wire dsp48e2_inst_n_47;
  wire dsp48e2_inst_n_48;
  wire dsp48e2_inst_n_49;
  wire dsp48e2_inst_n_5;
  wire dsp48e2_inst_n_50;
  wire dsp48e2_inst_n_51;
  wire dsp48e2_inst_n_52;
  wire dsp48e2_inst_n_53;
  wire dsp48e2_inst_n_54;
  wire dsp48e2_inst_n_6;
  wire dsp48e2_inst_n_7;
  wire dsp48e2_inst_n_8;
  wire dsp48e2_inst_n_9;
  wire [0:0]rst_i;
  wire [2:0]NLW_dsp48e2_inst_CARRYOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  DSP48E2 #(
    .ACASCREG(1),
    .ADREG(0),
    .ALUMODEREG(1),
    .AMULTSEL("A"),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .AUTORESET_PRIORITY("RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BMULTSEL("B"),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(1),
    .CARRYINSELREG(1),
    .CREG(0),
    .DREG(0),
    .INMODEREG(0),
    .IS_ALUMODE_INVERTED(4'b0000),
    .IS_CARRYIN_INVERTED(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_INMODE_INVERTED(5'b00000),
    .IS_OPMODE_INVERTED(9'b000000000),
    .IS_RSTALLCARRYIN_INVERTED(1'b0),
    .IS_RSTALUMODE_INVERTED(1'b0),
    .IS_RSTA_INVERTED(1'b0),
    .IS_RSTB_INVERTED(1'b0),
    .IS_RSTCTRL_INVERTED(1'b0),
    .IS_RSTC_INVERTED(1'b0),
    .IS_RSTD_INVERTED(1'b0),
    .IS_RSTINMODE_INVERTED(1'b0),
    .IS_RSTM_INVERTED(1'b0),
    .IS_RSTP_INVERTED(1'b0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(1),
    .OPMODEREG(1),
    .PATTERN(48'h000000000000),
    .PREADDINSEL("A"),
    .PREG(1),
    .RND(48'h000000000000),
    .SEL_MASK("C"),
    .SEL_PATTERN("C"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48"),
    .USE_WIDEXOR("FALSE"),
    .XORSIMD("XOR12")) 
    dsp48e2_inst
       (.A(data_i),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT({dsp48e2_inst_n_24,dsp48e2_inst_n_25,dsp48e2_inst_n_26,dsp48e2_inst_n_27,dsp48e2_inst_n_28,dsp48e2_inst_n_29,dsp48e2_inst_n_30,dsp48e2_inst_n_31,dsp48e2_inst_n_32,dsp48e2_inst_n_33,dsp48e2_inst_n_34,dsp48e2_inst_n_35,dsp48e2_inst_n_36,dsp48e2_inst_n_37,dsp48e2_inst_n_38,dsp48e2_inst_n_39,dsp48e2_inst_n_40,dsp48e2_inst_n_41,dsp48e2_inst_n_42,dsp48e2_inst_n_43,dsp48e2_inst_n_44,dsp48e2_inst_n_45,dsp48e2_inst_n_46,dsp48e2_inst_n_47,dsp48e2_inst_n_48,dsp48e2_inst_n_49,dsp48e2_inst_n_50,dsp48e2_inst_n_51,dsp48e2_inst_n_52,dsp48e2_inst_n_53}),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT({dsp48e2_inst_n_6,dsp48e2_inst_n_7,dsp48e2_inst_n_8,dsp48e2_inst_n_9,dsp48e2_inst_n_10,dsp48e2_inst_n_11,dsp48e2_inst_n_12,dsp48e2_inst_n_13,dsp48e2_inst_n_14,dsp48e2_inst_n_15,dsp48e2_inst_n_16,dsp48e2_inst_n_17,dsp48e2_inst_n_18,dsp48e2_inst_n_19,dsp48e2_inst_n_20,dsp48e2_inst_n_21,dsp48e2_inst_n_22,dsp48e2_inst_n_23}),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(dsp48e2_inst_n_0),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT({dsp48e2_inst_n_54,NLW_dsp48e2_inst_CARRYOUT_UNCONNECTED[2:0]}),
        .CEA1(1'b1),
        .CEA2(count_reg_20_23),
        .CEAD(1'b1),
        .CEALUMODE(1'b1),
        .CEB1(1'b1),
        .CEB2(count_reg_20_23),
        .CEC(1'b0),
        .CECARRYIN(1'b1),
        .CECTRL(1'b1),
        .CED(1'b1),
        .CEINMODE(1'b1),
        .CEM(count_reg_20_23),
        .CEP(count_reg_20_23),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(dsp48e2_inst_n_1),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(dsp48e2_inst_n_2),
        .P(I1),
        .PATTERNBDETECT(dsp48e2_inst_n_3),
        .PATTERNDETECT(dsp48e2_inst_n_4),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({dsp48e2_inst_n_106,dsp48e2_inst_n_107,dsp48e2_inst_n_108,dsp48e2_inst_n_109,dsp48e2_inst_n_110,dsp48e2_inst_n_111,dsp48e2_inst_n_112,dsp48e2_inst_n_113,dsp48e2_inst_n_114,dsp48e2_inst_n_115,dsp48e2_inst_n_116,dsp48e2_inst_n_117,dsp48e2_inst_n_118,dsp48e2_inst_n_119,dsp48e2_inst_n_120,dsp48e2_inst_n_121,dsp48e2_inst_n_122,dsp48e2_inst_n_123,dsp48e2_inst_n_124,dsp48e2_inst_n_125,dsp48e2_inst_n_126,dsp48e2_inst_n_127,dsp48e2_inst_n_128,dsp48e2_inst_n_129,dsp48e2_inst_n_130,dsp48e2_inst_n_131,dsp48e2_inst_n_132,dsp48e2_inst_n_133,dsp48e2_inst_n_134,dsp48e2_inst_n_135,dsp48e2_inst_n_136,dsp48e2_inst_n_137,dsp48e2_inst_n_138,dsp48e2_inst_n_139,dsp48e2_inst_n_140,dsp48e2_inst_n_141,dsp48e2_inst_n_142,dsp48e2_inst_n_143,dsp48e2_inst_n_144,dsp48e2_inst_n_145,dsp48e2_inst_n_146,dsp48e2_inst_n_147,dsp48e2_inst_n_148,dsp48e2_inst_n_149,dsp48e2_inst_n_150,dsp48e2_inst_n_151,dsp48e2_inst_n_152,dsp48e2_inst_n_153}),
        .RSTA(rst_i),
        .RSTALLCARRYIN(rst_i),
        .RSTALUMODE(rst_i),
        .RSTB(rst_i),
        .RSTC(1'b1),
        .RSTCTRL(rst_i),
        .RSTD(rst_i),
        .RSTINMODE(rst_i),
        .RSTM(rst_i),
        .RSTP(rst_i),
        .UNDERFLOW(dsp48e2_inst_n_5),
        .XOROUT({dsp48e2_inst_n_154,dsp48e2_inst_n_155,dsp48e2_inst_n_156,dsp48e2_inst_n_157,dsp48e2_inst_n_158,dsp48e2_inst_n_159,dsp48e2_inst_n_160,dsp48e2_inst_n_161}));
endmodule

(* ORIG_REF_NAME = "sysgen_counter_3f6b639172" *) 
module design_1_dsp_delay_0_0_sysgen_counter_3f6b639172
   (count_reg_20_23,
    rst_i,
    clk);
  output [0:0]count_reg_20_23;
  input [0:0]rst_i;
  input clk;

  wire clk;
  wire [0:0]count_reg_20_23;
  wire p_1_in;
  wire [0:0]rst_i;

  LUT1 #(
    .INIT(2'h1)) 
    \count_reg_20_23[0]_i_1 
       (.I0(count_reg_20_23),
        .O(p_1_in));
  FDSE #(
    .INIT(1'b1)) 
    \count_reg_20_23_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(p_1_in),
        .Q(count_reg_20_23),
        .S(rst_i));
endmodule

(* ORIG_REF_NAME = "sysgen_delay_13814cb9b3" *) 
module design_1_dsp_delay_0_0_sysgen_delay_13814cb9b3
   (data_direct_indven_o,
    rst_i,
    count_reg_20_23,
    D,
    clk);
  output [47:0]data_direct_indven_o;
  input [0:0]rst_i;
  input [0:0]count_reg_20_23;
  input [47:0]D;
  input clk;

  wire [47:0]D;
  wire clk;
  wire [0:0]count_reg_20_23;
  wire [47:0]data_direct_indven_o;
  wire [47:0]op_mem_0_8_24;
  wire [0:0]rst_i;

  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[0] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[0]),
        .Q(op_mem_0_8_24[0]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[10] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[10]),
        .Q(op_mem_0_8_24[10]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[11] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[11]),
        .Q(op_mem_0_8_24[11]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[12] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[12]),
        .Q(op_mem_0_8_24[12]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[13] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[13]),
        .Q(op_mem_0_8_24[13]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[14] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[14]),
        .Q(op_mem_0_8_24[14]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[15] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[15]),
        .Q(op_mem_0_8_24[15]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[16] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[16]),
        .Q(op_mem_0_8_24[16]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[17] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[17]),
        .Q(op_mem_0_8_24[17]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[18] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[18]),
        .Q(op_mem_0_8_24[18]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[19] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[19]),
        .Q(op_mem_0_8_24[19]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[1] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[1]),
        .Q(op_mem_0_8_24[1]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[20] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[20]),
        .Q(op_mem_0_8_24[20]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[21] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[21]),
        .Q(op_mem_0_8_24[21]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[22] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[22]),
        .Q(op_mem_0_8_24[22]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[23] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[23]),
        .Q(op_mem_0_8_24[23]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[24] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[24]),
        .Q(op_mem_0_8_24[24]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[25] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[25]),
        .Q(op_mem_0_8_24[25]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[26] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[26]),
        .Q(op_mem_0_8_24[26]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[27] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[27]),
        .Q(op_mem_0_8_24[27]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[28] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[28]),
        .Q(op_mem_0_8_24[28]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[29] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[29]),
        .Q(op_mem_0_8_24[29]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[2] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[2]),
        .Q(op_mem_0_8_24[2]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[30] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[30]),
        .Q(op_mem_0_8_24[30]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[31] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[31]),
        .Q(op_mem_0_8_24[31]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[32] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[32]),
        .Q(op_mem_0_8_24[32]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[33] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[33]),
        .Q(op_mem_0_8_24[33]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[34] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[34]),
        .Q(op_mem_0_8_24[34]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[35] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[35]),
        .Q(op_mem_0_8_24[35]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[36] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[36]),
        .Q(op_mem_0_8_24[36]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[37] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[37]),
        .Q(op_mem_0_8_24[37]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[38] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[38]),
        .Q(op_mem_0_8_24[38]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[39] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[39]),
        .Q(op_mem_0_8_24[39]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[3] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[3]),
        .Q(op_mem_0_8_24[3]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[40] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[40]),
        .Q(op_mem_0_8_24[40]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[41] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[41]),
        .Q(op_mem_0_8_24[41]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[42] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[42]),
        .Q(op_mem_0_8_24[42]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[43] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[43]),
        .Q(op_mem_0_8_24[43]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[44] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[44]),
        .Q(op_mem_0_8_24[44]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[45] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[45]),
        .Q(op_mem_0_8_24[45]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[46] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[46]),
        .Q(op_mem_0_8_24[46]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[47] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[47]),
        .Q(op_mem_0_8_24[47]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[4] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[4]),
        .Q(op_mem_0_8_24[4]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[5] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[5]),
        .Q(op_mem_0_8_24[5]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[6] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[6]),
        .Q(op_mem_0_8_24[6]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[7] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[7]),
        .Q(op_mem_0_8_24[7]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[8] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[8]),
        .Q(op_mem_0_8_24[8]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[9] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[9]),
        .Q(op_mem_0_8_24[9]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[0] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[0]),
        .Q(data_direct_indven_o[0]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[10] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[10]),
        .Q(data_direct_indven_o[10]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[11] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[11]),
        .Q(data_direct_indven_o[11]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[12] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[12]),
        .Q(data_direct_indven_o[12]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[13] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[13]),
        .Q(data_direct_indven_o[13]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[14] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[14]),
        .Q(data_direct_indven_o[14]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[15] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[15]),
        .Q(data_direct_indven_o[15]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[16] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[16]),
        .Q(data_direct_indven_o[16]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[17] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[17]),
        .Q(data_direct_indven_o[17]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[18] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[18]),
        .Q(data_direct_indven_o[18]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[19] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[19]),
        .Q(data_direct_indven_o[19]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[1] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[1]),
        .Q(data_direct_indven_o[1]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[20] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[20]),
        .Q(data_direct_indven_o[20]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[21] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[21]),
        .Q(data_direct_indven_o[21]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[22] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[22]),
        .Q(data_direct_indven_o[22]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[23] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[23]),
        .Q(data_direct_indven_o[23]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[24] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[24]),
        .Q(data_direct_indven_o[24]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[25] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[25]),
        .Q(data_direct_indven_o[25]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[26] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[26]),
        .Q(data_direct_indven_o[26]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[27] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[27]),
        .Q(data_direct_indven_o[27]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[28] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[28]),
        .Q(data_direct_indven_o[28]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[29] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[29]),
        .Q(data_direct_indven_o[29]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[2] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[2]),
        .Q(data_direct_indven_o[2]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[30] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[30]),
        .Q(data_direct_indven_o[30]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[31] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[31]),
        .Q(data_direct_indven_o[31]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[32] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[32]),
        .Q(data_direct_indven_o[32]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[33] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[33]),
        .Q(data_direct_indven_o[33]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[34] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[34]),
        .Q(data_direct_indven_o[34]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[35] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[35]),
        .Q(data_direct_indven_o[35]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[36] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[36]),
        .Q(data_direct_indven_o[36]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[37] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[37]),
        .Q(data_direct_indven_o[37]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[38] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[38]),
        .Q(data_direct_indven_o[38]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[39] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[39]),
        .Q(data_direct_indven_o[39]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[3] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[3]),
        .Q(data_direct_indven_o[3]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[40] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[40]),
        .Q(data_direct_indven_o[40]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[41] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[41]),
        .Q(data_direct_indven_o[41]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[42] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[42]),
        .Q(data_direct_indven_o[42]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[43] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[43]),
        .Q(data_direct_indven_o[43]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[44] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[44]),
        .Q(data_direct_indven_o[44]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[45] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[45]),
        .Q(data_direct_indven_o[45]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[46] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[46]),
        .Q(data_direct_indven_o[46]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[47] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[47]),
        .Q(data_direct_indven_o[47]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[4] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[4]),
        .Q(data_direct_indven_o[4]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[5] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[5]),
        .Q(data_direct_indven_o[5]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[6] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[6]),
        .Q(data_direct_indven_o[6]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[7] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[7]),
        .Q(data_direct_indven_o[7]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[8] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[8]),
        .Q(data_direct_indven_o[8]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_1_8_24_reg[9] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(op_mem_0_8_24[9]),
        .Q(data_direct_indven_o[9]),
        .R(rst_i));
endmodule

(* ORIG_REF_NAME = "sysgen_delay_2eda9975a6" *) 
module design_1_dsp_delay_0_0_sysgen_delay_2eda9975a6
   (data_direct_globalen_o,
    rst_i,
    count_reg_20_23,
    D,
    clk);
  output [47:0]data_direct_globalen_o;
  input [0:0]rst_i;
  input [0:0]count_reg_20_23;
  input [47:0]D;
  input clk;

  wire [47:0]D;
  wire clk;
  wire [0:0]count_reg_20_23;
  wire [47:0]data_direct_globalen_o;
  wire [0:0]rst_i;

  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[0] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[0]),
        .Q(data_direct_globalen_o[0]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[10] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[10]),
        .Q(data_direct_globalen_o[10]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[11] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[11]),
        .Q(data_direct_globalen_o[11]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[12] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[12]),
        .Q(data_direct_globalen_o[12]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[13] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[13]),
        .Q(data_direct_globalen_o[13]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[14] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[14]),
        .Q(data_direct_globalen_o[14]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[15] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[15]),
        .Q(data_direct_globalen_o[15]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[16] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[16]),
        .Q(data_direct_globalen_o[16]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[17] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[17]),
        .Q(data_direct_globalen_o[17]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[18] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[18]),
        .Q(data_direct_globalen_o[18]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[19] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[19]),
        .Q(data_direct_globalen_o[19]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[1] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[1]),
        .Q(data_direct_globalen_o[1]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[20] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[20]),
        .Q(data_direct_globalen_o[20]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[21] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[21]),
        .Q(data_direct_globalen_o[21]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[22] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[22]),
        .Q(data_direct_globalen_o[22]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[23] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[23]),
        .Q(data_direct_globalen_o[23]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[24] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[24]),
        .Q(data_direct_globalen_o[24]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[25] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[25]),
        .Q(data_direct_globalen_o[25]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[26] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[26]),
        .Q(data_direct_globalen_o[26]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[27] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[27]),
        .Q(data_direct_globalen_o[27]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[28] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[28]),
        .Q(data_direct_globalen_o[28]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[29] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[29]),
        .Q(data_direct_globalen_o[29]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[2] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[2]),
        .Q(data_direct_globalen_o[2]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[30] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[30]),
        .Q(data_direct_globalen_o[30]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[31] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[31]),
        .Q(data_direct_globalen_o[31]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[32] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[32]),
        .Q(data_direct_globalen_o[32]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[33] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[33]),
        .Q(data_direct_globalen_o[33]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[34] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[34]),
        .Q(data_direct_globalen_o[34]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[35] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[35]),
        .Q(data_direct_globalen_o[35]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[36] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[36]),
        .Q(data_direct_globalen_o[36]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[37] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[37]),
        .Q(data_direct_globalen_o[37]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[38] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[38]),
        .Q(data_direct_globalen_o[38]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[39] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[39]),
        .Q(data_direct_globalen_o[39]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[3] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[3]),
        .Q(data_direct_globalen_o[3]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[40] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[40]),
        .Q(data_direct_globalen_o[40]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[41] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[41]),
        .Q(data_direct_globalen_o[41]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[42] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[42]),
        .Q(data_direct_globalen_o[42]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[43] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[43]),
        .Q(data_direct_globalen_o[43]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[44] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[44]),
        .Q(data_direct_globalen_o[44]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[45] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[45]),
        .Q(data_direct_globalen_o[45]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[46] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[46]),
        .Q(data_direct_globalen_o[46]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[47] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[47]),
        .Q(data_direct_globalen_o[47]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[4] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[4]),
        .Q(data_direct_globalen_o[4]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[5] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[5]),
        .Q(data_direct_globalen_o[5]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[6] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[6]),
        .Q(data_direct_globalen_o[6]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[7] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[7]),
        .Q(data_direct_globalen_o[7]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[8] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[8]),
        .Q(data_direct_globalen_o[8]),
        .R(rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_0_8_24_reg[9] 
       (.C(clk),
        .CE(count_reg_20_23),
        .D(D[9]),
        .Q(data_direct_globalen_o[9]),
        .R(rst_i));
endmodule

(* ORIG_REF_NAME = "xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2" *) 
module design_1_dsp_delay_0_0_xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2
   (data_macro_indven_o,
    clk,
    rst_i,
    data_i,
    count_reg_20_23);
  output [33:0]data_macro_indven_o;
  input clk;
  input [0:0]rst_i;
  input [15:0]data_i;
  input [0:0]count_reg_20_23;

  wire clk;
  wire [0:0]count_reg_20_23;
  wire [15:0]data_i;
  wire [33:0]data_macro_indven_o;
  wire [0:0]rst_i;

  (* CHECK_LICENSE_TYPE = "dsp_delay_xbip_dsp48_macro_v3_0_i1,xbip_dsp48_macro_v3_0_16,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
  design_1_dsp_delay_0_0_dsp_delay_xbip_dsp48_macro_v3_0_i1 dsp_delay_xbip_dsp48_macro_v3_0_i1_instance
       (.A(data_i),
        .B({1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CEA3(count_reg_20_23),
        .CEA4(count_reg_20_23),
        .CEB3(count_reg_20_23),
        .CEB4(count_reg_20_23),
        .CEM(count_reg_20_23),
        .CEP(count_reg_20_23),
        .CLK(clk),
        .P(data_macro_indven_o),
        .SCLR(rst_i));
endmodule

(* ORIG_REF_NAME = "xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e" *) 
module design_1_dsp_delay_0_0_xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e
   (data_macro_globalen_o,
    clk,
    count_reg_20_23,
    rst_i,
    data_i);
  output [33:0]data_macro_globalen_o;
  input clk;
  input [0:0]count_reg_20_23;
  input [0:0]rst_i;
  input [15:0]data_i;

  wire clk;
  wire [0:0]count_reg_20_23;
  wire [15:0]data_i;
  wire [33:0]data_macro_globalen_o;
  wire [0:0]rst_i;

  (* CHECK_LICENSE_TYPE = "dsp_delay_xbip_dsp48_macro_v3_0_i0,xbip_dsp48_macro_v3_0_16,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
  design_1_dsp_delay_0_0_dsp_delay_xbip_dsp48_macro_v3_0_i0 dsp_delay_xbip_dsp48_macro_v3_0_i0_instance
       (.A(data_i),
        .B({1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(count_reg_20_23),
        .CLK(clk),
        .P(data_macro_globalen_o),
        .SCLR(rst_i));
endmodule

(* C_A_WIDTH = "16" *) (* C_B_WIDTH = "18" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "131072" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "0" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "1" *) 
(* C_HAS_CEA = "0" *) (* C_HAS_CEB = "0" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "0" *) 
(* C_HAS_CEP = "0" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "0" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000100100000010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "33" *) (* C_REG_CONFIG = "00000000000011000011000001000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "kintexu" *) 
(* ORIG_REF_NAME = "xbip_dsp48_macro_v3_0_16" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_1_dsp_delay_0_0_xbip_dsp48_macro_v3_0_16
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [15:0]A;
  input [17:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [33:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire \<const0> ;
  wire [15:0]A;
  wire [17:0]B;
  wire CE;
  wire CLK;
  wire [33:0]P;
  wire SCLR;
  wire NLW_i_synth_CARRYCASCOUT_UNCONNECTED;
  wire NLW_i_synth_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_i_synth_ACOUT_UNCONNECTED;
  wire [17:0]NLW_i_synth_BCOUT_UNCONNECTED;
  wire [47:0]NLW_i_synth_PCOUT_UNCONNECTED;

  assign ACOUT[29] = \<const0> ;
  assign ACOUT[28] = \<const0> ;
  assign ACOUT[27] = \<const0> ;
  assign ACOUT[26] = \<const0> ;
  assign ACOUT[25] = \<const0> ;
  assign ACOUT[24] = \<const0> ;
  assign ACOUT[23] = \<const0> ;
  assign ACOUT[22] = \<const0> ;
  assign ACOUT[21] = \<const0> ;
  assign ACOUT[20] = \<const0> ;
  assign ACOUT[19] = \<const0> ;
  assign ACOUT[18] = \<const0> ;
  assign ACOUT[17] = \<const0> ;
  assign ACOUT[16] = \<const0> ;
  assign ACOUT[15] = \<const0> ;
  assign ACOUT[14] = \<const0> ;
  assign ACOUT[13] = \<const0> ;
  assign ACOUT[12] = \<const0> ;
  assign ACOUT[11] = \<const0> ;
  assign ACOUT[10] = \<const0> ;
  assign ACOUT[9] = \<const0> ;
  assign ACOUT[8] = \<const0> ;
  assign ACOUT[7] = \<const0> ;
  assign ACOUT[6] = \<const0> ;
  assign ACOUT[5] = \<const0> ;
  assign ACOUT[4] = \<const0> ;
  assign ACOUT[3] = \<const0> ;
  assign ACOUT[2] = \<const0> ;
  assign ACOUT[1] = \<const0> ;
  assign ACOUT[0] = \<const0> ;
  assign BCOUT[17] = \<const0> ;
  assign BCOUT[16] = \<const0> ;
  assign BCOUT[15] = \<const0> ;
  assign BCOUT[14] = \<const0> ;
  assign BCOUT[13] = \<const0> ;
  assign BCOUT[12] = \<const0> ;
  assign BCOUT[11] = \<const0> ;
  assign BCOUT[10] = \<const0> ;
  assign BCOUT[9] = \<const0> ;
  assign BCOUT[8] = \<const0> ;
  assign BCOUT[7] = \<const0> ;
  assign BCOUT[6] = \<const0> ;
  assign BCOUT[5] = \<const0> ;
  assign BCOUT[4] = \<const0> ;
  assign BCOUT[3] = \<const0> ;
  assign BCOUT[2] = \<const0> ;
  assign BCOUT[1] = \<const0> ;
  assign BCOUT[0] = \<const0> ;
  assign CARRYCASCOUT = \<const0> ;
  assign CARRYOUT = \<const0> ;
  assign PCOUT[47] = \<const0> ;
  assign PCOUT[46] = \<const0> ;
  assign PCOUT[45] = \<const0> ;
  assign PCOUT[44] = \<const0> ;
  assign PCOUT[43] = \<const0> ;
  assign PCOUT[42] = \<const0> ;
  assign PCOUT[41] = \<const0> ;
  assign PCOUT[40] = \<const0> ;
  assign PCOUT[39] = \<const0> ;
  assign PCOUT[38] = \<const0> ;
  assign PCOUT[37] = \<const0> ;
  assign PCOUT[36] = \<const0> ;
  assign PCOUT[35] = \<const0> ;
  assign PCOUT[34] = \<const0> ;
  assign PCOUT[33] = \<const0> ;
  assign PCOUT[32] = \<const0> ;
  assign PCOUT[31] = \<const0> ;
  assign PCOUT[30] = \<const0> ;
  assign PCOUT[29] = \<const0> ;
  assign PCOUT[28] = \<const0> ;
  assign PCOUT[27] = \<const0> ;
  assign PCOUT[26] = \<const0> ;
  assign PCOUT[25] = \<const0> ;
  assign PCOUT[24] = \<const0> ;
  assign PCOUT[23] = \<const0> ;
  assign PCOUT[22] = \<const0> ;
  assign PCOUT[21] = \<const0> ;
  assign PCOUT[20] = \<const0> ;
  assign PCOUT[19] = \<const0> ;
  assign PCOUT[18] = \<const0> ;
  assign PCOUT[17] = \<const0> ;
  assign PCOUT[16] = \<const0> ;
  assign PCOUT[15] = \<const0> ;
  assign PCOUT[14] = \<const0> ;
  assign PCOUT[13] = \<const0> ;
  assign PCOUT[12] = \<const0> ;
  assign PCOUT[11] = \<const0> ;
  assign PCOUT[10] = \<const0> ;
  assign PCOUT[9] = \<const0> ;
  assign PCOUT[8] = \<const0> ;
  assign PCOUT[7] = \<const0> ;
  assign PCOUT[6] = \<const0> ;
  assign PCOUT[5] = \<const0> ;
  assign PCOUT[4] = \<const0> ;
  assign PCOUT[3] = \<const0> ;
  assign PCOUT[2] = \<const0> ;
  assign PCOUT[1] = \<const0> ;
  assign PCOUT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "131072" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "33" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_dsp_delay_0_0_xbip_dsp48_macro_v3_0_16_viv i_synth
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_i_synth_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_i_synth_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_i_synth_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_i_synth_CARRYOUT_UNCONNECTED),
        .CE(CE),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(1'b0),
        .CEA4(1'b0),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(1'b0),
        .CEB4(1'b0),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_i_synth_PCOUT_UNCONNECTED[47:0]),
        .SCLR(SCLR),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* C_A_WIDTH = "16" *) (* C_B_WIDTH = "18" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "131072" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "0" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "1" *) 
(* C_HAS_CEA = "12" *) (* C_HAS_CEB = "12" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "1" *) 
(* C_HAS_CEP = "1" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "2" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000100100000010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "33" *) (* C_REG_CONFIG = "00000000000011000011000001000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "kintexu" *) 
(* ORIG_REF_NAME = "xbip_dsp48_macro_v3_0_16" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_1_dsp_delay_0_0_xbip_dsp48_macro_v3_0_16__parameterized1
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [15:0]A;
  input [17:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [33:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire \<const0> ;
  wire [15:0]A;
  wire [17:0]B;
  wire CEA3;
  wire CEA4;
  wire CEB3;
  wire CEB4;
  wire CEM;
  wire CEP;
  wire CLK;
  wire [33:0]P;
  wire SCLR;
  wire NLW_i_synth_CARRYCASCOUT_UNCONNECTED;
  wire NLW_i_synth_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_i_synth_ACOUT_UNCONNECTED;
  wire [17:0]NLW_i_synth_BCOUT_UNCONNECTED;
  wire [47:0]NLW_i_synth_PCOUT_UNCONNECTED;

  assign ACOUT[29] = \<const0> ;
  assign ACOUT[28] = \<const0> ;
  assign ACOUT[27] = \<const0> ;
  assign ACOUT[26] = \<const0> ;
  assign ACOUT[25] = \<const0> ;
  assign ACOUT[24] = \<const0> ;
  assign ACOUT[23] = \<const0> ;
  assign ACOUT[22] = \<const0> ;
  assign ACOUT[21] = \<const0> ;
  assign ACOUT[20] = \<const0> ;
  assign ACOUT[19] = \<const0> ;
  assign ACOUT[18] = \<const0> ;
  assign ACOUT[17] = \<const0> ;
  assign ACOUT[16] = \<const0> ;
  assign ACOUT[15] = \<const0> ;
  assign ACOUT[14] = \<const0> ;
  assign ACOUT[13] = \<const0> ;
  assign ACOUT[12] = \<const0> ;
  assign ACOUT[11] = \<const0> ;
  assign ACOUT[10] = \<const0> ;
  assign ACOUT[9] = \<const0> ;
  assign ACOUT[8] = \<const0> ;
  assign ACOUT[7] = \<const0> ;
  assign ACOUT[6] = \<const0> ;
  assign ACOUT[5] = \<const0> ;
  assign ACOUT[4] = \<const0> ;
  assign ACOUT[3] = \<const0> ;
  assign ACOUT[2] = \<const0> ;
  assign ACOUT[1] = \<const0> ;
  assign ACOUT[0] = \<const0> ;
  assign BCOUT[17] = \<const0> ;
  assign BCOUT[16] = \<const0> ;
  assign BCOUT[15] = \<const0> ;
  assign BCOUT[14] = \<const0> ;
  assign BCOUT[13] = \<const0> ;
  assign BCOUT[12] = \<const0> ;
  assign BCOUT[11] = \<const0> ;
  assign BCOUT[10] = \<const0> ;
  assign BCOUT[9] = \<const0> ;
  assign BCOUT[8] = \<const0> ;
  assign BCOUT[7] = \<const0> ;
  assign BCOUT[6] = \<const0> ;
  assign BCOUT[5] = \<const0> ;
  assign BCOUT[4] = \<const0> ;
  assign BCOUT[3] = \<const0> ;
  assign BCOUT[2] = \<const0> ;
  assign BCOUT[1] = \<const0> ;
  assign BCOUT[0] = \<const0> ;
  assign CARRYCASCOUT = \<const0> ;
  assign CARRYOUT = \<const0> ;
  assign PCOUT[47] = \<const0> ;
  assign PCOUT[46] = \<const0> ;
  assign PCOUT[45] = \<const0> ;
  assign PCOUT[44] = \<const0> ;
  assign PCOUT[43] = \<const0> ;
  assign PCOUT[42] = \<const0> ;
  assign PCOUT[41] = \<const0> ;
  assign PCOUT[40] = \<const0> ;
  assign PCOUT[39] = \<const0> ;
  assign PCOUT[38] = \<const0> ;
  assign PCOUT[37] = \<const0> ;
  assign PCOUT[36] = \<const0> ;
  assign PCOUT[35] = \<const0> ;
  assign PCOUT[34] = \<const0> ;
  assign PCOUT[33] = \<const0> ;
  assign PCOUT[32] = \<const0> ;
  assign PCOUT[31] = \<const0> ;
  assign PCOUT[30] = \<const0> ;
  assign PCOUT[29] = \<const0> ;
  assign PCOUT[28] = \<const0> ;
  assign PCOUT[27] = \<const0> ;
  assign PCOUT[26] = \<const0> ;
  assign PCOUT[25] = \<const0> ;
  assign PCOUT[24] = \<const0> ;
  assign PCOUT[23] = \<const0> ;
  assign PCOUT[22] = \<const0> ;
  assign PCOUT[21] = \<const0> ;
  assign PCOUT[20] = \<const0> ;
  assign PCOUT[19] = \<const0> ;
  assign PCOUT[18] = \<const0> ;
  assign PCOUT[17] = \<const0> ;
  assign PCOUT[16] = \<const0> ;
  assign PCOUT[15] = \<const0> ;
  assign PCOUT[14] = \<const0> ;
  assign PCOUT[13] = \<const0> ;
  assign PCOUT[12] = \<const0> ;
  assign PCOUT[11] = \<const0> ;
  assign PCOUT[10] = \<const0> ;
  assign PCOUT[9] = \<const0> ;
  assign PCOUT[8] = \<const0> ;
  assign PCOUT[7] = \<const0> ;
  assign PCOUT[6] = \<const0> ;
  assign PCOUT[5] = \<const0> ;
  assign PCOUT[4] = \<const0> ;
  assign PCOUT[3] = \<const0> ;
  assign PCOUT[2] = \<const0> ;
  assign PCOUT[1] = \<const0> ;
  assign PCOUT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "131072" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_CEA = "12" *) 
  (* C_HAS_CEB = "12" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "1" *) 
  (* C_HAS_CEP = "1" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "2" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "33" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_dsp_delay_0_0_xbip_dsp48_macro_v3_0_16_viv__parameterized1 i_synth
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_i_synth_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_i_synth_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_i_synth_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_i_synth_CARRYOUT_UNCONNECTED),
        .CE(1'b0),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(CEA3),
        .CEA4(CEA4),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(CEB3),
        .CEB4(CEB4),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(CEM),
        .CEP(CEP),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_i_synth_PCOUT_UNCONNECTED[47:0]),
        .SCLR(SCLR),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nT1iDpedwZFVkRSZDJusiwI7kFIMBvviCRm9M+pZKTgQdGFO5jX8oqNrtlexCu/uDfp0YQ+QGyHf
W9HJmnELyQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
LSiX96nVtTeT6QH6SYBUiN1RW5Mga6q/2lxWqXdOG38n69A/VIFv4MZSHjz1gILFox9JEY7OFwGs
6ebz/mUxmwP3DNumoccQ6uOcSkKQV1eRSlyyHm4UhahbN/tD6kRdHgTGQgjiOPFINjK/bQof7LKF
xQMmQeb2+71XHcPjUHU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T14r4uT0q5iPsUM9da3RnLjqN8Qn724f3Fcj5n9r1n/OCu7B1m+A10bBZuAn11d+eTpUOqwU/X/p
2zzSaUcTE8ijWpgSLXU8J/0wcBVyuWUHOoOpFIkqda/gzGVSmbiUUBGDhktV/P2ktOR9PeMW1pHu
QeJD3NMerGL8xO8RkFz8+37CXz+yNeWbl9EKsnw81po0312geoX3g2TFZsqRUaRMVN1P8+qQzlEb
OAUU+/BYNrtsGGxq57Lea7LASqCQSI6ZVYSocjpQzYz+zpK1Ifn6KpwvU5YLStgDnK95pF56yxWy
4DsarUkJGiFZnz4hzdYJeRLciFb00Y7Z7OHKXQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JB9E+rFzptTgWubhsk/ytb/NrSJBaKLviXMn62i8KWfOUbd7Q37B9GOtkDXor5Q39oNYqlzgkXQQ
9g+vxtDNbMGPBkiP8HfN2tKmqAP3203t/R+B1D0CmN2mK9Bzwi5rAw0zNBanLu0Huhygqeuyv4SW
RjQSZSiUCtH8UQpPnwdKQSS3zlTnpPv4po2tgA8ZzjRNyXUAFGD15dFRCsv3KN9TGY3ySFrBZTpy
ddZI86gPVOR8QamQKAtVPZgLCYSIOtqQrQOt9c7yM0NqlnlC0kVD8X16GQ8LchOJaRRndKljCiJu
T7V6wUYHHVdREAeFxWPEgIwm8uncarb/xI/YFw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hiRSLr4QLw5/mMP2Zn25/s5s8AF5rzEvu2TjIzKu71zUg0RQR79nm8y7jnlLFI54qMdeDd0ag1F9
TU+c3zvS4L4EyGAGLDGmOYcQ2klSCEkAp0eYHfZNyKQhLKpfpdEXhwpsfAMa8mfqBL6skxrp6C+l
wSbnOqvq502wmvReAdkBa7hQBquCP/Kxu+jlOzeR76T33fKFxe/GKjVFC7CzkdJFg59HGnCzg15A
KPrAj/GAtXhrFFCtzppSIgO8GnVXXMrxXlQOTW8Pa8dpXzVVlhWlbclRL5vPlMcPuo76TstX69zf
yyp3rGNQXyTGQn2cIxCTDQ183lOjoKza3cx3JQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YGcCI/CcJmhsdgWdOuARrKP5BvDGllkS2MoY0dfL6ioXfX2lO7pKY3qpVerntGDre0ZdXSkxLBW4
1veoXYSLGmDdonWSixQKLqlzm2MuxscRuCLcic/Y885s9obEV+bR2Ys2BljpSBpVcE/Ur6Ywxmzk
LxfHQW2SwTpLvo2b2fY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Qfahy1mSmZHw7posW16zQRrSI47b5EnD2EOzgkKc27KVqFCtYxFhu2K8HcIi4g9qHxVkiuCMS2xv
+leE7EvRlzy778OaDw5sNTj6pKXuDNf0TM9Z5qWIQfZXHe1pN3vk5+JwIPlnKOQNdR/ZvyF/MGlN
OiLTikOABwXxl8J3xz7JkKAD22NG7mPIcFEx4r+67vvFAsaNrRdR1eeZqoEWtdnoXxed7RU4EF+M
gRoH6yIiT9Y1/s6OYskQ6JtiRhnYtAuCfzREnZAh899nzaIcLd7LEVfL5Iz9Ugu5o0kDqSWTin3h
e8cg4A7UdkCUVgAKEJvninJ2KykH8gXo3fcIvw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HbALlKBbAUqOw2PuNtEZZHhXKTqgxoC0fIM/TsE4g+Hwx1tR6z+mIjjQFXi5K+jz7LWBPk5ivYAB
gp4qKAg62omeoQjzCOtCkbHjTyE+rVyQABD0MEqut7YzBzvHNmqa0ZsoM1+NuRWnejC4snZdjmuB
euwMUVQoW/cVR0aLWrqYT0+C/Sbr0z30qxzq84fQ0Q/MUnd18n6izMXnlET3OrV4hLkGjbOWyEWm
+vqOPFQXPRVzqPFUtqSosb1u3WfXT2GNf5GcM/libxxrgAtapQYN/U+qgeCsfa/8kNxTd3Eil49G
CY6qMpXOxeviKVJOy6xIifqgTzdF8FVWwipRPQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RZRArgQegEa8BI6kAvEuVwwrrUsBmH6/P4LYf6GlSIJuvs05+qSwQg4jjNmO1gqEPwJ6iKSmQ1W3
yA09aM/R06CgWKSOopUIXdDVjmAQB5fwFhf7sJePRE/etc2XXmpB3NMFoh4m/90MwstthB8r1mxY
x6OBV9mLksOg8OBUYe6EK0JL8xolWR4w1nDUxS34Yi3UV17REkhIYj7X7fFW7ACnlLRP9ywjR3i1
WredfGKf1MWM57+mHlcijQOnYd65bAz/RbHR6wMLooVA4mFJ69HfWw71+w9K894vBch7hSwyoczw
AMqwS2SzB+bzK33XNJ85K2f67QX/712UymOZmA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 75856)
`pragma protect data_block
szRT+m6EWuePzTW7E8vbh19fDS2fXZpLOvx35hHS+t+n6usbBNBypVRammGqqTj2AAbD7YofTwFS
Vb1OUmBqmIdM4oCltJK7DeleWUjaBZYWEJBxJ2nYiireexqmZXunXvlTedD7r8psUXTFxLsjgFu8
7wyxlhjaoKO+6p3svgH5WqCMagVUZtE5LHPa9YhsMVUFlzAN4UrllJnpPyoWF75xzpeMHclgdL4Y
xbQHKpoBaLoCyYeZdEb2+fMB0XpDgOby+5vQE54MJmqZmbstGrBDcAiCW5frTqpa0Tc3gCc54UT/
QMFKE51Cuwo4pSdPW7E6Womm3jrRHM4fHNS8e2nG5vFxxzs9f0BbK9xws1QWOvjQKepEFPpuNFk2
KplKuOCngsE65WWSL/qDAiK1aKJ8fxrsRQT4jO8FTvcqbhKObazBFDvTnmk1VoHNpDpz/UZy3feX
Emz4sgo3xClJgSZKCrzbczHuFKFfreMX4WMmwbmQ9ybnHb/ZhAncvfSyyvFreRxhRhJIiKDvSzrv
Q0IBTdRd0OYqaZVENtMJCBRs4StyOt2rhOLuWjOHAckdyfRbBCekztB/kPHr6m/l5WUBUpf+71Vn
No8qRE7qjgQllK6+leJL+/xU3PUOfaMiUvGQPEBX5MIS4pD+FmUAWIiNtdIhuveIKObo3DhnlzZz
csyUu47rjvLm+j3fYO040sIOfyZqS8u2mlUAokkdKdWtl5DsDZGPBQIw9nw88yL5IHKj1HqR/AG8
AqK/gv4Fw/zqqPBTN0TgvsHF50yQ/iL3FAJy7qjTw/zfmyhiLed7aShZpOF3gOmSPRQugI8jmouK
RjTv+dUgx/Et685Ee2gniA3dd76ImXez4NxstlyFAvJL3tOnnzqe24r6VtldoeRSsAUd7D3i0VJX
6zh85DirFEl8f1q/9F+Kw/9/WGRghZQU7U9wQ1faoVuBFn7QcOheXVWWtea1OoYvxpefh32g0tPr
AQbeXPDzXFVVA+2jxqXyK8mcTLeXYz74ZDw8xGzM5R3HVtTlZkTsBXIFCiCQF+RHrSuQ2dzL3F1a
72sDfs1fJ1QLp4m7dRtb88TLygP4rMWvJDmyLQJ28+PbMU8jCWVcoVXopdTQ0VDIJsuBI1KfTf9O
fibDj8SjWHEPCcURrWGPdxmCMuL1EXZbBZp6AO/CMPwDBgc8xpWq4BHhyFQ3YTKq/RGUKlMN5rJs
0P+e1tOoGlIbRLgReWlelN4KfaaAhIuS9PeNWptZtUzjegME5LbFisqF2Mx7Byiai1XfwtLgsmxn
I+yavttXz266pxJITjjdYk92Mm0e5KIfT1X6c+pjK1+8VBuF/m0wtfKVBz79lG2hu9JFCJwotuB7
jsroYrKor9SYHKDjPIwHlXD4nNDQz5FHAWnMsBjBgXlaXgrCo5AliYI/g5nbilBhehlD+xod5NMr
7bgYCoxaztk/mRoSSTO1UpeoRC3SFmyuXjYfCaRtpr6QptDywDLIOGPFUvelTjKmg2fLSbUpqx7l
3IzjIEgP5tLrx3CWhhzv+dYzFYE3SfOQkZP7wJ5ztkvDtwqAXiWfyyBLoeHVOPrVuURFafof3ADG
Ic+R+tcGVMulVH2LTQq0po/xdsESOhYUpGfygkEaEKkNTp6uKoRCP4vJLWj/jMgPTaTTFo62I0qx
bH8tn5sjTCiwnsrgLfOfprqya1w85Ub/5lmldGCCJJY+/CthSgGj6ZZJGJG6uo4pICN0Pwd5qmcD
DRKrCbK7BVGl9/pN1jIiXYI3N1vq7qpzhyIxF5BC/lv6Y/FQiIccJ+KlJ6YFaluGS+VLuN9jowQk
J2t8Nf4r9VxHh+Sf4kG4VMR8yn2X/XLE1tP74yKRl/OpPS6nyY5y6jgMabjNYufsTKWwbNR9x0iz
RFw7VMtaR8U+WLMT720sb//OWCAy6Nz6xVof0Gut6UvfEHWpit35XRQhiOLAdX+MJqcZgUcWxDUI
0k07fbXJxg0a2TwK8FPhf3jbjoxvmbFz6PZuGnEW3Q2yusCKnoZ1U5cCHkmpk5aW9Xk1U/gSJkjV
F4d2CCTr31nrevEIouXxZLhpiF64s8XM2ce/PtSdXIt1pxaoYTwSI4zhtzYKlb5sUPFdMZ7gxR+n
qvdGOtpG1sNeJhvg9GasA+0FAPM5crDE7KgsW41BwYXUPRmRBdlEX0AxlF9HPWfF3DqFC1gKdrxl
nsSvs8N1f7MBv9nedQ80FCDodUaJXkGy8QSppyxYdD4AO9jBAQUAkIOTeLsIsBKkQnjrN9Zm2HfE
d38nSUJ2eBMvKugg/Ro142oJAIjlMKIQiR1lJf6puk4WGBGAwA6sIhD/WIX+W5YT0VsJSWkaLlIp
UIjwZMrRW4lQMzs5/3cNakQTa019EqylVBKkJt5/noEMjheCLUKxR1IONzbPU+9boAS1XXixDPrk
PXp0Rv1yDuwwrr+NVpVfgF5GlyOlmabmp9B8fDhnv6y23Pqh9b+cvQbadVCFSiwVz7jDStkSioFa
hMU0X6d/1JAipXZJPoyZ8tH/aQSLCIYBznrgFUIPTW1FoFTJETaRxMYBqJiP64/s00YjDOGQ0D6G
PtRroiNvqpAQ7FWkGmlvIqPe22SIQwnxDKdC11Rw9/nIE9PQMVsnxq0vHZxYkJw66BeQ7z4M0j2H
WP3imOV5SEev8Zp5sAY9qKaXboII4eLX/2uz1Z8NarlHcHPW6Mrn2IyPhoKvfqJLJ8Hza/lNJdNZ
rDE85eBcWilBIDq5Xu02jnxN1dEZgZK9I+sn1eKGikdCr3TRYM4ObCYdNyXik2+bqWcj6NYI4dIj
1nSJ8a5pgfWE03aj1mMV3fC3U6jNwMzV3z3b9jfqSVnSC+VN/g5he7R2ruYmI1E0eHMLffmgoCLL
CgrfOnVa0wSh9knBpNldxElqn+RZj2YQsCTNfJeeDPZaLTMMxLRNyb+WeC2Wu9mNPjZ6VtDZZVLq
tRExniu3BUk+IfW70dQBPtGvNkC37adtiJlCKqxY2JXPVf1oCi1f5TSOgbZ743RnHFs32gz6nDdy
iJk093oUC1yv+sPLlkffTCNf88GTfzBr7l/Hi0HZUqOvpcG92witRIzxOIEbX7qJIn68qdB2lnTo
fWwAh4ETBGthhwc1xz+Dc7SPx3cxjdRhove6U8nC9K38I5badYTZob0X58jaJyTU64BN1D2QHrK6
RIx7JjVKAaattswlBQY9A4rkC5w19Y2kSjfodj+gy+aQXEs+oi5U6Cs7hvxdGPblu50/Bkl5BTwq
fDbHfN6rpOvKoeTaJFOwvWxqXyY5UIxs+ol6rwgStFCVdlBlj33X4rcE/nuwA33nkqZ9xg51FQNJ
V2DqssNPu8ue0BvDYNJN53boa3Ls9plryCXARRlma4juO0m5mY6h6gh+OVP3lkJaIBqDCcp1LZ2E
+u+pJYKIWeRlP+y2p2Bdwk5Ht89N65EPK/VE78xm3t2EN81aPHwU64jB78NZ2fc/tC1O8bfKahYZ
IjcGYC3ztmqFWGXNo+jHUSeoKo7rQSAlxgfRCOUNAmP/DfjlXGe/yHc3bqUPS4pKAoR2U8Yh0S/Y
FeWX3cq9yPKAXKiPzdmIGJEis/ATU4HXEFVzznpP2ALoXE/sMGinyFmrnKxy9yaoU3We9gMtf1rh
Xzt5Tnu8aXFVEyb/uKgLt8RwFBJJYHf35iQhHqTuai65CY3kBweoUty3PpZkGfxqwcWf6GPkc96Q
mc/5z7B4KpSgPyMm4K4HBAascEA2Q5OXJ41qm7uGIPlXXNpLVcDzhAP4LzPpWxocKPaoFxp1UhCr
i8Ti1aFXavgl7uQBt+pNowuHcFvzKQIyISDXmJTSUExVm87WYhkpkRxwaEIq61Mz8BXjQR7ICueo
EmP+zKSNuI4kpmcEorE4GSUfRofWY5repZy2ab2+DG2O3CWI8uhJUEGCOyXH4jDsZgY3o8gqWYth
XtHPwATLUTAcltFccAWXVbch3pGd1HJ9/JQWCtQsQ8jc/VrcO57gvAVTGLSG9DOMzxh4EW7HoN8Q
IxnZV5szjV3aZ3V/ijTsyQg6y5KlohNYIhcC7TVhvQVdL9YH4sCeoYw7oxKShyUQ6h0QGrBaUebM
fFhJOnoHx+s26JxTMkFvD8eJh19tH0rClRNOKbBnwO1Zj9ULUvHXbX5bhgPpSWAXS12HouolUylx
e/pIaArTGNOrgRPddZn5ci8Cg5bjnyuh8UbmYQ3MC2PVSkSohEyhAYtvcotWtp4Tny63O1ZEhS5L
KN6/vAuPALUFZbqFvl56a1Z+Ww+tucXEvoISo1UQTbMCJaRjEqHdogJ0QaKmRfRBilEmhGk6zlYY
u0P3yIQNr/8yeNr0qs8GRy/HrdvFSiwExStMo5+PLuqyVKwPoQ7vRgRwj/ChCY2f4rOSK6HO+sVS
iKK4ZAUaYxdRqwbJaWR/JASXsFriXUzkiFFjokInl0memwCcuzW4FSfatwWJkWT9w2P9+T5xa5x8
+TvjpMUK13dItXVf2NoJsuUgLzCs5XfY+ggfrfXbXPD9fWHHpULLFzbaX0MiMzOLz7YFHliMkRmk
8E6iA7DP0GNMqwQsNI6IJoQ4a4aaQVWt9MU7f9a35+cyj0kQdbCkZWJRZxQyjs8yHYiF404esf9i
HhZPXDi7WzsZ+MV5TtTxVl6a7qObHlcCoUTWi917EWyKp1jFeSoNnwGj+5x7ooUgY4tQ0cUcEKSU
+0/7Z4QyRGLIcvOevWHo45ngvmXXJg+x6KxXH4x8LiV7HONYNFCAt1uNPGijE3EyrIRKjoniq0nc
kwec3whCaITnZN2v7hRsRT4LPf3WV7abBQB9chB6ASUX1mHQdaf3YXZSImfIrpn77fHZnNS8QPq/
VdvqmuctqxbIBE2aXma7bHGdRG+m0Lp/5A7Lf3D+Zj681vT4f6UVhGRCDTr+mBI4ojDZt0JqsDXa
rAO4x20faupELLqlVSrSll4NC915Htofy9/CyVmom65RaJbyd3zc2thkjoW9arPVF1r7/J2ypaaG
/5qmfRpHF0sJFKDxwOMclqDRZwkVMuy5KXUwfagpLAYWUaZ1qdpb2ulxlHieGs2qCJ5nawvgpG6h
NXM3gwH2gvR0mUYR/AbsiJc1+geldVF9CcCWApMNYDEltUinxJbHVqp2/A3L+94AO6qsvUfN+LeF
PZppU0PEkmGTB45NXhkfm+B7rBeD3ckO9UpQgp88NTkedggJxlmwEwSBHc109JwTpE57ER+YSicz
Z9eirShlke8vZxQVXpsUtnMagqpQyH0BSIVFm7GdW7eMIo8q887FQo2vKhrAlBaZ0+BaedCxgq45
uFHsTS01G+poDCGx3DOsZKUX/jozSP2KqqCpq3YaKPdvJ5xnvlNxZawNe5FH/3/GJbWlVBtuum7e
erw1XuUjzCsU4ZfTexmoKsM7kDpTolfJafKmWxarogujl6upkS3ChCPg5CSMExqMYhLWlzo6OiwD
Xzg5ed24CbU6JbI2/GPmj8rP5/ztC4iIn/Liv1kEr+eVY44Ji0cSRoHENq6vOXHKGhbyaCtDsozu
niojfqpCAVLqEKbA5/9CeH3oAaCMo2bni4RD4Pd6QTvCRePQ5hMkkSqBmFxjtO2GSKcD0DMO5m0U
1KglS/w15HS6SGZSZEQuenfdoU2W2VaOWyYPSh/DqFUWRPoPzYkfSOqOpukEgnPct4IHwItQvkev
U1YW7iSj2k8A18o/MEsRNv+x4wOw8hKAbP35O49cg0n/avyk1Roffi0Imd4FKqJVbkDQgTH1FoCM
B4yFOC8Ut/B+nhxVjaE7pcckxNskT46ktFez6JNKEZJcm1xbA3VehHpMMTBIQ3axmFgXASO9ua2D
i9mqk44whuVNcUYpD0JePafAwF7UGlx2dS68HxsI9HyNDJW8DvLfrUl4WGRozjRJBQGTUFPI+3hJ
ILAM/TlC4zWlLQ6Xuma7wiVxsB6xbB9CceTg9fFDMx7yKR6zeibSr4dt+mwVPqg6XXGEqOiVr9an
7Y4coQNa014O3DJBdcvwjH4PkQ8eM7UgJLlJlmGWlrVYNK57wjNIwbVmm3URud6RFdhYXLy4YeWu
kBDEZL5LafZMS1NWFaFUfmbZUGGCR9oi95y3ugqybuCEW+RjwZTXo8P/EZB/AjAp4EYuiuTYQS0v
89XajBUw093Uy2rfc5HGfSo7zdC/mDIIvAQvfebgct+UmYOMKCFxanjYHsUrKn9KHOzs8PfZKsa2
rRbH1zxTdCzNbxB5tM5cuUCWOZcUZN6pwhMYtFbQzcW4bs1F6Px3PzqhOMB+7M1GN6htRVsaUbfr
1avNv1FPe0ocdmykgy3wsmHs5wrD62AoVuU8gKteOo2k562SwvJXhi97me87r51QqBUwlwjoujuJ
CBDVP9+fzHnsu53ij/fNDaWpTNV4+gREAvUkCTSCt8B11fmgPE5WgjSAIK0BAIHy/qlYHlIabken
HVWfeVDyBV7hNqgbamg5lXWIMhqYFEaOmlWtjbxSnv3ou/kB97JOnXaCrVSPAjC02Xt+pjhEnZBv
ljvfDj6D0J37TsPmC11aMt4WmwQz9UZi0dB8M6hhmS7D9dyazxp91SgQoWRjpGr1o813Br7hIIXx
dRWQBULv7OBkREPpRmsuzau+j8qoIzm2R6a8rB9A38z9tqrQnS4tymuBuvJsvc6YoCbqPASC4b+/
J1vv6xiiVx1DW0hbcOVy44bKrpGUtpZAHd5IXmPiOEvaYRjvJbqOVUNHrC5EZI6vKunl/EPlJ3da
8nRDq+e2ATCfd7W3Rh9ZeCckBR++AIdQ5ZxzXHbrAgSRqAfT/+oq11tJTSH7WAj+inpJU0zdtvWI
nb5zw7o7HjUMVRvpi5EgyzBcYhOA9Gauagw3Fl1QxJ19RBASlM4Fx9vH9bwA9bMkH+ADTEYVWaQx
AIAFp+ff6b/fTca5/qJaza4x29ossgdO+3RZgi8SNhYOxqZ8PQvC4Lfsh7cYW1zUcYzcMwGsijwY
sB/iQ58mjARefWTVgvAQN7CyjMkGrUdcpuzG7jrD2aALzqJlyPoBIoKt6Fv/WlsMxMI9kx/pUkqZ
hUWZjXmqlYSdLqCn8R6obkflCpGF8LX37Aq8TxjOguxq+fqoqbZ6yX1cSeJbUzE/7WdB1YGAcb6G
ZIINr0EjH7BPx3oVhlW86Ag3aitDaNvyeiaLv6djplgdfMjzRpyKskb/vl87zkfsP1Hv7FhQBlLT
mOdmirT0QXuN9qogIw5Z+3u8iqrt0VBaX6nNzVhsAJL1ZMQdqO68bJMsbZfMaE8neURllTSTNlJ1
75ftxuxvllsuQcAtlnauQUfssopQkTOVJoWG8D3HxTLr/9w77cdHn0fhUV8Qz3yWIXqZFUJSCKT2
uKGh2mSbLntSt/sPE0d2WYqHT7MjwP1YePVZYj1D/w9lPiTrohVA5RKU8iP6DX40bJHklDVSlIzz
z6SjgK4/lLZjcTq6j9IojZ1j8hZVx+umJHUtgv32Htp1dDZTMXFg0kpJrE9OprfTzF7OFs2RDpgF
2O6D3GJ80eWNOrPfigI8cpwYu9HqTOE/JfZFTFEDuXJTTJStwGUFSk2SmIGWZhmox1lxvix7GhqG
0tqdyVKcTWRAo59w5mWKBcBg1T2YeEG06ZMp+p3/6ZxfsyzuXXUT3Trk9XwiedseYQsSD2O9s6eR
OjDiKf526lbRlY1Ng21saYsclK6Orm5wlg12U7EOIL3wQdtC084X+eLG6Cav5z9JIfLZH7je7llr
PdVj7RJS08YcnyPDdKSmMSFOHUmmHDuc/44IXSFwvqzTB+xUKRBxMdgZ7JrdrrLMsR47aonEwAgO
aVfED88M3uzwNPemHLxXpBm7ZFOp4COy5uujGG+hPUgYHTUYwL5tVeIe+wo1Hi9BI6YotAfVnxQl
5bGKdLmW0hh3v7Naa7F1ylMfGl/h0Sau5ZgsyxFxDG5mQ4gTC55crt8GYXFsgZvwLCaj7+Z/ex9h
fHYbieEP7zuzjZa/NfNM5wtubO2T/4r03cPcNn/BFvh9ofpiTbS8iW5OoBIYAfyw10uSc9XB0vvF
DAqQMQZHlxS6gy+yfoMu6wPDxNARtcEadpkOSmY1Isx/a7TD7GQjWgMiqXL/X4p3ElRae9hIvhIe
13kaITP/tWJaNPQupwSimf/5KXWslHg5PSFH6eqvhPcDc2y+CPRhdXVDsLMSueSUkHPZ/N3pIgOe
JYKF65tJc0m8UuJP0btVgNsD1ri1snyquVXyE35Bh316Sr0rqRQnc8v+z+TV+wf+Ac0qHf8qseZa
/XxDwv78MnV/RTKnDyOyVADyuOXyyC0NaVuNeR0ehQfCDpaV29cHM1mwi9Q0486IdBwOiW04QqPJ
YiK6xKfSXZ2VJwnv2sKaLSUTmxDKsMXLwF4vCOqYgGpFC6kMI2e5OqM+c/D/ZnKFCwmrj38KPr1k
6KMQqBOtl109Ul2dXbzIMW16DEDwcc6zRV5irFuA41BpVzUD3MQoAXTCB0h/y1pDCAlIDZks9y/y
pU4tFsPtVUlkez1yV4G1q6UjfQiG4Vl0IfmOqQ1OMzXFodTNbsTlrSgPF41VNQ6K4RObu9mGUW5E
lf+QPQOEkDmNGf7/oq6/Ae2NCzSm44kHjIBC5xyJo+k56OTYGB7UFAD+NDjSXUfJpNfnPoAqyst0
MLpsYoR6URX32VvioVWSJcaFZpIt/hg/E/wp/qWBD4FXltQ6HV/rXnN7TivvbSnxdHWWbODKEPlE
l7MiN7Q+a2FMQDn/XXbDyJwvMCmE3rVM5OrRuvymSgQt8TeXJA60kQr2YA9PkIT99cyiL3q8ctb1
WJERqETpAHBhNHJghw+rFFM8v0f6uX9oQ7X78bgKgKI+RAvkc11AZWlSP1FGEhiJ92sm1PuRgTOu
AWdKn0WN8uaZqLZxvRNzjW/AQCdlu9OjHYWth4ivMhIRoxgv1sEj7olOkePNa5IJ8D+t8imW76cw
kDcoGpAJZYQJHCavWV7OrXy0sbJrWrlwPZe3EiWj0RwKdIxEUGthhcgk6qcSUcP9yteGIdS5wxxG
Btcg0OIOogryNbQ10egZrOtKnH8mU++0rUnOwMYhkqN8xw7tJGUAel2yBtjw8zrCg1IUBfM1rQzP
JIAb5YE0O6tyvqlglzuIRPtpARsI4OpDiC5OYjpuzRogqeufYoaCEzG2lKzHyH2TKRXg0oxFG2Mo
BtPaTjwbFZl1WudfwXjHuDiEyj1UkEUBAvEKhQRrEE9oR2rbIoVb3L98sUqZGTHo57SxmIvoHG6y
fNjDxMUr3r0JAdjQZyd0dxad6E/rdGkoYjvvT0q+kfY7yQpJrkKmLYAtGXEJYrr2oa/+9BJwRbWh
3WYAMuVARpY9f35nXELRV11HB6L8OphCB6PONYovRuufw/3aBNIHlrgitPMaAQyPNcQkeO9KjD2j
9k9IEmjIwP8WlqLc0cFKc1rjEKGMELlii/hHgND9b2ltXRHdFeHmr2HKEoZ/HTgWLVMidAvPho/0
ng0LPNENOCVWVWblLAGqiEDPs4hEVzb7JEMb4cdCM5nfiOqoeDE1BMAWFAtFiO2ZXlsern3mcyVL
41s7jTv2DXjXnb3M+WKM66HEnQsa1U4wU9PoD+L6D3sJHKUjrqBaJ9fdQV7CsabFpVy3sZRWgSvL
tYmRQFm/9azlgxJp589m7Ly2AyuWxbf5gxHol3hkXkw+cbrLE9+Fetk952hvynUCzVE/pOmJSXp/
TbsTDTZLNqdvSroa5QJcKVHs918PBnMmOTcJv325nqbzuKDJqoBqyr5iZ/gwbCfTez7+vhCWRPS9
qqTKmlMhhBM9A1U9A+onxJZRSEcRaQf/xrpSeGSmAwN3D+FqSpIHEZAwYbz2yt9dD2J2aXTgpcC2
9J4d1jJVWaTzzCnVhnqUraHmHWuDCjpU8/PHSV6/++g3KWMaYkk0GyZIIeVlCYJq705AvVT6zFil
gmewBmA6rQG1HbG98vwEl4hEM9cCW8bTX0+qxVpDuEAVE4wYxWrpQ4bllr2fokJwPaFHjPlSxZaA
0muEpimbv2yZ8SMaKvhAo8A1CqjY7Ohho9xyNbB95X9AOv0fUOnqsIwxVYxd7w4p7jlsidBpga6F
1wgsLZcYS2fnTIiql5BFRyq2li022I6ZL3PADdiKPm5zSgdbG3lq2z3MZZuiYUxmKLmDCWmm7/mO
tIlnfUqY1I8DE0Hj2HMp/Ow2pbc5J/9MjYSbz1d7QwPJB/lqbgDKdZ2vy+GTKChrWWm45je8OJLB
9yFK+cgQNyMStbb6jE/P47b61nGQPgAz4ZyXd+M/kSPuH+DGQuly5Hslt9UmoVvRyq5wk5E7x3+q
DpzmgHRrmrpL6auRCmSQi3n9eAsFVIUMMxG+3nZr36JpAE6fiAl5djHh4SJRpHLxgoUIaRXKyQbz
Tg7WICEQF0MZPrr8O4W2OlvrGqCl3PAAEkBSt9mW5PQgbCrSNvx5HTN5sOVdclUuOn0m7pRdqRLe
x7DAW5h0nK7mxjOMMglG1c800HdpESNdET5SkOXVsfqtp6piyhRiVISIh4uo3Biwn7JZlTOK/NBK
Cw3ukVBz9V8V8OEvKLgCd8J/qGTM9u8FNxHBaVdO89QynFjM/qZSs/UK2Kg3C6D1aIVglZfoD6OZ
UULgDTurLeCdNmXEanZML1HCGUO72vXI6fdHGZzXutuXmE7IBy2cH4n7/ZRas5jy3t/ohvTL8rC8
2AzFn0ypPlS22toBtbbd87puwuJ64L/zkO2BkBHrTcpEk4hAgl7h2agxF2cyG1o6qKYiMTOLp20L
XYbJhp0ZsZ5f9PWbI08F7F5h0tkjayeYx0gYE9P9oOvGa540MkXenErhEGBCq3Zkna4g5//D9Mze
7dYKILlzqbcfz7cmmBV2i2Ye2AnWvYzyNFmyCFHPtIbAfXsKo1m2duxCvahAVze860MYjPtJJ4J3
Xw1AMUqMbVq5k317k2PY37hkX1WhN+hPROZvt0BaQzbXdzPwb9JR6OwCVd4iEdZSMJzipEFxTkIk
zQ2mSemvlT1AZO2DWIDGY/yYSWA3r0lZeBL6e2FUwYM6F0W2MzZASAAkXXiGscH5CRwRUC/V80WA
i4AkcVgq/6z05S+FN54Wel1ZEnEyNQ22ItDrvVSXd8jmVC+MWpe3IEF9N6c+ueGBhC5KHSdR/cXp
t25eWwoGZh13IdCSwi7qCqYGdZGPvqvBmhTx6mutNsVsoBp/i2DHJFAPYp1CSBF3kBN0ItGFD6dq
ONh4U7Rw0FzOnFbN0EAYJHepLidI5tiP7d25Ny4iAzFCJIQXJWChk/rxT1lYBwJCOS4WrzaENdVc
OdoG1WVedAodtGiZW8q8fjBlae6f0RiNjtiFMnekD2Wd5r69sbrex6f6y4AO8LFY4f/Tpi6fUP9L
i5feLkJoIfTaGuZE3Cbwn7RTpgE8dRVikAR+nzAFmmer89R90swLF1TTwQkE1sqp8dIN0weKWnwD
SAqUQ/lthETgOQ8uug93JO2J/w4vmcTo59upWrRIZdCmt8V8i/jVS+KXF4IuC4QJaYHqbOnDSMQE
zOM5fqAFJHkpQMvPcjNTtaBnpTggAdYFwH06xDwYhBmz4qiy0xE5G5mWqts3pu3wZkPmUQVKWbEW
ExH6yI0gg38i0+EmThjaBdC9nWCELT10Hh7nwyL91UNkv3WRSK72ZiAInqAmCd3dJ1sGd/C9J6lc
QWm4YQ1y6qGADMZ0sHg90zymhyyKqYrTYrAxMCTSlMBsqdvF2uT3uoDa99hSj67EB8Hv59w8Y364
o25Tr+z2sSFu3AWTxO11IgsJ6tZkz3yF1EP4MESL3GoRB8jfWe3Upk+ZJnpwWXK4dLmM8zoWhs14
aRlaLds2OWvk++LRLD949MdKwCpQaN7ln+3BDfAjgabHciEnMasIvAR7xZ90FldeN6xY/MTfUZmd
ohbYVobQOKM128ce9AUZRZQLuh0UkhEzGPY3769kif9bGA4A/yi/mOenhu1C8pjmTTvzBfecb5LI
snR0Z1xKWK8qHDd70pUIP9RW+xu6ccoiJkV63kKARQATkQw0Tt9JWkMOz+MzB2M3BUCi9FUqG4Zm
00IU64jL2tnq6bHRPmA8CC0aM/SmhYNNjJzKUUaC6C2wERFRbp2iFHqL6YbV7vTM5fTHbRhbh5oE
738RSbtrgl7TjFZQTQlJ2fHwdcOkfYjNbAPfvY5WQxsTDYpWC692diqE+ej/hYeImzxq5T55Evw1
1MdmZWRShf2wgf6SAiK8I4wrDnc3pHt8YGhkeghb5KcmnHhCXXk1HsuQmYPWRjVdun24kXJNbj4m
LfWtqdZVg0xtFMSbF8CQ0H+affoV+4V34sZRMZO6uNFeLPwQ+RGIepmw/6v8CLnh5+JqVtsPPcJK
wPoKUqVW/GymG/8IaAJMKSOw016wmDKD2wwb3WEFhb41OLm00d3O126fr9p1MB2rMuXPnAVOoJU1
3PP/RVf8sNaWCvvOmu7/AGXf4hP7wCsSgT2zaJq6wQx98rOlGuK0A5qp0sOdLxqafzQT2NdxmWfi
6QzJmwR3wT05KSdAL3NCFMM+dwJzmw/vjoC6GzEwauGy7KoSoxDH1o8n7NQQSttqiCm5Jp5pNnDN
7XCvjjrT6rleC7ul4WLt+i1k4wFv5QW6OJOGQkNDDxx/AdlfS+SMxZhd+gMKLZuOVtLu/K7NEcMj
dde0ejLrpbxuVlNLnTZOY0rVJhl8OcjszQ2Clyt0CR94TXb3QQYj7r/mZ26WUT1FY+UPHR4sbx6m
xOKUT3Y6TSTBY0lhhxyfbJ2+RzC2LrVKVi9N9sWX7uH80cIrH42vLz8V3JxOvr2VvGiapcr1AHba
PRBgZ3Be+czo7gbtYNTs66e3dpa9cakp187yTBeYx3KWUrjyuSpV3yabeD8TwBSlBdlNdaINcgPr
WINNNPmz94A0psL28ldklva3JhH4rmArxxJlcaldwWUPfDhM3A5IStdpWlyHZyZuPhrD8rOoMSx+
0R69QLN0y0P7gu77QEHB0M40Ow+FaZt02dsnxjzoVAThNZV61hwm2SWWw3dhgQHc17dV8AGTs9Vp
b6iJwOZq4EC3M1nZo+Y4ixQSxG3pEc+4mdeCcRC4jDK37exA7QuZf4RZsCR7W8GdIzjwv3miAIlW
AOTI+PAq2WOnlK2sCNyK0YmlM+OAE5qz8ovcnjMuTbA4YDLEDmw+TnoQcvNNJJILW35zKUtecZ6h
zNHyyuV8ROcB7/k411qYvKKtDpA5IxYyzQTtojp9ERokr7cvzS+YJzUlPuaz5qwhjgeElcbJZBiO
DmMTRdWlqrzfcpHn1JvntQiRvE+2oULXuv9XkNr6wQGswWBjhfs0Qcs50Jv+ElXk4XGcTX28ybH6
GSHz9D2qEiiy+9icu27Aqn2LJzTS+ytUkqRQiNkZi3xo+X3NJwoVlvXGnJUK5NE5/PxQh+5zCJdX
ryhag5FhG7hbqFcMW8EyG/kbxoQVrBPpN3lO73qXkamFDjcg8OhkZuW/EkJMH1/yWU9j9rsPfIUo
JfxV2xFVBTLkwgRCNHwzt8Yqyz3zCDxa5vE4WApIRWoAnghhjMq+uJBk1CbJEGPPSgBVDJ19NuJc
CWi+pZkGRHKbm2cT27m3KOEmi9MWCoQxerchA3oGI7mKK8Iu+P5UZpJHZBwleYcu5EnggJlZNcmn
V1Sq0JRxv/htPar4xvS2Ah62M5SqVjutEyX5je66n8tK0LX1GKrMUZB+VX788qCHnCL/f4zTf2WG
w/mu3ivr6vMv+ofp5cAxig8jbmOzKLEgaPI5hDwJuWyIv29UFBEsJQlHintCWDEeF7pTxeuGdUk8
+j2ZogWnZ9Roak61yN2fTN44yWMkm30AZG28Tx9AmNGasSyPeA9nd5sY5gjVWYixGmtzCgDjSG/6
8YHIVAZ1OdyYiKdtPpRV1WatfmvUryj8GWZvOJE3jk9ArSgKN5X2ABD5uQD1Fqzo1ttFqagy9943
uXu5QEA48vCV+m2TpY3GvshRfDKi0IaVKKL/7DYwoU8SI+Le99Zc17GR7HaSBUOQ6HmCBZnz30Xu
m1OznGdmd1zemukJN+/rB+K8+vZMTelLNsTLy1gljyGRoxfBOvFFYW4V6fIn+9LtiUDeaTTlKx0Y
P3Qo7Kxbq+nKmWgggtl7UXGCiA2XU0jUkm0vSB2n0fiZgXf980/OraVc4riBMLUwR5CBzrdqWiAN
CyshsyBPndRxB1Mm8hwj+N3q8F962wLVHxUyS1tkJcNYNDY/Y8sB4mikpBwdPC7w1hvXNugtp3jX
0B5aPAMUb/fysi7uYIhY8KUcV+BVHW/wsyBB8Q7CkgtVpyVA9FCYb/xBr4rS5eQXnppnJOtr2A4/
hyiztlJlEzfeuVcyqhQPnhX/iIDFgginND4RhZX4OZ6Pzlrdpm3u9N4Gw2e81adlZMoRNOdxuaGG
6ZKMyHmW3Ur5Ce57UfAgXtIF0tZxSl9sqolwaoKaNX/Psc+X0VCBwICSCJFdvygzB9nKpVvsPeSy
PNVVgNLpCRbIYPnrRdMUOLsqDMp1qT33p2Vih4L8snoo+vltGSQrxhFQ+r03kdIpffmJugrLuRnt
LcOX0lnnDYM6du72Mm10IZId7NIO/1J0cLku+E6XE3hfzFkPr/ZbJBbRJNvlCnk4Js1ImCRg7XkO
/TOhiGrWO/K358pqjRaneQ/HkPLF3igUw4AtHebUDD61j47wIygLvLQfjf0aJ+uwMGVdnBg+frH7
QwIyz3Xlwjgf5znOtScpYtER8gz/LnoIw/SJX72LmhNGBkqcDqE99MhiIRBHgadazh0kx7ewCslH
RoC8es2Faq2u4ypxfH9TNPASR2m4xJ7GNxaKKB68plecHv3smHBx0u0DoE74KW1ZyMVN9YnOWFdt
/N82KZsiM8c8foRC6EvbfoHY0xWiPmkXJFzR8SMVu5OSQumy6FB8eUihZ7FXC8YXssYb3c9XCKUh
bPRdIyNiAo/96OB/LuTa7Kfbyb1FfzTBs0xZR9w/F7r5i3ALJsOaILYNAUOs3a1fX2yT8lRtO5fs
/3rUwwwqBQcquxcXbhcheM7WG0aRHlUObgWIwU4lc4IYkYkyi7RQ48x2hPSNGWpbhvvjO2WE0qkA
kjgfkAPG7RvJtG7eWaY54GQbwF303bodrwzB76VRrs0uWPgrJZez8daCvw5YZpHhMQko1Zl6LLZh
h/zIQzHGNT3Ec6ukHtSyFezmIaXpvi7hm6KBr+rJu9ygQTq/h7oYOuGHs/+YAhqzcDSVOO6wQxEn
uv5lZ5WjQzqBLsjJc0T//m+VCXC5jVhwRUyRmN0hM4bufELTNxgsOEXMWyDXe29FN/v7uHpZhGaM
tPgLYxdqwbcx4SYEWEgl/LDwqThRuDx7fkstzHtND+OfFFne2qDssA6QIGZU2bz68UHP6vQbcPqc
qeXiIlhxJ+63fnxnoGVqYMIsT2v4z7TZePGpORqy3B0dk6v4JpNzhPp2fDSz2epoNEUKOsAnBovP
uXT53/GodePdTh+s0a26FTp915184swn6eqOkye8jDzk+XsJokvSuP7L2FcMVTWa7cfScBt3eGhJ
tjrLN5pFqh8wu46huNNeY8uldUa/1WiQ4869YJ/aHJ1xaGYvmo7fZLUgkZ5tt43vaYOLp7TYV9Lr
NQLdF12KoWwYtsmvUjgWdOjCUEJqtjTwVHqkCSHllmmZnHNpU4Z6K8rX2lLx/1uKimy35yfJGfcY
KN9jpmPwkgdqrtwme0zwhxIgoM73ZBNpGIbLwW4BXN8/zzY9HFUrHnAzioSCP1Wonz1hiNIdnTak
rXVTIxhiAND2qTsa4rOfg5d2oTycUm/fIngq7O5Rfy4Y2HfLxp9cERAmlTj0ImO1Fcbp9AMvCKWQ
Ed9GNw4PLN1VlKU3bYxzw/Jy86tgLslzKr9N62IK7gJ94qp9UwlqsHwFyMysI8SVsZZv1Wjy1OCX
1ry/U97x9ruocGq1BGBb9AHiJx3DLzOyh+DFj9+e0Cw5/AfTu9gQ2pV7TtL7NLVooGSpUfWtL4w+
f748OpzRsniHwZvLYjTTYmixLGxoUnhKKPBws8JHZHC+T8uME4KqotHyOmsPW5/QRZdoar6dL4gC
Hgir6BL+rGzJ6KtK8IWMXWaPsVSi2kU8LOG6VIQnXJYh5Hi/WugLMDd77Eqe5C/LNlMyMyqLVnEj
123l4/VqsKm8pLBj8sO3IbIs8jrsflLwRfRZSeV+jlhcDz6HzQ8YPcq9M1QZDQi2NmS24KLZr8p/
gfZK3oYEYtuzRWdo5D+YJgK8rQyTo4OMUZ9rUCg2h8V/o6xsu1hXeJ9l1MmYAftvsmLmOdrwOHEs
osm3Ohv4bh7NLUzANfPdE+mFqWZV9ofSCX7zb/PImHhyUKKoXgJhakI8pOkOnbY25iJDrrodEi1/
372yh62psTJPt1iJGfZxNtcxT5/AVRatUwQfqzUNDJO9m2r9UrhNoy2g1jYNW/OXenaxv8AM+h63
Lz8LLWbgXzFexFPLXEp2SfFBVzjWTp7so7SG8xMUW6gPtETIytWB/FPixXV3j1RKQFoiGuHN9Tfn
dzCRwGTc2x2vkTqvbJc6oepJQMrEzKYL8osHqByv9VL5cZBYPvtHGC7iMWUCgNNxMN+a2o8YmwIq
Z35D9Brx+46nhk1Y6witTomx2/nNnVShB8VFU+M9UXK+EJBTdGhXTJEJQGahyfC3gvR4pJNSUcEq
5cUh1ulAcr2TKFSXI0x3yVHzKVTKZTYZ9RsSkMrolV2pa8uzqtjvduR+JUbuxafoRrCNpqV1TPj6
/DFew9gXLzG577fEEqLbVJMsDhciCpSCcnz242xKcFHvmqRvJ5cUCiFDcRlguWz9yDllh2fvfmfy
3yPCWBPoPXAVHd3Y2VT5tHLTiN9CdKaAyHrHEGJBigWLn9NLeP37kdNVPoY1NNvZqwfW5C0cJ1pw
HmO48pochU+iwColVyV6Lo1wqfFGOeqLCVnz87H02UGgpEi/cw1EawdCDq7w1+IwCcf7EwxzQYHS
+ksmeF9W5S25oKXwlIKmkTzctLSlsM3CYkrhzbRET1P01hnllMBjRO9PKRhEMgI9jM2R0urIx+Ow
81nE7tbk1UTjpdEsheQVczSbKk+LIuzIAHu3oI2vtYWcAhQZRU3lsXXPAvCd3UYYgTnOXXe1H0Tw
SZhcPk2ZlJwplPV54MvauzWYQDflZHVLoLAuRHNT4RXZXmrtlV3a49A0rorF+TM2RZEVKZfbBEpz
uZlijU5KpSwR/Z+QtZwJ1jOSJT9re9oPoQg2SIIIj9SHTJ5vEnq9ZBz/aeNh3zbvcJ90p1lSu1vi
XYjE5pqT2ltdY5PEcIrh6npPAj9QYhMEy1KStXACYwFfXMRsOknUQCgKtm/AtBTjKM+e2CRUKr79
9jQ/0ZBQ7qywxpaNpTQogZ8yMWC6GY1dLzUqEeblAek7NXFXrz+SLj0oppVNzFZjAFQCfj85fZXI
W2liKgoU1RTgEIOLkpiVE8y5oBQ5F/MTbpFPshSxeeL89DgF3r5nBlt+gr2a/vdotFYyYuNA5mBi
J8NUDtIJo+xApHy8inOyxM3wCPopthNXdHPbxSm6QwUlVRjyrSRVJ/wwAmSZBtSnn5WvW3UsIsj4
2ChLR3MJiX9uB+F+PKLrp+Xr8hIUTVRQE12bf+KP9KKcLIC5hLc7LqWgy036Gv1V+N46rqu8mjj9
NRY/NJfWLqQZGTciHcgAvwRSVWZPsrVArNeW8cZyJq4oB19lbemPfZx5jgGbLrbkcL/UMWhjkr/i
yGGy4n8ISJAZZT+G1j+mCPKVNf9Yok+sud0EcrjcXws2/sYqTyv703pXpmeQ5mIkB4IwMjQBGarQ
+QAY8ErhEF0j2grdEm6DswFrtE97Gtm+JHzIxzA0xvXu7gZG5D+Q/rtA+wmrQY4UakWznW9agnKU
cIPhAWsE1SmroOViLAHAsMrmvi8YqXJgsVm9hJLoT0IsHWfAgYZv/HKaKSrvgXLSAKyDES5pELZe
8A9539Xr+Trgoj4L3QjRWV+1paJ4q+5gOtAUK18C0EKdeWhg98GX3lMH2IRkBRUzraHq4RJ5s0jr
Ygd2E5BgylnMqYQJmzPncrJ+RskDkIGVr17DxWLTG1mj5N1eHRC1RsZ9wSC4d4xEzsBHwe4qp55e
qEx7z6jwueCzFmBPs7Xl1BNsy8yzXi0XV8NXbZzbHSjDnaggqh7GVZC/H4tbe1bguHGZ0qZQhlNY
MY/PwS2gITXCIGVrxr53adcRy5q1LWcpk+SAic2/zTSVIYpva3+ttvjPjvTgyhDeVZuGZjZrYpYI
Ij/eXw/NXrQ+T/vRMyVskFdzFt/PaRnCnMlQ8bfDEaxYcNyAmN9NYH8L0gLe4E7DJ8yCreE6LvJQ
ABYE7TLvbFJfh/jOb3MzQi02hnCRz4/eAWSS45bon+CSAqa+2Cvr1YQXTtondlwyg6Bw1Kj+uKQ5
bqy/V2dXYmK15HnBSv8CV8dpVOe0Gg5X3FyBUIqwW29Ljq6Lo24EYx68sK/jovJ2PlObGd1WjTrD
wbvvi/yrnoU9dsT5Mh1yqM31zUU5pnfxFy6gICNO8cRhEYRfFghU7tPzG5TaWordTqRsKlkmz+Rv
CQ2EV0OdfEZszHoYallhNIiLmHKfHHyNNfpm19x2JmOEs22lCAYdQ5F4RWCFTP/KMUc5YgveSn4p
6TQuQ/uRQ6XkIJ7hU8s9vzunTDLnKP8ixG3mpHfb9EeTzF4FE67VRUT48WATTSiXQeifSNTd82jv
WexzfGRNb9+OwYR3GNsbZrq7XZXFpNAIYbaK3hSQKhev0IxLIhda45doEqk/g/K3hAKwAXn+emPe
QZyt8CEGziEMVHUJAf0aQ1Qt756KfUSvZFz9Jsd6w6a3gA28gTIX160TW+tXYRksKhey79JY1rz1
tvtR4q/XdDUjFYQdN1et29uzeNHcH3qN0B8O/FRPf5lw/jrhnhNoTbl39ZBIwO04/XbVTB7XQ8B9
DtY64oMbPxhgS6YabqySNH90env92Xp4Ui0pEmroC7haizD/EBoX5ms7N+D3iPTR8pDT98+EoNhc
bO99IQN3seUd/zPj2IdyUnGlpKBt3NnYHNkQ+Rsh/I/gYXbeWSmWLH5jZwc50koUbvz/fHc5JLXB
JpBuimKnjY9Ux3SjR0Z6mqgl8ob4dT3giRGuFV2eu47nOPp5mC+zw8Xcf500J7oxNVr7d2vF7+yc
VFjnrmUytYAUxucgbZ8zZHrY5eV9N0RgYf6fZjjHBU+xjuQyBa8L7K2E19Z1ovcEGbslxonbbQV0
1XcwRUoN8FG1nWrjr9YcLD2nd74TTIPq/FcXFRGEO2Rchc+VI4lnSCM3McmAeHmZXDF6z7RO5uzU
NAmS+aMsCUW1p5yFhyiqwIDof1ihLIL7H3eSawNCZs3bXP+Tux4IJmaqWDVetLYQoEk5OAtgeE4c
ROI60aCWQEcYY/TvvAzQiKA2oXn/3cvct9Mws1bm2fV9RvsTkFVM/GqW9i3MVvSTCJKYWX5plZMb
jnozFg3bYAtNuL03Jut/gJdFKUao9QNExMhRkw5wRm1tx3Prbbrmb6pNbm7S26XA9rTMAjuCWeQQ
bzJY/S3LiPngDoB9uRS0XL1qEmh4nTGgoVz6cvwpastPw85qAXPjWHuwPXdP7xH6feAD+5Lkpd8x
B4AUUA2T8P8gVF6kP7nj+YI0sHVAEIHCVOYTrpNgCDKgoYJyoc/2MiUvCLs2lMNo6cglKTbd6b2e
srQLSXH7qoANZ6vHCvJLhOtceuBLzL8fdIj7qzFvc4qJ9TAq4CJ34+YZj6C85Sitt0KA5GFVF41P
NwKQaBkHtJ4qhljgX2Kd4dhjJTvqTzqy0R8NgO5WFYdZfIaKTyYh3k7RLdoStuBm2IUFvsnEIkG+
tMQD0qKPb8/zLihYThUqM5Cman6xcvpusFUqFEnpEuScfSFprXjgbFcnJEvypCRTcK4vKIdpcdUU
gmxcfR5ijKQ5Ix9DiLk4fNqDWIESn9BQLWdm1Z8+VjQcWS8eiMQhXbpy+vavWEDwnRyEW09JdTsO
cD6FkVTFHAGm2Ww85Vf4dgaZNlL6xSMCP9l45DBqXJi4GwuId0oJJM1GzL/0J9EQ7StSjlIkYUU3
ALU41hxNfO+Pd8qcX/m/7T7gqWsM2Sw7jn6go4C7Xmfr33EOidpghcpgNl7e49J+G3a6EJJOe4uA
Ku8Yebg5iVj0o/ZuhZnzBOItAXRdPh1yTqPfH3tPW9ZSafSb4iQGzTXYfU2lS6Yq46lLM4Oz3Up3
YK1z0Trz0dKzqTIJnIU6n5UAqAkbWqFMcXXL1A2LCo4RQFlb6uZvzU4HVjNLPDGxCey04EePufsR
pVoADmFOuuLZU/DlyluAvieWC3VUJv+BABfc2/WoYjYkAIWdtshjWmOu3FPPzmyqzwAN5h5YYTAu
eBZTi+0550KmKLV1k3Q3w7Q9JH2iSmgOgBAG+jcp+WhHXjYywa6f2cKhs8SOeuauOLmEli7CGE7M
5qD5O7210i3wvmVlJsQPkSuZ0PFITA6sy7yTz7Nz8iXkqPJQ8oWi4hgksHFVPYVyxFQtP7ZO39z1
tW2KdgTC2IfA5HqT5xn31we8QfRl72y8l7D2PoARCxwHzaP299at8Qubla45tgAWa1pmU+s+DxKt
fD0LSsJ6y3cCVMO0fo6CWeNAjDFKPS6qRLXzMAJaTiK+wX9+utZXNLzhuq2y2hkQsXv8N531ZDPo
Q51QjQxkEbvmwkCOo4mQ4Es4F7cUUo79uNmCpU2JqLuAHlmS9b998Itvr9P8DCJujTnp/CMP1SLP
ZwhjwqKbDGIelNFabIAm7yMAqyILN82iRntsBQvD+X7aUWikQNT9PN9HRxP+637dGtdaS1QVGF7g
f+wrDOddIQzhYFrX+6b0UXYYgvBwrk/px0+IvDyPZg31o7EdB0rsCKRYC44ZCE2k6r7mUFMRL8v+
kz99TLR7WImjsyKPe74zfalsYRjnd8Y2yvbAjXs3UTrtQl3we+aVfcawB7c1tiuXvkSvXUlFB6tR
WH8Oju9pHUd+7ZHcB5tRb5jhHLRoE+e2HvSvboYduzAyiYAF8uCmk/kZkqic8x2rT68toHasuT6a
OKcFlvyFnHxycTI0TdYuGzZY+ER+FswwpRKs5nmFBrvqB8gVDAM/t497QsSb6kudeqNFIYs1GUK8
EoJlmC2sYoa5h23xebgYjd8L1q3AiFcQ/jEmMa9pScUdGWY6O1/v08P4LuT0l3yS1APNhvPFuPhz
CHMyvi/m97awX+f32DApGh5GmLI9LFgC3hqVrsSlH6kuU4EFI9U+kxY0LLhbOSgB/DmjoFy+AM4a
b2O+6xu9RhPWC1Az+FWxqeDBLPajoFvI3N6GtAWYbVJ3bMfyhvjiMEbIWgVRidrjNQ7gm98tDzX0
LIeocWXHDX8fwLxJElllk18T9o3vihM6vk6jGGMxnIfd/OEmtpEJAVBHUmvnJ1YKhQBAYIDvAbfm
Rv1xASbi6OiVNscBuRZjuhOOe9qeHVB1K/9sJMdX/ttlQc6MJDcr6F/7LYHaGgr2spq8vrc5KlIr
vOg5cYwSK3Pufo4RjEo6hqAEJepwnSPoP0nCk/Wxg4aN+IlfbxpsrZL/fPh1sxExbtUBB0Se4xyD
E3rYSzAKZsAEKNCI6dDhxMHAKJsIrN+w1f5Lr6rotW37MhR0NpRHEM+rPdRMB6BMZUGW7y7+8YOt
UuXBkRZvkrTNT9NARE1CtxDQhXIBTnl41nwcLXTVnuAG0jBZdtzaD5hOkqLvFwagp9tAEcfive5d
DhPtgYbBsjlVOYntWcbHld3qjoTv4AOwZYJFylkmtNQcBxrFMcNRnAopYqcTdlqR1s8q3GPv7aLd
EvbXT0LfUHdv1aHWf4T7hjP7H8TNUBhosX5fE8NccvTPi8E/jpKbJw+ZcQEvBnpCxGu4OAxq4h2/
eu/tgxBvLmXstVg9e2MmHb7n6EHZt7dIJY/e35jPr12m7awpbxHro/pSyW3AFriGRduoLuvn3UxQ
1+zMws/Xp2vQ82ygoQUyvI3qXiCigeTOSwF5XoFQhExICU9Nf10LowEwOtfoT/hVcOyyF4omSXJk
eMcnxSABKp1SEyCFQI+BmNIf0DrqdOk89tw7u48OHlBKxwYvIInge8BNuN6jDDAth/Eht7eIBlep
ojaksE4fakmVJfR2mmUfkNgvw/hvFFXeZ3fSVEewW190FmF6ndXxuHrjY8OxBD/ZHwqR2fi6KM3X
svrnp1ihVxaRNSMT5I33KsLXNZ0fUVDb2Kn/FeZoc1LSyQtz8GbWs8PaXeV5/+0HYor7v7EBjzbn
cbkpq2KSAF7e6OaVvLq/JNIzHIOrbtTc5mIIK5pTbO4UYfTxKvUX5HwkDOxbBkImhxuoRAEWylzB
8J+HkWELci8vNfJ55J36K3dFffPDwUhS74bjG6hsWPpjcJdQznkDx5djZZg3oVM1XU7yTdb4SqxA
BQL6Murk97ELA+IT3Pc9KMQFVbxxDIUvd4DuqFWld4AOo9dVKa0oMCGvmb71TRH3tyfNRG0wPxK1
RZayUrYjlHcDz7UVu3hQS3LASOjg2YNPZkzSniwVPDbT3HtJ1PJAD8iaJYuLChkzU+xUTUxBsQXL
xYa4ocdXSwGxwD3O1bm1W/5dfXlp9VlxTDA4aAyP7CA6ecuFS5ccn4TW4EGR9EvDGg2+dwwzaOdQ
PmLBuAkTJ3VfKbUHiKGY664iiCqoeavj0TaobXH15KOSnmIdyfiF0X7jQw4gsxrIis/CaYn6D3iU
mEtW1EKVwRbjqsOCFN0ZAWH++ujJZN/IE2xNDwuhYX1DWnKZUUwuEvFO/MOgNsSzhCqSff3JkQjp
wMdWksrkn9Kf6RIKQC8ocjrMI8h9eJeRnR+fNcXUNkXeDagZpu80lVxZFnN2N5tLgIpoh7VUsKU4
zUds5bnFnr9SLETiUpIYHfNeMvAyD5GloPkTGp4QGXWcYLRwvFl51xgADcyMtwz7GNy7CFKbfs72
F+jTbdgg1XcI3aJi8gxQU9hN8ag9702bvW+DSELMEsWtDGIN+XhvxTn/J2MN4c2SjjppqMKUwuqg
+x3LnS+BCvlSlZ9+ke+HcxnY7eg03SCXV9GLKbrvBkE5N/Z8k057peNRTPNlNT/zqB9H7M0WddlH
nhe/wz26+romhSVu9qQQ3oopcWE2g2y30ywfjwjMZdUU4/0khjPJ9H8mWsikGRfsKNrziSsM4WK9
ARoDc68qd1mLRx+1MoMVdSWf+m5Z33irmtlTyprkLeCf1JqQN09Ce/FRzqGGBRHGb9KFda5PCi2t
AIWtFBawsNGxZqXSOf321hFWcS4J5yuiBFVm9zWR/6xIvRGSVHezOumE+9foDREJAQaf16la7t4T
pE9nk2rIqPuNit3Y6bfXzdDPED0otRAH9eW00L5eiFTB1XZfiwbucp5Z0VKTiHwZad8LwYaQaMtR
VYvvCHyFXJMQZdjYJR+sY2K+osbqXiTtRE2jzZPlb2MlsM5O65lZ2ebSxkSCcJs8Xpi70+qrXz/o
Yk8ZnFTeqCVyCmG9g7EhVZd5kqXQwSRNsSaOeWz6heC+Mx2St8yxeTx8SNpM4XVWlqb5cT/GatMo
DB2Vm9+Zq7TFfU9J4d/ppPvm1D7ev5hUIEq05JyvNeVQNQYULiNrCt5dAU9W9ix3KE1nT5RMiUNX
Fvf7DfvD7veU15GALCSn6WPjYEknd0QZm4xMa6KoO94k/caOt0WYyTtPGOhLpA8lQdimkNoMJt2q
94fmWeLHAu7F3A+5baep0qvh3Nivzdzb3HCrMktxBVCCeoFdokg9pImtXt6GfIu2tcwNwYff/ZMX
9SJIbmx+nFrFRbcsRNo1jQ02A91eLyPjf+13M/KtiN9FEwqKPv9kLjf/Am4vDQITRjAj48Gvwx35
m1XfV9544PFShHel/SdGr1rUBY7syCBE57sEr7Ld7qU9Af5UHPR3t01RzlND7DwQ/ZIqWktkqIuB
H+2GFs4mifxY6QQbPuS5H/H9q9hHcpL4LPAFouYE5M+skn9jzMO2UT7HvKVffIg95mnI5ObvUuI5
guIzpyGU5y4nmNaOA0PSr42/Ud4JX/+H6k5enuetTXDUsCbEO2zTnrjSjiFGbm69vp5/uEeMKqoU
yAwOGg8pP7vM+QR8N/Lj5vv+g43AXaMK2JnJg60vsLQVwwYKUzlCLPS1SudhM0L1EGnm+eIRLFvc
6lCuu+wUCgZEehqJfSNFVDn4Rs5vzgNJuhps7nhm6m45yrpCW+tjkNQ4UUEBsTC0V4yyHpZD39a4
gAvbSL47pA4gtHnDNJHQf7PPIVxCMXRYxv+pP+qew6BgaJc0YuqqFddAeIJrx3AjU7k4EBUbVZXp
ZqJYubR7Q9x8vi9Aq576Ycadv2DXN7InSdh8ZEfYgbWrFUIit4DjT7cAvIda05SHhxbRCqCbuZaD
+6a8fPbu1ZgGZdGm6Iik2IoeyCorUkrzkuGMNyCGtUkjKVHQMcjqtwzkdmMlTQ7KRQ2AJpQ9uER4
gK7V/FmuZhGyKCkXcBISdSK41+UbtFj6tyc9RZM3L4yrQ39OQV7qNT05B+gTnF1UDW4GsCLI05sc
meom1759jZjrKnozUmNkk6nc55+KFhxXo/F0Sn7croQhfMXZ4a7Ezxx5QDcydgkcGhEwxqT7sA1/
mWFGsIFrJZnzSJ9VGqIRtswZYxiamlw6c5j66fTNXTMN3W6j9f0H2ogTxMa5a7F7z/52Xc7cKj2q
hGFLn8sNTsKIMouNr8h7A74L+tKRi6wtG354jewrsJQ27YagqDlkfGQjUXfkEDrFIvG3sVM+gMrV
hP28CPb8CB0YJIUC0v8j1wwaT/fus7ReK4BPhetlUJew/arPIfrDD3PgnPXSpbHSjiBZcMOsQO1y
kJ/zRVcWSJE3+3aAJYlPceDrWtJQ0CWKzE7NHGMBp9svCVlLC1y6L1TGnKX4aADlxM7RexA8f+HF
4Km+WLdgvSHvnle/v7uE1JTOdWoAbNUjZ0G0cMjj5vSmgZc3cEbh12alMg77KhsTVLgQykm11K5u
0CWjhcUXAh7WSNOMWetquNF4Ub+UsTFq3JQaslr79bJcb658zj4TWfrnVyWOa+RKwGSbZP6/DSZE
ZfNFuWH9Mak8a4etHIs7tqDYsvPxgtOiwl6HU8niEtyfqEDXR9SDZj1clHnVwiXHIImox/r+HmZb
SM2QbahW/WLAnXgXB9tPTneLEhyS8lIeaFMsiUXrN11aqhi6/iB7qN/BAXbBCRGDP898NxP8GZbb
7w0fKk7uAee10aCJ3hc731V+SUvnFx99q4lb+ZOfaX6zGdI+o4PFbx9OCkAoJq4bUDd4i4np9V3c
t0M0Bjg6KvHfrmBQ3ZUDXA4OdMdVouu/tFqcbQOKYKpSstSVacsS3NjKLAmu2wWj9+NYzkcgkJpo
o/Zyc4ddx+xFflQAnW627jNsU/bXKbHClkmgUZBTgzlc6yEac7gWeKmXPr5tqvbFHVcEU9/EglLU
cegNUfGOQpCpPKgfFlsRWR3ao4kyNaubPHrXCT2jFt1JOrkjjWs4jIDs/wMqcykteBIaU7aqFEZX
bInoNqX2qsmLX+zokG74OEmyjgm3mwLA+Wz+Z9ppy/MOng4KfiAfdsU6Z/R/ZVMpUxlNGX9XDGKJ
fMtJg6Uq18p4qkctjSUKG9tVMhcrc5byIZAjz7eRR210wa97pHKDAjZZW+rk8T+S9GmRe+erqAvY
Ap27xAhkNRz3U7Wx1U5BYLjtcyEO74n37lzur9Ns4Cy23GWPwVRX4iJnoWO9YaCt5UlTDAnsAK59
rpSRPgBCNyuljlF8HmRfsH6jpyZmDHt9lZ7SpOslfbtwfsiKxQb6R08AXvEAKcsYrNwfvkUBXBfA
+jUC8/LEbI0okoXMYAsS4miAvw8rp41Ccl+08lCwHGPtuk7/Ot2cDrnlE8X2CZbJYG0bUPGOmR3+
CRAeYn+38AyNniX+5UZ+Bl7wUsZ/hKJ+SgErq+j0BCqCneJOOAnRAHEmoGlIMc77OBo9ZRS3oAd1
H3YOM7TshwtzVjMX0FFmWMcJ6/bGS8tvJaptWJHCl+g7jTtx4JFYIKn8yx51giZLDPnKtzPnY6YC
rS5Fobu2d2MnFnKimF7I1LZYbHXydvOnJ8neMlf45YBPdRBZ+frv2Vn+GT/cjl1KqpJyzcRkKPN7
9sSm7axwk4knVmSxB0fVrsjJ9m+OboLXgWBIhntpzCOU4BCbFRvuD0UuPIOUGqpaPBOTCXtLv1kx
nnuIggtcwHlgnnGF5zvOmgoEuBnj6/f173Jq6OihmPbFpMU3lj1pTgtKP/em2Hn2CRKaVaF5TOOB
YxFS3OKYeoJ135s5VnxOJ4Q9cG5cfhlTtNLC8LiPay1LIteeFyaNaUVtp0ozPMzWmVyqItufeURx
iq+fjnzP11vH7A2SC+s6vIpVZGo6jFTUxPPTXeJVBFG+pfpOAKEgVeZglM+v3DW5c5VcTpySOVyS
WkIPoBKnqMveBWkr3GBmAV1cIW8bmFp2Zuk5gqN4ilCqrOp9WJgR3D63PbOY8yEEZQ0YebxfRsoZ
5BsrCcWG7HVCfEmi0gEv5MfABt/QqtLy3Kz+6a+pLDwO5HuGwrG+bka8fqNByEtEuT7RGCBbEO69
nC1iW9EsugskZZNvTBat2gFHe7ZiBtsTJ0/+lAdOirmVdQpZvFsl5b4wkpwyB1GJ/x0kR1/LjJz7
5gvnc9u+UhWv0llMqrMW8PPymyK7lIrHvXuOPMjqALLNGcXTfzhypvPpc6qlSnEAc6kDQVmlVfhA
mIceTbKbLMaGO5pBSDhLXR5lIa/sZsS1W77mCNGdsx8xKaB7WxD4GvRTEPKVVAmJ6a2hiGeWlnt9
VFJO0w8Wq8MzAouHvtsdTuhtlhDzPt/H1jF8rPMuotxvN00XHWgWJZm/rMFC/6b8/cg4tJc4ELeU
mI5kPtF0iSGS++jwiG/CL9AMmwwV1/kVxqVNeaBYvRTk/xfDg9XXZDkewXeAFamKznRxxj16btar
ZCE7jYdAsQOD4aSnLu7djh5P6Tl0ZJLrst8OgDYcKjpc1pDtVmLDS9ato4wCeLndlpRw6/ZrXdkv
CNr9+/Gqh+tvPGCvi8RsjoT6QrB3VnoRFII+hcMDnuNAzxdI24WHrxMGgktTcKSqIT72uiRisKGw
xrzlelEKpcoSDcgPg6LAzJ6imiSiABbHG4UCVJdiSF49cWykHOoNxcMc2hxE8lkey/3VjSjA6JxW
bbkSr8l09qy0hYSjAFFL/SngoE3+3c/e8fQYGvU5WkPHoLrFulu68tlH6iERpMCakGgqp/zwF93B
69KQZXy7QXSGyvIXP1gIY8bOmks5gshfSroAQ1sQSgfEKwNsBhd46T1TOM24/rToYclhu5pI7HV/
vvsFNvvnLtpf1QU88uVXMB2SkR2AVdIov3r/dHNm6pe+N2xdBdLsNNbRnLvLelnqWFFZvaLjoIhZ
B+i8obYrPV+Sl1qhDPwnPf5AAcxkAU5EFbNaoh2cT0PZQucEX4iaZXafsGaqjmLqdcyRNW++2suG
l5lGbs82UlGBG6zr+YoVeB7S5C3uKocMQ9ZvahxnBJjsFt4IODWysXNOb5ay87JZUBHcxw8Wm/ua
//Aw+agaLcfDJoxrES31YSCFtpiftDF8eMYsxUtVCq6uWjID2G/TLwD52FI4x6wBcHuMMYdRMibF
otf0dD+CMhMTjYltbPQzuc/W30qOQ/ieIqP7eCt+AZYcV/xoCZKccGFP0vDySouB0SbDZ2DpPS1S
87InE7O8Ci8yqj6byUAXZ0D7LivyyT32NOVNELVBhvUQMcXWQDFTsalEze9undKBoZlpwXcsQ3I5
rZ/wWjc1m7fHn4VhcaiW0n8Vc7djYrnDHQNzI/JrYsbKShleOYBkXxI8EBmm8Z0xp4C+fwnpPmF8
BQu/0RE9Nv9dhdj4qnlU7zxYQmvTVC7h5HRGT12/WXe537JHWa+x297KxCdwHPgIgSvsIb/fy4fh
je4Fq9+TUJ3CDz0Of50Ma26vZUKsoT8aiL38B+AOcSZM3bM0Hbokj1/BWSC0LVokt3LaCZcBIA/f
xCUyQiRwE4QicIFO149aUrwOa/ovMX3Z//EoGt0YicI5ofwsI5X05UIcuwlIwu5aQiKJ4IS03FVc
xhnvKC9pmy0N1jF6kGDPlp6EuJtdLbHWtfeE74yO4KOggLQpyScrsq4BE8ir21KS2PoYrhP1P528
Tzkg6FE2qPvzu0kmEZNetyIsVGFFtG6+56mU0TcfZ19my9JaYlc4ET3jhoc7vfuzEVOzM1nQLG+D
uNASP0e5ydgcDXdy33o1dfJUnyRza4DH6kiZwl1N3rrGsGFxeY91ocdz/WlSKcpMiPugaX9tiiNO
/53WVSAjKVYy7CHzqrGB1VqHVpm3sLsYlTA+hsvbgzNvhwWdtBlTHKDX6xsWb+XemRnUnUrTzukk
gSnlIhLgYBLANlBNFX4RnsMGNuUgBlwCW7E430ZuIUQ4MzVLvDNqyFdzJ3sXYNj/PB4CTTzfKfO/
67zmJ4bL3BrHAdi8NiJNzB7pKtR/6RAy5PWp8rYBthKG8Fz8TiGwmvhK0tQiWeq91y+ot60HvucH
UYIl/cIfOkk7o8Kfw81LXyguQxtctDnVpYLTEEGopEGa0pQDSAo49JkAZZ07nAMgGJC5LGob0JqN
FvW8tUDYEkiopHOA9tTJDcAVeM83b4KYvcV2bacpswch3LRhJDJ9dgCJ3vZDv79JUG7dnfG1I+zS
6yWPOWViLcR33v8+Jy0QjW7KLzbSDEBAPDko6EvMRg0woF5OY4V16HE9j6nVXn5mt3Ps0d/ZH/13
bEc8OcXaQoSK2sBhrO0oh8ZPX2/RMmyZOF3H4zBaCd0tcuaAnZOrTEl+GSNSHtWRP+bqmWD/WE3b
48qmUvomyj13Nv3rinKHIURWwJ+xgleE5Nj9w61xY47+2dSV17t6bYvqBdNTRDMQJwV3PJqsyjJ+
7nxmXrovRBDTjE3mTrAAp/boPeoPPUDIrglNzKFGXfz758tMJu/6u9khOAPqiAf3JYypsApU/H5m
x8qgxILQX6Ou6gJW+tXUrCQbTFawwxLFEzrUsdnW/Ft3z0CeHzAo7eUo3fG0QaXOY8MI800PplZK
dXdL0HCUjaEKe5gGAC5eL9igqNAr6d4+EV/4anx8nISZ37VKqy1CV5uTxs+jMlV/bXdV9rEkvvBM
1Q5C/EA48pMzgfxm39hAm0vdGIESz7AmuKsnxjxSphZQl1DxcFZ/RnZHZRBxGBXkR46PLPoq9ur5
XnPW6vT2CDoiqTdH4U7iEJTG8eR5PCX/MW87dj5bUm/hroZCmlW/Lz+LRIEI0sffbLc6JIvdaJ7/
8rxozFEmYVHDavkodeHhdNWQ5fqKmqdpq0eRU3zG2NyWHJAMMB7J16v0r4BmjBkpgiQgc2NKBl6V
3bLnZm4wBznemnzYZ+hmnmwE6gXWXkDScs7TsfvyJ1wvTTk/VNEmHwP1mOB4ceX9ad3mjzG/ynmw
hFpTqWkSgLR4vZd5rVcxLXo+uXIU3Z1FbHSj6ULnvAWySO5xidd6D2A9AKXYhwRZt0C0/qxKeezC
9BDumRnKJzzkBfjXqZc0WzqJ6OlS42Z8YR0BwmrDvBiX6zS5j4uzVtTuz2uOI66z0d+qOJdtqghd
uvsRRGQ6Um/TfwBg/GycsAnIF5wFQYptrtIKAxWmPTIrext8eMvtVYU48DudH9Xz9V+53ZxXA6+W
flMxGuZz2wG1LEhkmyy85aTE3NyP9e8K+w8IGfbmtH/I9iLafR9ICz91rNS9KQBId4Dg+VEbsy2T
zbI+r3mWGUE9jeMNPUzT4FqsXFK0roESmkl9ZX2fjCIX0ZziguuuSL5RbmzK32No/dDHOUjp7dwV
2U3ROyGN5V3e7bNhsT6gTU/yFYA8vQv7cY4QS9bYZWJegegnWW5/eLz2LIXjGMrJcWpEFaggd96n
WkU7QKwQ/J9oIEMvDEwj+LqU/pWQok/FCqbNosd9xYsIASb5nXxCje0PJekyEOTDdjqdFmkHduX6
QJtDreGW5soQg1bkCRjSbUS3T33g+u1qrx28h7+cQSv0P+pcIHmlGluK2rxMLpLupnxQOFP15NGZ
Uwk/kyVC0/IVb1UfjKeKUdOjWCynY1d7lO67y58boQgiKGy7v77FyRSBfeXJSpf3ezY8nG6Qocju
5c7WYTNyocrGEcO734ltAna7zqyQPTX3ls3I7vm/Z6XHr4K7d+NP1kJJu22UjWuFkaoF+10g/VgH
YNGQPOXRvBWxq9Zs+Kr6hsqKEwEUP/cmlV+d8w/Z6ZyuPH5NsOemU9YE47d2mUAmFE15VX76t/ki
PPKXxUeYk10xu+SFPMi9DoNvPpAAVVCiaPvEfKXQTJITP08fGQhareovLmacOC4BLopFg8n/wNsL
f9iLTkj4X70bwrb2COnza41TpUYn70/ZzZVVeU9DMf4U0CD62KnfpypbSWhpXAhUWFEcebhxnfw8
GpzMrieNsx8CeVRxMB+vXUVoBYAO/6ivk1iXNAEYFYmk0/c4ggb7WKGMAAaHFFXcP7ipzoQi29Uj
CmkXi5xdrbevaWKySwcF/JDKn/lm30BDXKjRDNdyRx36mTdqQ84ENK2/YuClJQwos/zSDcYt1QEI
28EvO78io16byQYr2gdgbdeOlDeY4rQD8xW+Z2KzQokyCCzFsWrd1loDGAb+ZGLj5ijduFVLE6CS
FjmAW347boKByHc71puQTMfUhfH6XZTDFGWpt5uP5cW43VIO3HeseJDhqW06Rz5x7TKRUVTgQpcI
XbJAHPbCq6wPpbV6HgbUb4q+a/8PboF0/RLBWmKLTjB6YyAHopJKhgNOSL2sHXcPH+sEblT1UV6I
Ce9sB7gbC4+eytEZxK/cTg7xnt30743MA2V2jQb272+EdAm9x4tUJ750j2Apqazax/vDLjgatgEu
qimb2Whdm1H0ZXEw8jhaLMNUrZkuj946m+DdT3UQu0Bc5/F8cp5ZQ+T7i+Ih3Q1aHwiFwrRBQaO1
UwDFjZlBJOb+nWOHHvkOuWAAoAVae3bsvsQ/ajZ8J+L+yqXzcUv/0RCn/9lUFPyPABqEDV/sd2Bg
C4lUD+jl6k7AmTuhZ2mCbB7xAko2bG4U1qbnEvg1mUk076NADPcFHoZia2rj4JhIhe+iu4bojSZn
e5Lp1sfqRBdql5p9j/JryY7fakofTQdnJKUmnJb8iuhQrZEohCzAO9AC83gyOlJQX2um1XNSuuMu
ck+mmX5/gzbDNIYu1zt2oCtnTTgPTLF47s/TrVcZs4KsFtC5CJgWRfBVfnwPsEh55YbpUPPmkdZm
+Q3mzgHRm9MtzgnKxYzeBLgXFDq7a7XzsDR0cNK99YC/J0+gtZbxy8+8ue5sTKSk9jp5F+eY3BLC
FsbEqHLaYgWgu7JJI/02B9hCUvFHzKftuA6EwjRje4CpD2yBdK8QHI/+fucE8W/WkwHLm9bidt8w
98Oiuj3tk4iLYEit4tqhnLGFGpEgqyOh2QYOfBnwCjh6ChxQ2+x8sitZvYJG5GTF5b8pbiUJgW/W
2q8HdgJxI0Ok/MK0+Xi7ObUG0z74xtSSAxJ+pDt2TQvjiQ4rjOV3KVmK7Lvda0zN6yqY1OVmfRbS
g8ZSXYBjvPpkGu3q1gT97BGD42sQ7ogiMpVAmDzopXYdhKxUmYFkZIjS2q/XFCG+zLGQKhgFJrW6
lz8e09kwtMhjU3eupPUNTWneiGlavckwDtXOp4vjSSC0jESTZHJ5kpUim8xBltF9UpEXWcOyNpiU
gEX8tc90pWVq15GEcO8Kd1EW7z0spQs7NMPE27LLullIik34Bs7ogpMNCMXvvUObcdwX5QDnQgNC
Kif5d3TR+0oNhIQJWGs9axQQNfeHC+YMCPKA2KSYMoYCg/Ye8QSOM88UxTS3RIjp2ZAL06mUC+1f
DYpKI/zloi+BkwkvIuTw65Kf5tQ+LFj78ke/oosnyzygJDgnSfnde/ENKMLZKuUMXCaZ0Je4WvEb
nGps7BnT+7oo7K6x/uLti1V96h0zHWm6IW8iQ+dxRvWRNlSS2QRo+4ncCMAEyERLlWFrinZt/QTO
ShsrPG4/xvUoPZfoQG0BB6dw/o0T4IrvnZkeAfMezex8A8S0kC82cdwysBG2mEo+9p5hSV3FuPk0
L5SZ92PZXZBiVWzA8dTXECd1eM4oLw5j57MNvekjOKUy4/kPJfQF/xmJ/36R8Fftq4R5Tr9O4rLm
Xnw6Wev8viBnMwNZmPXHlVrw8CCCZedHCTh1cCmodjf8pzrgQrtJ+SlrRb4RBIj0ObMx2ONEC9qr
zdJI/t3WSxr5znmkUnc7P+LrvbuuudhMs30JtXAXkDTr9T6V5Zrz9zSZM5KOcEtB+qjWIgmgAejV
jtfi0fro8NZZEMUFVqC157xOJjqKRoh7kocapq5xIeGodorG24AzyVFGrfBsUMEdeb1O2eo6OSmC
EHmdCdnSeMi1oBCCfMP/lujhPI3MM7gyym+W+yznCxgqK0m06GbGi67pAAsOyu5FHn5XJqCgk0fa
PyjaqpHJjjTpdH7xe9YVExfP6iWJ41mQJxxF/oaSYS2i386NBM4ZzoclljVzvu5BavOwsCmjyqka
Lrih4Ew2/2Pen8BrlaNl8kBowUR8hUYz7sfuhqiLvoWs9be5IkHPHbNi3c5LwxiLom19D/tbILpZ
UHdnIgToE0Tg7LKgZkK7Myiutr/R+dTBnjKU7KZnAkgNUn3WHmyr6vJ3Az19a4gMvrKZHW+B2r1d
LoYkBA3YtyLdN9/59YwQFa3ltY+8ojgZ+sSbaBqxknRwO9XMjFEfp65Y6s1pjnnBNiBm/XE5klCY
eJ2/tM3yAyXdUzxBVkE4rdzjCebvt0XWuHz1hcmZsWlqLVgTh18mvfaWYy3PSCep2YvdfVVk2nQL
gfMjGr4o5+n+Cx4jrovN8crVw9a332cJ/Nc9j25q5O+2RVbwyNIr545lyaF0M1uwhtUuQOoUuoun
NixLCrm84BPCC5UruDCcFedgYw0P6+Ont0Sr4M3/FUe25Ag/uI2vWBRXnW0jEbLDtD487jdc+q9y
hV6I6jpNmzDIhkr+GCEe/jpA/U09NzU9YN2xelgagfe2yHSWLV1KOUlosb17wEoe1SkR+uMvXRS5
cJAUXz4EW5CLqsGPkbwK5ARmKlJ0jAG0d1awnhpNkqBaOQWoxntAjYIEaRQavlC1W+lvnEZm41aE
NVAz7XmaKTZ3gZ2OkmlA4QIvlWIAmOoNWLAQQFOPxPtctQhgfHjyb3ilEd0CAp3m30pYhfGhnChx
jcRumt62eDpkaD0wSmwUBA5wjWnmju401O221RcNQ2QkprNYHsy85qfItURA+2fQYYWcWDv/vgUd
CmmZ31o4+P/KZEtoaPSPu758et4EQOhCAlfOM3RpMS55mzLDMi+Ig2+ysBIVPOC5h5/dWH8du5UH
5zxW6YjwKlmnEEQJrZuuzSeJm2cgdLKboF8LgakSv5xJaL3u5oZj16MvoiLIq7RKPpsdZdHQBFG4
zLYBtmgggTFJB1/S1Lsty2cH1gZ95fuz6HTjNQibw0ReXyXrjjmi4N4k/J4b3C3kZFtDKhSrU45F
I5zEMHKx9RCeGtrgI/CFeX6r/flrYW4JdhVSN94JboY8tVS2A0WND9dSt4/g10MK++jRP7/H+khj
HmnPcXd4XtaIuSEbn9Ga812xcx5u91IZO8kT6jOQOpAyqLED4ae0Zld0FriZln4rIxZZjaaHm59x
eQ3XugTIjI4002SipnEL4nHKVpVf6M+rR6h0ua+/BZRvqNWEMcSXHlXUfXffGXlqyYx9cAUCjzDQ
80Nlx6s71h2+T0vm8q38YJE6uEjFVu5wbLEo4ifN7sOIYMtRPyKVZjBKwX3OFlUxR8n9S53P5VNb
DPVd10rp/6/ORjXQe68Qt9Gz0sC4JWCJ6eASzbuPhhzc9sM7Oh0Kxd1EubAiwlEdg6U8SqHmwEGH
rg04AlDm91Fa+lKKJ0B2H2BXNzAgjbVAUd6t98M4mq329C++NURejdwapPJqBIBtPw6U/oixCuSi
/7Ho46QHupXqZjyzIANzO562MktjCjfShbklXvgKBvW5XhYF8uIpALXOpKYAE4xDVVXgYWhXLWXb
yql1ShYbn952CrYAynyLHDmQplAoy6zd6ko3T53KR1gpoGgzYHBkPT3u9xm/iPBxK8EPjYDN03Jt
upWygehRvk/OafiupCFLKmYXlVwVx7IRiJfRzz/TDz/+J4LT9cio305Jxdk47EmR4m2BiCEU9aT9
WTBkHTCTieQwO1la+u1zVk1YxKFetMN5HRDLyE67eDnv3L+rVpeWnHVsx0naH6m94+SbOLCAvg5Z
SmZxAb14teB4ZpO0gKwssjQS2KUtXISQx2u3Dnx/Atcvgj4nZS6WU201Dam45SW6MyNvbUeL+ABe
K2GbKzG1sfIFDtQsSFyjU8CxWQpuybfHK4+3SBhu7o0s28S7zqv/xmC07fBNZHHgXCNNmIc891h4
/MWTMq0Bik18d6sgBCvWNdwYY73dGaATycKPTJx9tdxlsVnPqQ160Krwjt+6Q3SHMHYMJOZ+qXaY
A98eY2u7xcqSlapvYyQLCN6Ky4bZqCUX+jgkbRKuByJDbrFa5FmqiZjZk5tkMPE50TbhktDoHAc9
gteMIeN2q+wVUSfgGyZCeYP4DXhY3dxKzmSzWW8GQjnIDP50ZWkWu+hf0Mh1wFKCUfLLzhX+ZkkM
pdFFNAGg4FFTCp/vsm8XGF7nJ+wurZRadZi9tDW0BykYwqXv4TGg6KopKQ4lzvGiBJSxv+5MHVia
m/GzYJqLJ1EqZ2Q55hV1rIfDEomwhRwYlZLBM8rjFoeAweb+IA1yT0CszL9RTY4KCiar8gNvNy3c
iWUAggR3mT/Zm65yXDpFJFDAFzLR1/yB3jn/VbQvHh4TYTqADzlfIWzQHYUzd2ZS21NwYgCof+W7
2nf5cRVFk1zcUUmCqUoGOvwG1cywJNcFTx7lMpZWrClM1zAi9VvCJo44URZExbY9RSXKao1Yk44D
os4UAQkEiycxqS7WlX+8gWY+exSuqxWiVH53VVnv5wf5G1EjwmAQ1jvplJ7AGr6R4YS/ysnN3Hxr
ZB54IaJY0/3WJ43Oi9b+IwWE/zG7ocMegbHXaYFcoBAh91w5YEI2TwUHbVz6r/H8HDzvMCB3D1UV
P8z+BmtmKElyiOYRlkP/Nn96MKTCZpJ9Sl0oZd16skVwd24T8YbjJMD11ubmvd0RXyDPQmx7IYfw
brsHy61oYZ0GTar7nTn8iBTunTVmxH8yJH70/EMdFyxsYaNm2lkuFwuBexxBQ70A5hexDoa8PLjC
EgeR3ZyiEN++uHd7mhBzZHAVIOKkZf75T1D1LUBuB6efyla07VfxLGFcwKVTL07t2aTfj7/DkBd3
X6uYar77WcF0gtjCmq7ImkBPKnic7VmOpO2KnUGdPMKJc5o4DNp5vy2X9lgKr1iA6ksPkpOGLD0v
ZZ/4xak2OXIi84zqYlloDHvJQwBOV191MQmirLw17CoUml50gG+ZUFXWoh4b2RLbJA6Ub34jUKYo
nc5STsC33Fh9en7YLC51aw6JtRN2F8ylEUR59WNIIE2PHwK8l3GiivIkrwsZGQKex95lPQhiUEJ1
ZXSEXe0dAQUc2qR6uk21wetMqfkHxdg8f9OcXwGXcNuoEN3Oxlcm8yKNADaYdnTa3RHzcQxG8PgO
6RGKc7fPvCFVGQCsDcZRP9w4WFMH2Ie14VVnVjpmVF1hN5t7MUN3LmPXAZORZotPWB8wFefs8kD+
gAANqWV6NQLAuBAQPN0SvrNh6OblypH3L+VoBJ1Ss8UEIjLUnbggykV8SqXPZqCvTHVPlMCCx21J
D3wwo8OQw/LKwT/SpXKKe/fmXExPnRidEpHQ8BQpckr2VTqlShZRjTIUygpbVh5nCeROJdEykVlS
TuhMQ1ZbK5suZIn92gbYfVCM8JZ0sPvlHcSpI/O1y3r9ZR1tHMxgatx21X1GrTloOnv9nyfCSsxu
GxOSdEkMXPNvdjPHmhsFJGCzF5vdUrMy3DU6zsr0ODyLjsLc8/QqyDOYwJa/pYcC/Eo1ji4y/Dvl
I1wuE7A7fuQG3Lk9NmVhNrcc44HQr4MqqjqGbJFCAJj4dazDXIPVHGjB1E0ldw7dNQP4tyXAw6U5
/ycN+PYWZWvpFhqf21Y0qRp7QrVyY/zYihXH/sK03Up+8aa4skKBYx3gepSiJYn5W3iKgGCM0R9F
vcS5AvZZsqdu5jaHQ5LoUaePspuzmhzC/aLI7yAxLcEl8pzsDmNt9O1S3goUgvRjgq1UosJMEaWo
3/WqPU7eQe5f2mxt8Lmm61jH2LLYfjAgwEw7Be/NYyLsqtjQFO9pzK2Fs14mTLovWrtWWhnyLydE
cS+3joeUNti1UbZT/KvQj3ujAHXtiUQpuN9tX0OblpPT4HNi3k8kCBt/13KrWzI3VajvxbcIcV6o
5IkpjGKWXkhDBfjTAJ03mGzpWTV6A+o+bWW4G5hEfGxGpri2TWgoM95CoEFYzvnt+g9NphSEh7xo
7/XYxBApnnS9ll8yP49h2mOMP8COGVIwymM8vgKvKqvTxivnbM4L5IYRu699t5e+Jjf3dl9gjmP/
hRqbWSeEhXgms8pO54HdyniefR4I5Nu02onc6lCCgjyJn4lHmQzZ9MdEmw7IjneBOG28+E+NXkwY
nX2lOGCMH6LXDBILxgWmPSU/OaS0RocE2gZPBKnS1BuUuV0QljA6M+OaFElJiNR69NgKjHVkgmRa
jjAbz8fiL5NUMuwNkqhXZ9f3ppIgJcpHGQ8AfaPblTnmK6PwPFfucyAW5QcAw0xGQ16q+n76jr2j
3IuegeSEhYaC1Hko4TIEJC5olMMio959pxFgVHkcL3OLCocCHKWUaOyTTHZrAmMpM0DdWOQbNJvd
8bK0WWFvH0P7tSNOIgLTcaH8Sgu8TVQ9bhRchlJ6ZZr1Pp1IMWL13jgMEprZ1xHTW+KEBCm5d8CP
QI+yfTIWda+5wxtOrvqSYSRNpzoPbKcjyMB0nMzpAc5doqcTMIU8F5wTPm9y/N6pWyQrJScQpFli
YMjN4xYyrTlmeH5oqunHMeno3joUmO45Tt0RfsG6oI3yvNO2nROpCy1cBYIJ3ui4bZzzfyHeM+Oc
CE8MbSt9yvBD0DX5j9dBNdxhBl6jdZ6vIefdxeFKk4RtMsC5Sf4qePXLaWv8Z4O1nPCucAlvUyYR
ceOjMggHULwsqGBXgiEcouuQv2sk9D1Jd1u8mYSQ5EdTqfFYgrqZ4vCkKAy3OC7B1BfUQ1UBK5wz
A00lrecDiapQOIzy+6lKKpE/H/WHTh3YRnNo9KDnM3s8APgRoIEhXup5CrpCy6FP6jGO3WSSWKIC
ETUXCLuhhWhfTQZluRIYHV//q3zobL5X1qpQA38TlW/chuZq2wKLMDc7kPZlNZqRcsil4GOOn0vM
Ibh/Zz5mXs+D/omW3tswBqY+X5EClTD03BbX1pozgWWihhtRwoBaeQADUe+g/svwG8ItRfp4LQEH
5q5RiHE6DESgVNwfind2f4XGZNxrYCCKYS+yqpDOX3UfscPL815fFYrLYR8E/f766VIMqyEwcOWB
RvwplB+hm9WnKKiSqJ5IBqq+/TvTqDlj3JRcEwkunJufZajVGJvh/eoZWxXAP+mfIU9qjHdlj+IW
OOfbj7Y5yCQzi+7WMUCrn8YBNN6IHCdJ9BInKYTkqY366VLYaZKCoQeShAKVYnBpSns8QSSBBNIO
eAQb9gdtCNRihn4Ux8KTRt/eSgsS4EtCggw1iLAHKbxFIHNdcqMUsvOlcepf2jB3ElZPneAN1UXI
AMetE5p58iUoEjqV9CHfr8JHfhMPzl6CEcE8Ywix7TWKgE78EM9ca1muYtSA91MhFa4x06O8ZyBx
NVlNnSiWVEHzsXmnMRh/QhygfXKuBN/8ac0QX98+rWKJYZclRMo02DWpioJDu/AWiV52n6kqJKXc
PgJkeF60+I1segNDLpgyHPIWAw35sYOVcmVfH9Q6WrJkVSUsEqLfhaaNIfSeHO4W5cKd53/FAkkD
beXtrYuCmETsx38vEzWVr00X3DaRr6tWfpY1AFlRm1ObL7dQ3vnF2d9sJ1gUMMUWAcHbsUeQd+Js
9wZbjH7q1dN1m7rzhIhpkzJr12ILApYYjexu/g7SuqCEuSB4jrSB3MI7gqFrFreFG46RSr4Iyska
wGDIqQdpzMQln1r/NdCpWs7RDN8ex1PIwfgZcfkW06uRT2P/8+N9pdtu5O4xWWMsixsOxnAtne0A
4IeZcylN1TQnFNGu+QVoLhXYBYg3szewr2zYBih9RCtDi/HvK2sWbLCLWWakSQQvYKeDHBypeMJ3
0iUq1HCI90dL5O8+2owXTHppxw9N1bNMZ9KYDveN1NDKP1eK4PHoeF3CRC5aUtT7QAR8ZNIyFUrw
1c7S+PC21j4aqCJtZllly6fUF3kM0SN+TdHEPkbiM8YJ5pOSl9NnLMxno5G/VzKfLsxUooXu1oPj
HBdzDtdYhQX2JnBChqmwXBzrOxgD2kfP/QIQqkHe/CaHZE4lk3GeMJVlTKnhjUiERnXK3SO99Urt
7aD0MnJldTnPc1zttbXAufswnFCgfpgZM2H/Od5rcadeJCD+k45AVqAU1Kg4IQ66VziLsEtJ+zHn
GlLZ5hxqIQCpwfj6gtGyS1NJf9zm9qq95e2v1DkNCLeLpG7VdRIYNfhk/4dmdjgYf7kUd3n6UbxA
z8WKHY2HUUc9UWUPnNIIj0IR9YUTVcVHkYrrXBtmK07bpf3HNBAhivkrAg4Aw5coOfLwRA7YB2IS
ayFR0PQWaMiW8Mm9b04AnultGnaTuksuzgBLBuE3dLyagadphizCt9b0at2MPpYtss9o1zyza684
UOQHXhSGVWzcZzaJJVTr8yOpzScKbJlL08Wk/qbTgaUxKDxdh7nPFU17jia/owLqv1QMWMQ3hSy1
96Yx9jEKidODGNEd+XF5bmQcN0n/C5GBqYapYUUYfro54Vv426bDyskZakWzRap1jXeK/XauaMRi
SyBC3YxvszENY9EcLvT+A3KS44DFRfx4CgD5QIPdZg1forjYvKhF2Zg9mmG0lFohP/Vlz+nZP/Vg
Evs4H9BgsgnIVRDGYFJv3DMSm0szshhTJo4HknpImhbUsEY/M3KmWy8lP5UyhERWCZ7QllNqTB4A
oahmV61xxeX4WTshm9sCye0+nxNoauGBpa3kf2Y98o6E2CYbZA73AIIUmHRNWICNXNK2l47CkPcA
gntQY+b4ThUTjuzyTTH/nbwcD1gZVbKPbefly7WNWSH+4dAts0J0uvTuh1kJSUCkS3TZQciAtJxX
ZuUVRxbLhiZl8LtfrO2ORFTgTzm5CmycQ5q0fKZBlrNAjCBO1fIAAdcM9ErC0llzs2ZgXvqhwi5/
f+x/rWL0fevAcVa1TjWUhNt4hxgubmW62VvGRmYKZtSJ7hMcxQtGFPR4B9TUqzJIsJlbHiZgJCVf
YqsPkyaNNgZF2v7ay18TtGB7o/tEbQ8ALXJkdzh5wZ8l5LyH1IOJTOIClmhd5/UOvXrhKRUJDnTC
MApMq2x6Ek23tia/AqE889C9hPPIO40QhXxeXjnLsrmwUkZoPGPz7aPXT6wj6aDmTYBMz7boUnsJ
f2gSa91rJIg1xWvKj8WLX3Sdt5LSwbhIrATYkkdP3Q5uyV9cqaQ00Kdi5uTgsjAO9ueHSAW31m3o
h51MUqTuI9nmyFxUjx6HJ6Yyl5SqbosPiXHcrTaJjotanEIU96D5BHAqbBLvz50vq0oXu3V8pbs2
bnGIE2lvq4wOmokiXuQNOn5ubHDksSM0wJmavPlhb2QrJ1FxpNeCXjSTUagtNDWu+2xg2SulKd8Q
YACxLxvrsr2Pa2Lpx0Vc8eUsh3WUiMorCqsSG8SWztXK761nUXh93hM3TS3To2uYndOTwUPY8Et4
B7Y/Cla42w8A9/F7myWp6rdmKOoXcsjv6ShzVtdt01aItcFgSGE0wT1K4F82EjxPxqKB9scy05Zl
Q5zJL8OSVjRMwpWf77cxFlfiK0wPphVxsiwLGHz9253cou6FwD1cQTK6X3TbspN72W+ewpt84q7s
b+jztIa6cb49f2x6k9ZUPUVaxbbgDeXrlGjFd6yHGE2tcifpDdehx0SDQ/nxe38yPu46TP0EftAc
DQFG4crk1I4q5Swxy1a684QOk64637tKVqe27JqWNlgZ5Wm6LRncjOsDkZZnqFfv35soCVr1U6mb
rVuEc/eyII5Wmq/KFXsSC9xEB7nbsmVeXUXfBJ5mrnbUsqy+t7Gn6LxJYnD1iud/jmfzavYCnV1j
mPVGcXfjHRlbqVqh134JN0gn7pbYW4+S00kECTCfucjLqVZtRbIPJseawTs6oFjiPu+l3bekNgwr
vGjeK6mWVe9kUqciLnaQEy+Oav6V0rvspgfeMe0GykP3+vlNjyhVR/Fb/RQwYktdcxInQ26/1qo5
JwmlewTzr1KxYQCPonrxBdy8p4LUVJuN5YHS8qH4T6iWyGCezuelMOIJdupJPuZz3vmDLJGJRFR3
ikzEwxW1Ze8sqHauqUv3dfQMsIMJkAX0XHxrbqMfYzH5d8BS/Px+KRUgv0C6hIc0vQwc7SAtIQuz
Ncd5cbOeR5ARazCc8l8PYlJMQ+B7HQx/lgl60GPIGkfbNvucwhJhnh/hq0J+2Phg0XqQr0Hvv60L
ECpUXgFMZ6fak4hUP9Nmyf082HChaAjhZL+SJj2pnR7m0PBoPOChxVpr25qDGa8I7A97DCV4wshH
GSjeYkmo5FiR136wI7z1OCCmJ7WlkJl9fY5oyBL6gxsG6VXm+QbAdlu7i2rk418V9QMBdWeBaux8
gTCMNrBkf3jIWUSrj+2M2qAkj4Eodqi05kq02+LQ0HHbYizLGuKpmWHZ5ptXKCoqe7Tl4Hx5jEqP
6ph28XpBnn927uFoGGCzRJ/cHMKfSlalYZsq8Gsv+5XIm/tR9y9zSDfRzWdyZhTgwD65vAO5F9yb
q6J2VjuCpZm8RImWEm9aXI8kbVP0ENTGbKrsXsKAfWXv5buv/DoY0NpRbssRH18gujIPPuvk4EKB
iMz/shtU2Qjj9Z7O9urPQ6r+bIQQiwyXLoCn9r2rh1r8xp0IM1qjEVhz9270MrMjEen8NFK6X4Wb
raWbKlTMi5Af0sdDA9V1S2ZaYKGj9f6SepV4pkfGZameZUhQH+MIGhMp0x8nZwMfIbT7o45tYXbj
D4Ij5pfabeqmDJ4sWgwlqaCdVOz5LbJZ1V21ya7/Q805Moy97XmtqJgWoAg6wG1adoqts9OOcy/Y
s02guldCLACwDI3p4t7qxXpyA0vTppGZdixILvBn/tiBAxt0QCu090+5Kn/QeJ9SaQcgNgYqQuT6
91052rKJOm22sBauLx6WvmUjLCDe6Jjd9D+TFDdW4EODOC7ga38s3w/OuQCZSrZPD+QrXgaE6RbD
FNqO9C6D/bVjuvjA92+UWZz4praOW6SgQqekslY/YLPD/NYljwOOV2wOUFI2u5JZ+4VXnnA7Q3Rh
bljLJO+EW48L0fPv3+Jv1treApyZEI7E+JhpMP8nJzqFeT4bPxN26yJp3I3btGwMyYON0jXTxgNk
x6MgDmbGTURMaU02HpxN7eGE2PcJ6jv76ETCL6MPWUz6BFRh2MgA3GctoCSxyyHPwHf4C+6Yx2mY
SsVRxLD0KLbpH/ptGy6nvmEe9jgtSpLIvY9Q6ZFvg2UhVC5wOnYJvTNMO3gDK67dcRYgoOmG4wbJ
NrK5IC9qe1fCsWiVoX5IRsNXPEizqB0t7cniN2m8xm1fJT59N1WuFx6i/MymV5kMA9IykdeqkU39
YDA6WehC2+vetE64eGfl0tIYfbvf6+ysDs1E6bjQ1hBqNpMx+Fgp+LALUuUMS4MbLlF0kNp5AxJu
y56bNenifLqobgHo0yFpirhHxte1C0gR0TX0DiN84RX1ZnwwSGhMD1rQv/ULnn7Spzu3dgG7WWvi
ea/C/YJW3yMlUjIk0G/650SGL0k2tQUU2xsdH7s4xnelBBya6Q5KWyUOi3AKCOO2GBxjk6LXZ2IV
u5rr9argw45RLnEzKbwNg6unxzRG830MXW2jUSNDNc7ptWC2OidtQ2IlxNAOxUrSY3qqQQHKIAxo
beoeefn0Qnuk5r76FA9bUWmE/lbhBPZUKs7OsJBVX+ccd58KzPl+d0kfqF+QdszQSDTK25rRfgDr
LuOlzRL3qXeegm+9y5S2Q/GEISQlfk+YJP16SPwhexbkMO1bFU2slohUVgl5syFagW0aXkMA80i6
zGnQnd+2/bfE7efFqnHWb3BPwR2d1vRpSJHAx43He4JNz48I+l8GBXZWRl2W0WwOaw8Doi1vKUQM
SY7TxkDL3drbk2BGXNgheK9BItUSJNplrHXVjUyDalty04zL7WNGNqMS1hinL39OWb4yEoqhdYPd
VOSEv5tTucUN8lL1pRJnb7s1PeH6HVRctaw5+KAeveAdwkxVBAfw5LFEi5MlNVjaiF21wdl7T+Dv
WoWX1m020AGoD90zcQUvaa/+jogZlar5GEjPow1N8b2hn2YdRDKg2VSwbmV4DTUGHoZhegb9WKkG
cMD4gYNtJ34+yGtK7B2XdBIP+f2D6bx95A2IQd1QCumkcLOlaKecLo5ZE1OvJGg3ZhYRvU1iT3FN
kTz8CxW9c8B2pM3PybSvwcD4azn35gX3YCzILYFAEo5aPmFmZzOgAkrPehxsVp7DEeApiX/87+hO
rs+zLPW3nY/CbTouN3dpOTzOshZh+mJRp7pm2Lpcx2L3xFonYhbHnjsdletETnbDnz/9HgE+zbpx
L+2fxGbDavOKuIrvDKjeXZqHxNo72b2h+UBxdh1XoJtXkLlb53HaXNxpH4a8HteZshW0D+eAljdL
ugem3DciCxIsqnBTzTWL/aZyx19f13Pp2srdzrI6FXAQ/e+aH4KwG+PYgpqaIWO5NIaFUF0/6eEt
ThOMIwfiifh2ZRdps/yQQXeDMZ8loO8wuPYq7f1Q30CxYu4SVYCNnhPB+yWFPgvPrz5xc4/DAjBF
/ZOh18JCTVLuWD+w0xwmSta/a0pLevhjq9TYnSTmXmqiM7i1vV0Qqzg/YEWsiT/SQE3OfUWZ6CuN
ucYByu24fcqtDzzhQOtd4Ign4eeAouo2zzl5Gi0DBzcP+ybQUuhNMiHfcMm6HgScqIITINRZXYjD
qYJASiihp1z0c7lTpojn1TLKQCcZKr8LRdhXYZjO9yqarGgKfbyy6MjJ41UJTAgxkeE2iB//9a78
7Jp6aubzJc6hPtu7MYerXaZ7P+GI34qAl9zF7C7vozDdwUKFYHK1QhJgltZQTK+4Nlaqy4wcdizc
3iDexPgiacYvlkoiS3Ve65axXaiT1p6cMzigdj4UbPa2XqZqb5CzrZQs1V7bYKWusIxnWddpNQ9X
Q8N8Epy1L7iqR6z8cEk2px12mC9HU8iA7SF9MPbcNC315CaSvuHWBzcYqcK8XVUjyE1Du75NQoaQ
vE6DqKABonO8ZUjJDFbGmn+XeTgZ1y96vOM+PIOa53UELmxfhdyryQTj6BmY3JN1fNoGOJ45nQOK
7NBM2LQ/BZmZDqnHVG3aYOU3N0uq9ks2qchxo0UiNlf9P9C7Vbc/D+xZRycd70AVXcJwt4pN4hZ6
meopRz9+UiPMSoe7Su5Jxv6mRRm8smoSclieECTzVBZDGobbfH8HFBHLfUKHAj9EVsIIoCZEM6Fj
EucpzEgOF4vRnbFM3lu43k2xswl5AbZI67GsL2ZKg0JqM+Ri0oDapnPOLfPsQZafuAg0KtW6Bd4+
MMy9zPWAfJoYTNGsu2Jg9kZOOb+pBMrmCkbF0ov2wyecniwEfqLsaY0lsFZvZGIqdoynTSJSHvDq
RNHGOFUSAeqrlCBkcx9MK/OWM4UG332zd2/ieioNoRM/fs+9Gd6+zzm4d2lHS/3JN/O5038NJod1
QJClywUfQYVi+BjcgGDb3xvAamF9wnjI4H8cHTIlmwUDFVeSjQq24r1VyWyNRyycmntQIB0z5WrQ
HWzLBUqLrRXSNW1zitZGIJGQllpygWm/Zc7uE3ZBN0EzGDXxwGFt8Dlca4DxTeEU7zzWax0P/lSO
UYomDjZ41rkfGzDnMzKhyOImxvwgzfy1np+YwL5WpvLCKIxd1Hoa+V25CQJ9KlH2NunO/jR15U1K
fQ96XmESqw/7AUUkrDVQBycgojWzHeJPsDt8Te303UNyR7Iq0QHCereKD6H2rvC53aUnNUhBxuo9
WrzvQT4IXw+m43ZKMqX0d9+mU/3ZrJ/ndfFOleBOCiXsGTkrn4O+ZNVKXdMdWjnZBmW6DzmVZS+i
UnmEQtme7zU3jGiYEY229jYPXwElJi9i1OPMDGXYSCEYLkY2IhHVtwrIWWvj+HWk7VBNSsMtIjjK
1cwkId0PZoDtsA8nM7SgJWAydI+uYQyr83JKtuxFq8JYxRYysK/8wpOrIMAdAjNJtqiVKEY+Zmit
qplcsOUgYsC2DF6Fz4ulv5yT4g6MH5d1knKqJOymkJ6lD9ehxSQXPnT4LMFXQHaIITvjtD49zTNd
hWb2F0cCU+K+docVjHl4HfPeAHaxNcCLmppiUyMVXQ+2OSbcxSKL5NUoueb8U2PS3c3yZCZNT8o1
lM9LA0CJglxz7nMrgEpGlN/mj8Dt8fPP9IKR/eVpPlO+JtcmiwYGe3HKhC2j8MKelCVhWWYDTtyN
KmRv4JPocaLnd81CbwZZi/ZCsXeUN86FNyLE6WHi4mMeiMA0fCSFkOhDjSC4dwZv89uBWZGCVmXW
uapEzyG/KOY9wlzidPuFHnGF83esKa4tWPtN6cJrTqWKCRNoMrvdL2bv4STEJEUwavDeCCopXd3Y
/PwMnDOfKZfBUQPi6quxJEsFx1+FWWBABkiFndHH3RzG5b0TrxBVpD9zzjgO8mYiqnykTm5YuU36
+IljFTVOaB9o6SpgShSSJ6MK5BPabPkWF84Z0mMBBQfi9HaJSJ1ARTo5mtKXaWriKSv54EujMRZt
4bMin4nqzWmz6kJaNniZHyGEQGJ3/v+x5uIM9H5BtBNaEDNdfJK09OS4iLEfv+HmaQ5leZEXDrf7
mFvj3YihQ2rJVEfHSbbJuAvZ8IATMjgtZmQ+GO8NN6B5scmkLP5uxwYkR8SFV/+FCQyjYqJkUMMz
rG1w1SmLqzocDSJVcHqsNnv6l9F2bdPl+VkCwK5kyLN8cdOkuoOzsoYjgF+9yznG38iCLd0MbV/S
7c+hwGl9V6ozKWQ0BciwuBPvbc+WJW8n5dYh96cyVLquR+p+ZafnOlbmvFETWSmUH6GsohdXjtlF
/+bc5144IvYUiX177WvF6eZYzX5IOF08hM4ZIa904LGkwXD8Nh6PaMbF1M4xIYSEvuX/ABHDugSs
qgklI22XAJo4spA4s0aCGSyWrGqcpXtJ1gdxe6fX5/KmYcRCR+olJu9ETaptikmPEpWuM4PPNMmQ
AlNJbv/hcUoEvEp84uVbjNrfUfjbjHru8kKgv4RjDZfdMJRuMmH2dpdmXtOg4n3i8G2z+zEcgjei
sOuCCaxrlw6Q1Ix4oAK890ga76cn84ecyvoogV5/TzQzps8PLmPNgMB+SdjYuHIdlu5Y474iS/zr
C8iW+r+uySgwotjrdQhbGsy0ZNelIXsI7g9bIiLmgDny6GY2Si3bIGQ2TxH4yFGjlckxvpqCKGGP
gOCXhYkIiDaeu+MMV9TG5VQxM51EhcEI0fYIWsS8vLvkylU+JLvj5g9PBj0UKt7IByNbVIGEm30Q
O4YtNDWdYhBbYwmRj1nDDmrGT0vxa0rKWRfj7u0gFsjTEXRLCRvv1t8ddb0ia15Q7YOptP7kTXzE
XM9vJ+i9ph6lonVbieWehXUEd0YHfmvKziQsGZ1fopmzHDETrSFrL8Tew3JIJWPG7vN/FoqCpdD7
uGmcIIYDuXWEPxqCOdz8fuXI2a+8VeCJYGH5s3RqZPFyZQoX58vda7llTVjtPeLQEssgDx0PpjHF
vpMREDuGTAUm9hjhy8L97kuMbRLy6IJ+m1oHOB6otntQYYZ2LvoFLDQdxbJgnJ4WXk2NKRT54koh
CQ9GTBs8PjdhbT0quJ4PRjjCOwGpQtYT+aO1PnjyQRAnRWIPWGqPNKKcsFPxbRa9paWpPPQqtXIe
u75Ssd70uKTurQ03rgN2CSYoA2m260oYRU1/wgZzCFzw+YQgsAue4hLfx602zjBfLUY3KUhiJBJO
k7buLs6nTljg0cFx5uLrKFHwEFqnIRXeFGVjrdHwQjCRpiYd7ZJsIVkRX0pwoF1dDKvkUGoOkmI6
tbEstv2oJGWtrj1Gff9Zn1C83Yq9MGf5vXJq2FOH6B3vtfXsGq5A+x9IpB1SREJrlfif1u/ZZBZ1
M3aALEJ+bXdeSGuAUZn530BUQ2pXAsoMcweXIM88Y2yoFg79Zl/dTs4kjaN/ctPm5dzOp8v3O+9G
/rPKih+l30mzFMZ4d5WOL7ipc1OhIpcU4a/RsyrhTZkOKP9aiJ8Q+rOuncEiPjyZduOAuznCyeKL
PkiAfSiLDqDCj95N0OIHb4zUxMOH/Hm4lfRmi751w/1bpfYW71j9aIj7viTt1+t5joscRgrt09G0
ShLIesOdbWSi1/ApLe1Vg4K7zQ4GWt5C0Gg+uFDhwK9ziPdImjyS/Ab2x2mgRpIAp0tsEDs93OuF
aCntx9nz87tzB1VteUCOZbKkApVePu/qvfBPZizevEPk5UAV5SEjIx1CJTpdeFBYUd24qgW9DFp3
+RQW4VfVmFqbfUKzwsc0jiK+d5wZtd0xddVQosC66XYF74hZMtntnCEL4uP1P0OZ1WG9y1vNK0KF
XFz+C/S17iwbs93u+6aLdru+Q5zctgqstzcpLWo8YKDlnWztqaER3IH5ahgo9MJ2BHIhmlXlk6Pp
tpOUP6BjFKZEwzo59kb6vvl/FWKsSKQdyTkMcFQbh3RY2BrpKYQZDrakxAQ3xncUTFX0j4mM7S/6
9USZ5gYGiErDi+BWEQZ4o6ZegJ+n5/BTEoKBWJ1XnLOkA2OKMJE8UpYwymdJnIY3WpVN5FneflVV
++86qbUKkr+kFHKMjA1YHRSvKHcR6vxqrvUq5jMI6is7fjkg7jUFyv30ya6XT88kIgFPE4dm7pw1
rUnWu0stoHtV/Zyl4kj8rVnTLrsiGF4iiB+fHN/yYrNseXoLutXqeFvb5/zWyiN1yf78YnvjZo4A
9Ba9MMyU77/9HpjKSm/CbsISvRgyUkR0gMRyLHYisoVFwW5RYLPs4Od3q7L/74PNQx1ScvO+Tzq1
MNmIfylXQW1UcnmPDx65T8+If/pnHoWxO2IIr2f49I8J+AnCxRL/VK+E68a2JWQXzCmsFJ0T5SyZ
qHLGZ68YQJ6HOOzr8Tw0CemdEM/kM+BGeeKOVZ97lhWKlhZh+zNbm2opzBq51qkD6V0mLZAe/IQk
PSKvFH4TzcNLpSzmZsgrFpOJ8qdVXvi/KjSse/B7FU77rW7ndIO2UM1CPRzW/FCuaCnhq1ClVp3Z
a8K8a3NjZOwJpZl5JoVYbGqRZ9zfZFW3zMJyLpR9ajA20jSv+Jr6WjRmG2J7ANaSm0MdLGz/Cz/W
WlI9nqy9btxfEIe6fwez81WPanHOCfsM6n+Jqzj8vICqxO0aou0WM3X523zDVCs/S6/pWgnfnD/t
T0KCpX3HKsxC9dm4f78oC7FpINsBsSQ+H/ualF4zGv+pzDccIU/LFluW/AD5G9ZB5tUclZ8Eo0Vu
1JcorUCZ++Mz6z5mSsBtF3FD8eit906tKtDwnl2PlGl9/3rFduR/yv5yFOtnGU6BzopuoZ3Us5Vj
9nsBbOBcuSrrNAP3fcGimrCHY6NuJblK8K1a9hJSu8q9MpUvxkjRnZ4uXPDNw2WD0F3RuEn0bxts
ApurZkaJoQy9MT+bFg+FXbKeKS+zYVRra+mEKJJ2krUlyEErK4nnD9wStGbzIthtrXk468pI5cEG
kZVAtsm2msxVGGdmcJp7OcscIgBL3mOEmEMp/hJkZgUunyRafprwt/Rc2enihS4W2EEideK8i8Jt
663R5cZQMArWmuMTBvzsEzRFxy17MS1Fw+Z5InSbZq/QaxBoaXYD26KBI1dEQ3ZO38fulNZzXvUP
BZfpTnkJPjVFFV5OAzxiBcYwGy/m+QKARP5AlENwtocsB4jbergqFJEzWsAEYgJJSq4R/wdWlLH8
C2xgZ+95VVO4CySNiac0dxBzuhObvxutcbqB1MPKhQ08R0M6QhOHTFsg9EhjANvSyePAPKgs5sau
yVfiM3dSojsrX0C0fnCaNX1MIJtqqnLMZy4jMvekT6HhPEzPbxAW7pR/LVVd9Ja4Ux/pCqOnRD26
yOa7uTGxdZ7a70m9Trktu9GXglauqiyR5XAakK9KPBB9HgB+8S+rPyFVjchyg1omx02+C1n/7dqd
wyQj92aAZszi3voS+IJXE7GHbOP0BcmBOEBQdbJSXzx7SyGXzMYkJE/xRL6mFE35Q40PL/jGkq70
+CHzVGEQ6jF6suzG63oFAJndCrMC+c+WDDIP1Sa5e0ZutN5YzgWU8J7wqgpS/QWulEwKdniuO41w
0B1n8qjffbCWgxlvoJzg/tOlPTfyJ3q505amq/Jnu9uWkSuHtpO0QWw0A6O63VyyLtwB2LDwdjf+
meLcrSHm19UI0ADY9ZAnHPzGdS0Q8JstyktPMlhQl+oVAhO4ZMZdXGxqc1KcvwOkZL60tQSu/ghY
eYQlDS9uUR1SHLhhH/pJqhRguY8veFkut1I3qklCjusemSlCmNnatKTw32RMZ1JrCuekYzi5cHCd
P3/513GmhIAm809NkHTsJZIaZbyAGZFcY1DoZrsjkLXcUCh3EsrOkv0oNKNPOiSygx1mOEKI2Xrv
AEK2ybygPx0liKjv/Lq/92HFiBUIzSrnaolfEpRobK/7Mn0mMgDtOSx8fowQAacV/j5WQGk1JMlT
Crh19MkrbOSeW4vB8s6McQzOWtZfbH1/nm4WhHenzuRtOah9kZbsIAzxLQ2XttpsXsKSIJ7G56Ns
MokK44+BdbtUIvbpQQ/QTG9SC6ee51vp/qAOm3qfpIjXP3jnFHY/noxXzoonXH8Ap3/TmDBiPs7k
NjTGV+uNHujoHR/jlKz2tIN4JTYvs370TyMTFDsSA6rva9iNkWmsaFMSXHQVjafh4A6/nQw5xNWo
Bj/xYaK+7Z3FoyOwbsHaba/GgJRT5/O43155Gpr6WlC+1jBjhVt/z5uU1EAmd9KL1EIM0yy12gxg
6fHYD5vzCs0lmtPHULeNx2Nkp8ahjHCSqwDZVKg1y+qrifFx9dIpbW4pHwV4jo+M7CRPK2dHrwAQ
doI+y82ScqJnCxgtswtJViC8f6F35XC7LbcowPV+1UIj9m0taBOJrmzebyL6tU4c65AG/pOnZWGO
SMNCN0OK2s1yhbEG19QIYTr+uu0zTWVYG19ak7UlVddU01K6Y0RpjeGLVP81g5H03v4voyeXG6Li
vGKsIQnWfBawYfjTHeAn5u8bLPTWah8dL7rOr72DLfhwzwLDTbvowV2R6YlV2PnSRwa8VTjwGrR2
qGao4yj84Nt62OBJdEICegH07IfwbaivJO6IK0wQjILlbOyrkg8WoKZAVKLnl9JC8iaiWJyulG+L
1LOii7yrY4AYyKQxRx5fZN5mOA5CK2BfNv6KVXjZ+/TIZm2WWokS2cSrjj1/1UYVt8keYffGwT24
lIYp0ys4CZ5UcdJSyil1X38h9/LhaS9AIqLo6HwOsXeZmwzVwC8k4crcaokKqtfrtUrAJM1dmnor
O9uj1S6O9HKcmp8kiFkqdDmvER5TMuSlrB9ZKlLA4YrMVgxNdd2wjI4PaMUuhZ+gMZMbcQWkS/TU
1LGZTBkcX8zhJ1oMtDU6eD/ZPHWX9RFm35MJW6oJIroTTeraWzKLmWxmImUeZUGInQJlVlrrfWAA
TcYIorbEiVrHZtknqyoCMrPU21H1KeUt1BXh6ppa4eK3S2+CpYKHg5wz+1cqy2++zZ8ioka9HU81
jT27uo7g9Czo2ikRfUvJQ8r3xG0naL3VhblK6Jt0ZtJ0hZ6jtYtBQMgKYuxG40+hR2PiNoLwv+sR
EHk2zYpXaogXDMDdxxFdyWAjQDvTSf58rnbzypepsD/FYysvOzGFgqc30sYWnfFohJVTURp7TMaO
Wxw7y1azRvRJu2GGUHUqPHWrN7LvXQKt9r6LanKBHsKymCKpBzWRE2ws/DSRp3b/0O0eENMoH8gA
4wkAOd3hbIbqPrt6C3cBedZ/aw0sEmMgxobZvoXiEI6V4jp2Uxvj/SougT7UnQq4yPEvn1UBEn5n
Qksl1RGf7JjfjINmthggO7bixXVf5dHcSUpl3XmLBUx8EPX3TqYwDNRKe7XgWZexa3TVLrRnNBMI
JTbFmYjLTtSbhTg4iI9KxOXSO4N67ddbn9oTV8PTEQJpflOQF54vl/vT266FeOw68oxHtTjVuQTF
NfMdkljRYtxl0PCP9Vb7K3DAB9/65HGnS59HSLKzgZK4aRGHhRvoJU+/a+clFA+bYan/9ZbqBF6/
KtLVfmg5+K8iM7NaiiGtBVfdplSoUE9k4HF3yh/+rYaWEKPdrNyEgYVEZi8mh7ODG7Aip9ociaDM
HRjARc73IJpTKvDRX00df+8t40DMjQqB9sDUpO5MNTXraJSp2yr154yRvHLOqUOWgO7rLwgmELic
aY8dF3oL0ohCRYFg5/EQDwlameRfqYSeeFqImHlayrzIelQ78RmxpWOYgvbkglIcGL38j7wD+IbX
AWiUFHu20uJqVV+ycFYArYOKQZH6Z3rlEVYqwN4jxOMfJJO1/cScMYhM5EWlOYgtxUBCuJZN9kKo
md8Y+srlVvQku4KjGsHnzmD4i42y0xWJTEWm17p2yYr7wKmIUFkKTZ/HVRrxq6tbV6lHc6NxL07/
6MQADDSd/6JSsSCF6Fdh1Ej3YL87KTbq7TzALxVgcRHx9TyT3yK+MyOPg+cUgQT7tABoQqvmfFE+
McnJHEG2PWyM8hpxrhitZAXq3HLNZap8OmcYLzNIJ/oQpQj7egKDz83O0SZERWrwcTkLm1Kes5JK
DjQoHyH5H57gCM5JON+eBOF85spsxiiEilWAP5AKTf5OpbvTmwCrZzmS+gMpDtl8tvgjkadAKWIW
gz27ZN045goo2ZaFkVB6XZIFpvUinw5OF+m6MdcsBbIn3cB8CWgwGNKiogsFpj50AGI+ez42W6XJ
o6MKOER4KqL1DeVVgwv7wKYlpabdzHv9kw9Wwd103mho2eid8oJ38TCfUBcE/RRp2ossEzSpJUcT
qYs+n3zZOorCWHaLV+FAJZM3ivPQ5jKz8WIYi+DBX3PYYUkGJh2uruSb+UtHGukdjuIt5lniDOyL
JIkj/PVomSgDmef7kZuRasIzX1pdW0WCr+hOh6rjkP5Hlflf4vuiIuyizub7TPlH4PF+cBhcIF/A
OjmBEubFQKT67DznbxWUjgZvHSkgVPmz3pptvSnE/NZIQnyldjc/So80HASvhoFl3w+3bzGpX9vp
pPPvybvfRBJSqo0XQSfH62HhAjNMn4NVjFyrzzuFkuQlgb+Wm+HUw1Llhj5PBrLKLcUqfh+LqpF5
CB5xYCNSjW4lo75+ex9tci4qtTWcA6fU15P0zWmwMN8Uzcr0W1qI7KaVPh8saCBCCaC92yX2fXVf
reI1yONrI7M7i1fAqYyxzD+Av5RtMoKNM8cwsvO6NHno6IqmkzLRbDmK9llNuUyIzz1czPpUhkfV
Sq3CJykDFuairJAiXxJqQ4vR77Zvu/lhN6HuEvEUlBS2UIr23tCKL5falHPsIDglmGxXu0XqCBAK
U77TjqNeGf87Wbc6hfF+OZocMgFbJLj5iwjMVfGCTrOlkeZW7zKxQsZyXK/gv2FM78hgkb1LUfCs
5ytdS1JXgSOiGX++sZtJc6XpvIwOZQMBqfl2JCWyuCM2ZJDNzqCB/Epgts1XmXMJ4ER+zgTR0rnQ
YNEdpCeA92QIIftYv8pYEquCg4Xbu3pemooGirjitnh4VltMXQ1DU+2RHNoCsDv74jCBx31MM1E4
Vi0k2UTom2LXb8K+oC1RiUlmEs+ilOgR1mJORojd6BIkLaTEfrL+h+d1GC+jMxQ6vlyfx1ps08iK
VSkck+ghFgkHPXSqk0qfiPscFOphBEs8RmZIb5VUyJZIQjv6d+kftAJ2qgoTEQ5+6C761DzW0Lv4
6bnVBvEQa0xeCPaGUOKRR9mcvW5U+qC4pTb+jCeaZeqj6ByndSLCz7/UqM/LkIC/jIegi96ezQ4R
7m0u5zNPXb0YnmTscUBKhQqC70KEnWXgL6wvIEZPntmEckdJR0npZmnIJWrnvPwcvvbWE5bISlPs
915pLuQOuvrBOgRY9wKhAQXX3KMctGNs7P/WuqiEXSvHttJ/KTTkL82h0dGKxPkng/4VpJzg8U07
q12UlVeH5Ojd2p70C9ge2h+6lgYMEuLzvHV4lca01U69zSuZTTdkSVdkoo07nCxZgqqF4TvHps3z
AoMeCUhLMq0SlE/NTCGmTsMfx9af0UI+6AlfclnfK2oapMBDCIBDs+sLtBRZXU1hRmBoE24bnH6J
PKMhF+zADlnIo7j+T7vXl7n2Fsf9qJQuj2UfhwwPQPcp1LscUbyr38P3zj7t4/jSrrxE3rPgywUZ
K9dhyeh/jKwGQNx+UpF/P6AK4ziCVVkotYI4SPd+NNK989DyPp8VBmJUQEsSLawLe5Q2MJLJJLVU
2b3hd5BBCCqgUNr1kPfy3+zCaKJwfJ1KRu/xFyeA18OcnectifrvUc1uXMw1OTDZ50MS8A0Jtnsz
JXSIyi+kpnaaXMGxoWBHcuQx9akZo6Kxg+fpdjkwNPe72PgS85c19JQPZmlkfo4ENyqCIzQxK2Sy
IvsrjdbPwgM082gFDIoINavfyRVSZcK5NAEWRpVt4FTZG9FKUnfyGY1m4Xi15s7kHPjVc8WWLgzC
eFn476zXmdGr+g0LUtr4qLJZMwIRFx6rhLExQfEe0P8IMQr0MXW0Bwu1RJr+LpFFvi0as+tdxdjk
Zx4OTSTwUirc8peJu7RnNbLkMg0s4JEVVipVmDJgm1oBzXqdQWKexLAhxr0lobrIGg3bvYV4lC3n
mofRFvQgi2TTkW0mPJIJwCT0g7C4Q3bwRyWPD5AqHwGUzwFHfTJmI1D+klHF7dFpf0ant4b0imea
29vs1XlRE5/Dyxv8IIIH9Tg4uojsm1dDVlo2k5N4kTN3ozmAfmsCJ76GK2GRqe6IAQQcVoa2JNNL
fSar/9CEe+IJ7T9BG9P7GREtlPc9aSbIatWMbCjawTvI/iFGqDffdwLJ+e/1gJFOkGFq5/AGx4Mb
DUYQv4beMaM5v1LbPLYEhcXtlniYy0MAkpxMkVi3kOQKKYbHiwIJ7OJc03r9IDl5C/h0wdxIBdhU
fd/9PtZuWlOMpRwV7jnnQokXBrC2dhzWTr5y2hE8Ns8kDaZRLKGMGUZFPKZEHG5lz3vV3Jrjlw/t
Zkqg+RPTqfus8JIORYgbvaX77GLxtwsavXo7BcblJUdgM83cqdT/hijuvFf+Kg0FHlz/tVeA2+nx
IXeBRhFKlBfde/0FteMaYJIedVVx+JjxguQcyqrnLJyahSiKigaPdJ+HiKj3ExMbuaDuuuq3ShmI
goMSJq1WKPBZM3H4OCe9i1yWDqTFVL7oX6LQa0wHkLP/WJx/4k5AsxB80vki1+Tu1jJBuOENRWOW
aLbv2EvAcOMgVQIKj1iZg4lN1hlVxO/yF0cHnJkw2viEuDOQtdT4S5q+7mzjG5AWcEnASLpDcmQo
FQNcr4Uq9g22OjZpDR6Naslr1Ela5cxbRk+jpWx5M0k2PR0WKNscZ9JcTjMc32mh/27x9cuoWlXT
uuK6XUle4dFIfg44MAJxlLXPx1fofm/ob3IB5CDC3xJN+E4smyJC2uwmCeRpV1NpngzAZzX8FVcO
jQXTLpMmM3hThik6tAHYBMavCXfvBTbVruZEY0TJtelVhQe1XdwLdmDxa75/JE0uh8rBVOYRsSmq
MUaQJbUP0VlPG0fER1rjFABlGnDIHg6ahyza99gXGe45mjiX7b3v3pJKxRGYk0m94uCVDTDcVJJv
PC18cURI5zKR+7aKdib2Y8KlwyYT8S//b/+VSzBBaSWdhPvZNGiFbItnUY2RDJ+wRjbSWqUWYZTJ
7uWN6A3VUKoLJ9C/Mba/I5hEBPXE+I5N6nd9ErnBTnlqF+ztOwWVQZXKIL6rJ/OsZh9EhMwWdbji
fBkQchJ7xT40fHPrlfGCJFzgqVUt5tjON6arkX77NFvlFiK14CiKNFV9yhtg1g/vpwsWKTzJKQHZ
oVTCPNZr9RE806jcUtchWJ4QOr7NKfM5H2lv71iduv7FFy2v4VSqaHzUxMUIHKfiLIuNVqOY8LWX
mgvLft40TWU96lXOmHVizjA05heWWaK8XanlBjl1g+JlJB02lvtyFBhUTmn1gwE8SwgezxnuXiE9
814wMMRdraJOS8Gt2RFtDLliM/uZde6nWyFcYIRsEtE3796iUn1VfqI1VHizzuxL+6PZ//EVVaTW
eeuH4Epi8LzFWqSpGkm8CgIeelNRrYH+SZ4fEQNRAEG5MZ26f478pO8FnkwMGNGvo5wcqvSKhN/C
2U7U1QQ/AWwsWqqVypub7l+75rtr6EUAnZGdNRgnzvJh6asm6XaRisC8iDVTdbIGkrPGhUpY6jkX
liONlwFhx4nqHvv9os98Hf6mtCNsbaEr1EYFKgWzwSD1p+kwC7J7YU4LvsL2cgoOeN92NZGa+3Rv
ODXHxoJcp3m8CIjmjtJLBdIZl/nFqKd4epf1vvBO4jvW4KpUyAQb+Shx0bmXqtIMFbJY8FvePTBI
Ay6y7Ic3ZLFf9HEztXYWTT+HIZ7mA7MQ5RcLPLiQJUA9XriJ64U/JS6crN5+UJxvGVWsJmy+jR9G
HlGh9g00XRKlrag1imd1LVreDwIeWtDMOLX2ZiGH5zxhYkEYW6fwOlUgkA5dZaoZ5lYkgcekKpJV
iUFR++8fbMQVzgbJOpUc3ZdAfHvSHbgkA5ViYsjpQGhEMGsR1zB7v1dWKkBqm0FYSr2IWqps4Jkg
Nm0P7Ag0uSeijDa/uCr7znRDkquibzTU18+Zhp6hvockmBSDWC5lPHUuH87zRnHSKYowewoqnw7w
MbIVoeewX/TcnB56dTqYSZ5kxo8oH0FkXmE3P0Out2yXNwTNC2T+4rAwv0P+Cy/bT0OP5NJhEkB1
9zBwrdIuLeEDVY8Gu8we9Tg97gzqTWpvkeXpnHKCdjKrsOwvOUh2g/SeLNUEI5drCNThkHBx+IDz
KsGT3LMJ7WNU4wo5iBCgCeKsxj9xUQggE6CXKvp5QP0boYVXIkwJBXyD9cDWuE/NmLd62lMBak71
ZbHL42UvM926yl4OYENHD2XfUUuI2p4bkI8QZkdZuHnV9oesjME0QQpAoTYBdulF0HmDEI8tnebF
TsdUd6LGKML3cOOQhI03EJegiPeZ1sx/4LBBYM5Pm4MvO0hqBxMSwwV7gAlqGa0REVdrIBfdvCGK
1wCPUWjdGnoUVw/RGf0ahgTSihTs1vQf9+tWt+u0dhEQ0fJqVivoioWu28EbJQkzIqiUP/rUBQg7
6s/bfeLYckIUSQnZOu3c4Cro71KtxBlLA25ir2Zc+QtfIzTHSB1QdqS/xZSooiTnvJd7sW2DUXJY
QNPkY5h/sFf1/m4NhnHWzj6e50zlDN7zw2165ClLmYxlzPlrzZjAOUWxwFxpth37f6rIL89ZsRN4
/Fkftyz/TEC3EUc64i5brDSchyUIi9j/YC65u6EjwtcKdvEGH8WnyaNPw2UqLkzeD2GfEZsr2Lar
hNH08DB747gqNCvJBzmXFIGQ4lkOIo9G4HElHz1LR2IpNdHuUbDZUAZrkaWkhT87ghosUdx6R+2Y
JhkwM2uAjap27LOwtmHLv3M3Jg0gVJw7aUhQA4NRzJ9fXvCkUPg2wPUnOE0jrD0oX2pi8pAizxiZ
3N2j+mpxmiVonPI/LlwW/6FF8hQ9FmdNJediRuorpIfUWlJttPlNq286cEeW7DzGK9qggtBh1iBv
x/Mhocp7JI5Pwd4UZsuomUaD3/OccML6IlIemtosdtpJIfppkFXhi/94xSQtDzt1TAgHSJAbeiCm
2VokZ7TIcPggzgeQmcQ2scAjmum5VLRtKImPwNd+YO0uxgTcURw/VufF9BD8GVVWVUCrad2pwVY0
xWoh9Rqeyfliv8yjWqDjGUIdYZoz8qyjZQuoK/BZGpJdsUzoiWyJrs+tdXGSxWpAFCs1WnhLJyBW
DWMXZ16nGj7eKG5pN3efJMfN8C1QiYnJwKgnmKPHiHOykafo6lazkpf9dfwZaDDNkgG9AyxNbCjf
lZvxtJPv1z8bak1c0C1PYKYmiNpFp5XId9ySarPf9Dd8SWpFkzi8cVwKDvDh1OyC+iE10X0LHHpF
SWrgHSXG5L8ZRXEkjZI+hDzalBvJG1xTYqwB9lZAOciGM5lFwNsJNzmCn72g/WYNRnIngIcNJy73
/+hyl1LSgTNB4prDR7PSI9CTlxUPYj+gp9jhKyWYoacSuNB4sIvYLILIGG/4QwonZkeM126wnPS6
lIEf9OyEKjBuzd/x4v0F1f3H2XwEFLRWTRG84MXuwgwNySrsSEFhZpspNxQTUZkZR79cOL4nwdOW
sBssYBmcCACAnGGn9ckIHJV/vHjz4330Kuex1MP/EEy/6nQUY09FmuBJhYIyWVlMelvPYsTID5lj
WJ4uFTyTeSg+QigBqcAVtmbJivcX8kPxwMIyUCTdhZytzIr0VrT0BQnjnyny8MuMhO1Q247SBAL/
npnFddLiYZ07wNYE1b700lCaCsrN2lkrTEsMZsvN6hh8ZuCQGjy0d+Vg5F5r9Gmr90ZEgWb3bmOd
NSZSmb3quP7/NFWD4BPYjZBRJSiy+PcKVpYbEkd3EI51GzCtschPMPJ6veGbPbW0ne91h60pa06m
Jg3x32wJo4zvoUdefOLwFk4Fd2RLALOlj2KFEIiNphaVqZ8mfyFzvcJ8StsZGQLtbNPqvUbE1lw9
1YWYsZnU90s9KeO+AxUNgYkcGy7otmsV1h+zoff2Sghl8GX6vFFR4MMeMCrykjDTvazPZYIc919N
XKCx2t5tKGFkfc53RN0LxKKABUadC+JoQoYM1uZyRKcMp8uptiQeshmoJjri9+e3KpAM6d0s2YB5
4UvBTKqnMIz6vqce6TZwvDJr1nlzPthuzwDT2xuYOZaKPa4V1jvwnA6zOcvEwvDz4kT5ZICPxEg8
j4h+BNG+Wuzz/T+cCaC1yZwczHnGFBAeEoRoAH2AafTfJo2QezZZ3tUPzREs+5nJPyf+Lwf/mDf3
fzyaNqKFwIP095f5GuXbsZgEAkktAzz4qiaX2kea6B3evbmq9etPVbZnlqNPdp4mUXgWjD1q+ZWB
VphmGfz82u7kPg0Dgi777iSE/fbFHhbHhkBjPg8tQ5D4Rw+MdmMzQVjU+1Li6XswuuI8GtzLQoYk
4HLsdY+GtxaB1LTOchphHim9Thqe5wvDjyBF9upA/+r2M+NpZ+hNonhhPLz0Ti7ez2xuvsQqI5ub
e8GEnxE5ZPzYshbh3t+UyGSBu9cSKy8mCIJ8bw3bKRDiVxEqRXyWz+p2jCiBZwU6lbl03INK1du+
I2FZAnS7QTW5ltTo4cRiHwogO8ij482ZEfL642lF3UYF/lVJJK/hgi1NM7uKcKQgxwIOxVpH4PuJ
ShN8Z7ydeSD4AO2vgnep/KmyiOXcf4MmDVwfwCUqJaGJVIfRcyV0vqc+tanOA7wk1l6c02XZGD/3
r9FjJ+kxDZJLkvv5YF9nQ7hVAFfvOHv5r0pVa8X5dG8ocYe0LoUCVBI8muc++EwGcqkk7QO6J5xi
hQAd0EWPxzKvwVVkhSk/fquZ2tJaLaq4vU/9cvsos8PoTIUsWGkCmMZjuoDQcosfioUia1eRiy6A
2TCsSIy0LtYPXVmb+MWCCDaiwzAMwOeS6KiabUIFXjHJgdLVB3YSudFGM3UtGchduiV7BCejsHZu
tEe5MbNyeVusqMLis6/oRmeYmkUXsyH/Qi0z7n+ps610d4CzUVdvrWTLToioVfqZi2PUwmA9JMvB
8tJAakXW4xyLbeDGkKcBLDeWTEy0WAANTMKenK6RH1dPfeeyz5FsHBCQx9nn32og588bUpYtVcrM
6vKLhxp3S1pflG8UvZrGudb6HD1RBfiv2TyVpbG5+B3YlxSWdp17yablAVbE8df3Bxl7kVCQ06cp
zUtL1Iwoma0kQJmSq19x1nWOWWCmeL+aYB7Q2R57UmRHfgZirdM/ntUPc/c687qCcLNphWZ3HdKf
fJrhw2KuiunfUg+zjrRZ0Zwhnx/oDIMmQNtEobyRWgSQKFe3aa6HHZ4YTGLgJRgAlbQvQTU+Ubac
itsYM/K0VBz11mG412Wi/h0Siza0FHUrr+rg1DWIYxM9Zu9hV+zEz6rztV2A4wPRvyTumKr7hYst
gavVAFaiJCnQ2VR+leC5muDbkK3S3dmY7fGTwUKO4QNc5mGe2AhBxjo+GGPATHZGyfDUhJC3gbEz
2nlO3VZJAJM6gENNtCnUSQ9lxl0zNjfPutICWuLU7O9swEzZT5Scn1AXq6y5DyAddurNRAfGxMr5
/Vbho06gL+gM1QU+Xv1mmLO7xA4GMP5VtDtlqVAonlAF50u2s69uqQ/khhggSGRNxBeClEuni4bj
uwfGLh0DpXXW+VZbFkaiLEME07IMitrSb0yMH6hC7N1oAF+NfJvK2YifH7y1RNgxQNIdC0uqtHcY
svlBsNT1/J8S7yTcoTa4i7PVv3tEKW2FocfIN0YR5BfRAXT9fd3y/MQUobbBl1PBhTzRZlQnMIZM
/XuUhgmSoIqVo4ZH5LVQhB6G+w9rZvtS9Kemtnt9CQz35vvJ1adhPeRxW6NW0Bqw25t5l1I9mdR0
cWKKSqcb1odxIAg8iVbSaZyHOXecc/7eK0LZ36gDAOgBXzSKD8EkoBjvqF9jDW5/dxWOmoK1H9Ye
7lhPZef+RJ0BT3q23dBdVe8euCoTiNpY/+yg7uSEFCVPpfguCS5/6VxUm2N4qn03aUrDjAJ/GDqh
JjMP1FhtEtK1fX/H8d1Oye1WqY5OYMECwj342dM37SKlaJjVF8GcVgjMLPcs55Pqpcs11nxzYjYP
wT3ntSkewVgyu7G+mmltGwwKAjgk2mhMDkmc0U1XspuqfFBmLr6PjGQ+WKtPFeo0oWHvbQi39q8e
8IDq1SYbQFvACshJTcuSosWMz/iJkhuu9knt85fo5TTs5LHp/FwDPCn6h0msIOwOSlQwgqSHi4pT
RrSvJSbhk2KwGGkSnnVjIToqqSiScAI77Tvj0MW5zhOfv0up/WQyoStQ1kZYHQ+lhO07Jn00Q+r0
TqUxEfR4v3B07KHCud3JyYVTUnczFo6w+qysxYRZMWCAyYsDdsEosmXNcuZ/TO6gdg8j6rj2tI9W
1e08hPIoRvUkpY6ABy+T9YU4Vli9/2X8XvANNLSRe9XZ4d3ITVR1QbJQqfUrYHoCKRZzJvoLJZhF
qVvsVAnql0uZaMHsFgJMYGZ96OTCTe1ka7yUuOY6K2A/BbSbie1m6gm0IL7xXr0NSHFOQEqGdqIZ
OsLfvrQEkuZvsOVQViWXZ1bFdCrmAHTqGxiTgjMS+Y3jzlufrC8bCbTh7GAlu7+Z47V3WCrnG66j
IMDvUhPe2h1azxYkwmQTZofYsU4fTeD6nHfHBmhHZqDXoVauAOZjlxie6MB4kBK1HAg58qoHKG6O
1s8tm7D5Yb3rxViuhQFzDNBExPk8TI7wmVM/cRKIGtY1bELAYypViqN1gmFywZ33ayqvNs2qFut1
0rkPwaspP7IMS5HHZv+WqLn5QuILOarY+HTeOq+gHb+lNPvd4zKcjbyc99N6j3odS4/xESkV1dqS
zoDiKfUQOQf+dhjgXDwCAO2QGix2HWItVb+1qzLVxzXeWH+Py99NiloDwSAAa9Ma+qVbfFGwglSQ
Tz5Phygt1JADU5o3xKwFVVHVS2EKFkc0llN41X7ON5Yv59c0O3pTVHzH2dK7l+b28QmvASq0K7RB
1mrUJc9T7cQxfmYrPyzk8x7pQVKBZ4JDii2PLTEy3U4dqHFaMNT33k9HT0qe6bRPMxaJs5rIFVCB
URMhPNBMRZdbqmcY6VAHB4d296W2a7k3lgDwFDeEqnKSWrmoM7ZWfCOa4JQ/TeO8kyqzt4DNWk4+
nJoZ8ecD2fsMRC9glc7NiyeZwK9jC+gtBH4C4z+IKXhuou6hqBpLPNeoBluJleJkALNYOVjPwvQk
XJDCqY9J87QMByrVJbstFO/oaoRu1SLYAFrjzH04Um1nekFzPxLdWP/e7CVN/zOJKCMa/VifOJzt
DMnO+VELXfGO3m9QL6Bq6wcEpdJeOdnb6xpqRI7PpqUxA1s9AHtgvS8+YUCmOpgQ8IdTDWbD3svM
39NqCRpVgSbiGbob/sKAS3/hjLIDYKf3+6Ga4uf0kgpcBPCGA87wOsSjQYlTKW1DyIQIkmBXW1RO
uoezuCpzQgRdUA42R6TCznPwBWe/rRCmLryDFyJjZPAlanHL5+1Rt12kfFq+Wfc3bDpusjvKsBMz
Gt/dOt8WNCCIMHipgDpB0izgfkGwQ5DExYK8psNCC1YBqfkhZiFMVzhQtIuvuJO6TiGGw3yPuSDK
TzvHXsL7CMjSlIZixTw8bfhyqMQDHS7FIC7o4u6zKDpu9n2ZyUzz0JnQ96dN/7k+LAGDtIYVFOa7
l1aFgwaX7NjHWHfZTJ9UZ38PvsBg/J1zwHrjBaPvJ2gD/st3eYOSaY+p6MLizWg1FxhdFRIEQu1j
8efKV5TGnsLt2s/3aPg+hoxPR4DosgXtLlc8JPrm9g9FiDupcz4S2yZJ4xNO+qJ2EbvRu9ABlLm0
GAQGOV/498zsRTCZ0sjbiwrkjidwTapwmIUf0hLcjqbShGW/roxfqqI5DPHY9kfBekYqjh8BSMPd
7go9hdRLAMRCwT08QASLyvgPKiDaG+QWbAQGJ1Xd381fTPL6zgmOtFRResv3AUcIfIOD5p4nM16W
kIIO6Ie2DUayzlwv1uT3XO3CHwZcc9xPKVupx6loNYNNXn3LRtDJX2IHQ0RfHdEQqwXXCOO9bqQ/
suFp79EKN9vF79UaGkO8FSg/qP9kO8febcG+RSKcjgX9kPx+Uur8PSBur0Gjt19UnhL+xC1/FJe7
t5ZnvXCi1H3X8npheI8a++go6zdRCerx0pmlZeLwHSgYGYNYCDc/K4quZJ5q9a5lgA6s3i0q+ois
xlOTUhLmKwa4e+lwFispS/kAdjytHZTwDUgK7Cx+WCVbRQP9xUAX8ceVgVvdW68DBX29U+BC13E6
IFM29qeKuI3FOIhmOrbbdVHPtF5OeK3vnetTnqPib3pDDaA2285k7XZThMsHY0AlBUAX2FYH5XHj
oD+dHET0n7s9KXWxCNNJSYAZPXC6TlE1mDwAsiC1bS2GeHTL4GfWyNNlXGhBLf1+fwQrMJUWbJAq
k5r7lYrEtlQdVsEggxfIP/iFnrynXMkRZxr7UXJquWQwx0rzG6MiUKYlyLu3HfZkmmlO/Td84SWA
zRTaQ9LzgvVEaFjpS3dnIyz3mDmxDLKg13AbdtTlMK1jjZGdL2aD3QvKuJ8sIaM9aw1Ye832ej0A
Bt7YUdQ1A5n7ERH7weAs+gK4lH0KyxvPp63m5ET3q1Xmkj6pOEYXec5qrG+c1im7YlWtxPnQleLx
3J76lqrFnGw6qNfcn0OG3lJV22dFHnLEJOkyRwZWX05nyHmMVYqW/nQyvZEFxqjDiC947ODUat4V
bTSEJIqPpmGiGBtHF9JEaVGAN3aYvMEnGIoHmA/7lR+BdZiRv6gr3fMV4fbblOWNzUusX6ZlZ2yc
ydFPEY5YZUtAhHkZp6PL75ZLzVlA2zfzysJb6OZ8xOLxzHM5rrWPWuceKBIsky1HxOE/QRp4xZne
hvnMJ0b68o5QydJGfEQqyEfo9vkCcJaVqvyKRRvjCJJZJOOj1IXOFL3io30FRDklPMiQ0QUg1FnN
ANS4IJqTzpAbh8tThtQMBmZ/7ZVs7uAwlPVcuwNlXb0eBrV0kGdh5zu+VTxTWUGwotKEk1wOKNhb
CzX0dDEYb0AbjjXn682aUcU3VW2X9x95/kHoDZ4h++FENSoiqQT2BBwuw+ap/17ONs39MAP+d/eA
XTQdOmOCMSC+ik7DUW/Zfd1vlUmc1FcWGakf5/ke1HsD6qD1MSCa06WGXnOHYh/iwwc41OBrzyeZ
daOK9eBQKcmL970TSFVx5q4gU02001X7WSA0PTD/vK7ZtxqbV2mFeG9+9fvUEvcvyQMOxfo3Uc37
4LDTCsuHevZM6eeSP76c3NUp8a2V4cNEcrI3a47D0eXt0IXIncDDFDq0xYUGOtXYPORsVkOuXpM3
jdZtnqK1z15C3w3Lt2V6Enhb+g18RUxxqeLb10meqKj6QRfDFyD36J6dTY5DSkx7PZ3Pv8f3DNmY
Q2svKATkBENAEgJM/m6xeHD7kSDFF17pVziklecf+B9B36OsfElR/bpBsK9qkrhKa20ZSHeCGxxN
scX54nc0jpO/zU7TfMto2hjsUUfFnSbs7f937Z41kKQwlwfeEiVZbT5k9btsGi0DdnK51mQl8X48
qVyTihZU7yZnmgZv5CxanhDebLpUPPTXHKk14F4hXmYyjA+QT2KO7yL4UZr7G4cjc14S0+UjeZXb
OnCjhXUlKJFKRdZ5d6r+IiqKr3CLP/5Hhn65zzdSig6ifNfHXsQ0JzKlFrNj261Cmy1VMFk3T2SU
h+XDv6rqL0+dP3CaOhTLi6ymaoP74VKaOzGA4rV/OHrJtUbklDTwzJDqjg7XNgcE+yCvb/81pOyn
ml+kpkLtc2Xa9TSNAkMZxSz18IO9cfjRGvjODkrO0MWP3ST/hsidgbidIhRVs0puTP+HAagf77bT
F7muoV9zcGd7bhk4inAm8bW0MvHbFIZx3b4gafIBAIRiXGuo9pq+AO6iKwuKwUbpg/vmuXqh2An2
g7Qjy8wfPKR7gCN3EJtVcQiRhEGz7ql2dzI5h/Im6YGZrST+jX0lN4LFqcEa3iiwIUAv/EbXBpY2
wFZp/nDh4z/K+BvWDe0cq+qyureqlLLCt9Qby3asVxkjLAHOhMEUh+iO5xDHEGLeuqZnlgCfbq+N
WyinnSh8FCFScV5IxV1GImyB99G4Ux9uSVM/T0MPDpudvhp0xMM20IgFqziyvw66wJD+R6QaWK7N
1Pi3V1bq4sCIcGEe48fC1qy6Alud26x5ZE6UAi43lgsv4/ubQf92oHyWR0jnkDJOTQpKrBBEEKuc
Lt2XDBuTpXdS+V9L6xd0C75Jw77yBsKdPxOi+Yu0oPIOHijvBmHihG5ERRmFmnIKRsN1qh2MuEJn
MKi4aDWL7XP1s9vuadsX+qj/1gCoCksdq7tCjaQ95nNwNx04DrpH2yaSYE32cJCYzJxIynmYiwMO
bYRDyfjK1XVlgB5ILl18wWAavbYvokAaXhUjN7N8yWVXH9bGdU/wQhqN7GOu2MClPuQzimCbToCF
0aYlv76PHT8bFJzk4ag9C8NO7Y6g+jMVLwwk55aT7Tc8sREn6upiVBHXxXyqlePWXLSUWGxp15wo
1WfM67LOJUtLOTzz8zcPrdHwYGj/zaKxCd8BmAREuKLZD+eHNnMQFcvftF//etzXczgA/3llMq0M
Gn3bBOEIyXwO20iLpXQvtn7uSHqKZbfX0UiwBdW+7wuHE5nswx1pY2q6548v9mfR5DCYgWCx0saZ
J8q70XOguG6yelDV0/9xKtUr545MlAVI7GRgTstCSlNbBH+b9uY3Q+DOXCxz9b2aET3KSKm2XSb+
7MUIkEUjANzsnoPu1BALtYAjSXbSjOk5Xx6LFk0SZG4yq08meBSspusmKzEAiwyz+KyYnLTOM7Ta
EbkHyulnk5uTRhaMRxSI3qH4KeqchAJVddi2f790ad1b6r80L3SfinPFsvDuUfCBrziiU+8hJIpU
yVjjj2TnO4sa4t/GE3gK1GCnq4q10tW5oNxa6Rz+eNSGjrBOqnAWzVc42AewxOHmhXq+kS22oXsW
ptHC2JduX9gDIHnQGLu4Gs7bpRufs1ImMVsja5QigpKqqXJoYX7HD/Cab0ZTW+fRs4GbbH4jWga7
wKydrJHRKBVx8GhGDDfLlrjThoL2tR2zwIuamShPSf2XEfOtkE/SHhul40zq+LU9fNTKR7NwBl7G
z3iNbOQaW8fiNRwXgvCqrl5GVkvKvGqX78Hx+oIP9fXOhYct23mrfcPDXPp/kzaFmCDoRlUvJt7d
xEs9GcacdMJfqdzG1MJcDLWUodjUozjQPxFT8HtzVL3nktXUd954iaXwTdF6QHWcNisxioM7yfCb
YNY40CMi8VyEP5Ymse6+Xy6xDLxe6Yq9vMUoyac/8Vq7Pex1nXzYRAdiF/KFBzl2PP0JWL4FRUDE
wL94h0SeTJKR7jCHQH6z1cOKxVAc7h/Llb8KwNBAlCqpr/3YYoL/B2P3kqXGKe+kmThai57oQUmu
Tpr/3OqAbt3HZ879NuSRbmr5BNGrt2AqqJUR5fgq5hvBAYmExWEiPPvN95pn+AECdCiHV23dZbu6
ZogkL5rHepc5SDTwPiOrEK9xL1EG4dWKFTTYu4foJD1xscaZ91msWVgSpRCpcUPO4cqQM7NyyezY
duMW8qiAegf2t78HRJuSG9ozNdFqagDG8czc4uKI9jDwUEJ0MbVfWu3scvzr1jwWjHRXdzrFcb1L
aX5oWaEyk70CU9/SoV/Svz0mlGB8fxyZUZnPHDedZIPq1vChofsldTmrmf/z5KYxjZ903EwlqLI+
jeIGfJzstmGs0U7ZX/7FGm++B3MuitI1cBHbElcVfA7ojUTBMXufVujGvtDHMW7zTEXzuqjkH/i7
ei9LTmtCiOEYFcCnZ6MmwpejT4+9EWTfqX9C+Q3GvWIKNqN0ZPbx/2Q5fc8f2u1zmn+ByVVoQt4z
LQXWuzSgWWykGEpxZRAPQRDscPz2PbwDNNpnZ1J8+QZvf1SBAZX6S95RPF6OH/wogdtbKvNnAz5x
yau2205mF01aHQOffx8hatkPqu06cU+AFiMp9BAhtfAVW3t9WB+nWFJqf1G9rTnuRrxZLjbyZpvr
d50/GocCmn8ElrZKvEuWBtCSx2u9D0jLZVgSzFQOmsP2LgbJHk7IRR5+ynFn1i30MUiwn1SXxkJO
kcY67W/30vX4Do6Tr6rDD2rjFuAjZeRTG2OijbwPH8jm4z7hNbXcy15nB0n8oW4DLURUlMstrqBi
kOEpOSQDMRNpiimuEaU5w6ff3zcd8boPBJscOmIwNxrVQFPyLGo7GglnMrRRUCQZl0wD2KtrXL7G
ZsDfZZSd/Aa6P1GemiZatvaCwd8tD4WbOgnylp2fFln57DQJYZhO/5SvcYH06t46qU61b6VxGXAD
c8QQf+AzHfpupFo0nW8191YaNXHxKjNoI5jopnd73kdKe7fGTuutGCBiV3KSxK1PuKS4pAThkOMY
4KIR+0BB/mrW1gEnyJVhM6fG6DcMPvIWNNPwkS1zUKosEqQKC8W/gkKLGfYDNHklObHvES2Tnwcc
e90ie4i9q9UKxRnikxrq+pcv56od+YTDIVwVj1AWTpc8SVSSzGG83tuGh5Eu3QVK3f0XyresnAnT
XoWp3qw5aQNJqU6pmEdXqqqzuRRC2fQ6ogkuXxgOvjiIOZrw5izdk3Yv74oXE8LhNkMnzsed36Yl
cSh5zzc8TlCDi6KNvX1ODU0cN6Z7r9AhUfqskvijN9xdN/sAQmNih/aYen3tmK0M8o+a0q5RK34N
mk+rI/nYXLRzEXZaybmqySzAgG0dZXl773/+JDTsDXDLJyU8ICaSkbm6THRp0DACzkM7I+k9xbiO
zpGw/X/n/JAfCDyl9ri6WmyEM3E2yBr5OTaxo6gfELC3R2wdvhzU76Y5YLsfp4w/XvXVMRo45xaF
0Yuqzfox8ZgApidAcYs6gdPbMMUvVBdMC6AfZR2+zQ4Uqxndpnq9X89e55U1RJPcFZWXoG1zSzVa
PGL6EU/z94zITY5QGC2t/SC/CW3McpmcnMWftEtQD6hbUkxA5FG5ID9TwiFlW5cRKaNoDPdAS039
tF5Z590K3rDLjivVmtjjZdW/ENHChWvImRotPhMNNsM6ukIBc0y9QAYFyDdzGSo/wwY+S/iLVPJJ
ld0u4VLfWnnzSNvqmDz08wppCxeuiC5EGzz8oKD0dM5/z75MirduLp1R1nZu1Bqlj/oIl7WUFYVI
KSMdQHmK7GpGnrzCrNIYkpsi4b+6pwMEAsw/S4YBAxb8Z0JgT2RGrpZ9YVkzOzkScokGcHTWpGHw
ZR7Wp/SmamoQ5PS4A6de02ud3nr5A/oXS+6AX3o7xjaWGjp063GVbJelYkhO7Qt2RWHsFWbcgzYC
ghTlMOsjg3Q9jfHw6kfwkMkz/Un01EwuuWPeWQienm1cQWgjRP5y+kiUDTmlC15AT8iXNxsU8j5S
iRqkIt6ge39OzSuM6CkjHY2/PCgFOeDi54XVHYhmKb0hx5dYEUOShTV+bMIQzhLTAfLdqMsjEGrj
lmik1kME/HyPmsZlIouwSrr3Sw9zXpGpKVhWjPVOEDZ8/IEp7cVqAioK9KxsFQo06NH4A+dVsnTL
lBB+F5Q1LMEkNzNI+H3OIx2eyZFNbX8sIoqLx+boQysHEmX9igO8+NXaSKhrisR8Ju17hG6v2l8p
8Lq3+sVeTBOBPZw54oX4m14c3d9jL2upQ6Pvca6YDBQYH6eKTrneS1uBZlAKNXuKLEY2inzqYoEC
1BuyDL+ME965XDSExuiVxammvsWbXk1P/2NqZjzCH8O0Y+fiyRWvsUD5xEGwuXakyGNsRQVxakI1
GALudltlw0hBL+QusmVIR4VSE0+sc44csPP4DANK8Nr/6fIGUZJwNsmglKAXfNTX8jWFjp1V5z1t
wEn5l4F1ON9X/2oa6wt4kOQby/2DbMTiupm0YC3rd1h/YKBo8uCoUbu9cy9UdYtVhe9qWkZhlD31
Ar8mYkL+5uaIsvt3sY19PUgsDiiR3kZGslrDa1VvxVYpEiid3Dfcwyj3Urt/ploAWWiiIMpdYnAZ
TAiVfq6aFq67m3STUqIvuTzZWxVx+z+6xSKYMr5zSIUVNhvm+2ROJCYveqz941SlRweZqfihKj8g
GNKbKwXDLqEogr4WadQJ6RugY4/s3YLLG6Aux262G2NHGKuZtTkdRbaBlPEOdnJDs0zl0o/VbO1d
/zm6ujA8lnIOOrC0bnx9m7Er109abgcVD4fAJeruDEzndnNoG3SQ2XBrgqxfVzu1S/ST/C1dI/8y
g20J6zaGOgw/2/FJXTKQcbXH2wz1V+J2bRs0mkF3Rht0M3XzeAe6RelokGjKIB06XLsNpZf7sj6e
W65jbU/6rh2y4DSC0iW9iqrBWfyqbnsItCbyKxHK83sRVwyGBrzSnzCgbf938SlgHvgpQvvsQn4E
VsNdNerJjWlJs5OVXBG+4cZMJQVKN4x2qd6B+cyzSulkPOv47PVLT1KAtcV6NU/tRM94XY3DRS8P
i9MnYhFjCZBHjAMp+FYVI917B7N/DU4LKN0gQXbFDbeXsq9TE4tVJVqmxktVgPD1FHWyjaVr2k8S
Osj/qIkY4ko2KAhfFk6ZeeBKkI5xSXYLJImJ7I2Vprlv8Vhsnd8kRztyUrw240v9bJHi5X2Ssqo1
xxlyI8QGTPyxW9XBBiwEMAdWH2HSfZKOYM+CgP428GdQK839WX//O09YZGak5vc4wnJNv0hzjPBi
M88ac5WTJ8+GruT/Sj6nyiPMLN0lIH7RNnhXwddds37LRBzS8I3ytDUMqjEx9BXxEkbSA1XRpOGb
FZYM7vNFLABeQDI/l7ODtUQAATtaUzfFkIU9VAwvfhEQRIrDearwHYL2uRE3H/sArSn5ssDqofKJ
6ks7/ghTaxzEqAAh7O86UryucKNMniuJb/0WUI23C7DL5P6PiR3+1sAch746WzPTgQ2kCjTfJagx
mTV3Mm/NcKkb8SVwuTHg0uQCXi0bt5o+5+MjEeUwPwSUzeI9zo+rsmrCueMmfxGooB8hNwnc99mA
LafeJIiNg3ZQQ4/i0U8diUpHS9dPUQBLvFbYpIjIChNredv/Q+wAL4GZOHNMQ8QZnV1qCXy1aU1D
ULpEMpQY2opv02aVT05TwWX0HbrRpuG8VP9RKiaxS58r9TXt7ApRzkt1SvI9j+V7awKnqCDZ5y1i
FmD6F0KA/oMGGCLLKlVvg/7NW14HudbiliPlwZCxOPjzvi8YbKSVhuACCCGelGxCyXwmRnh9IL5b
DM/suu1n395InoM97KSQyAB/ZcJy3+9LIo8KUCjujD+y8HOQ7OmMSZRX2ZCyueyzCWYPwb8VzfzM
Zka5iQk7MLsbO/X3W/ygjDJWrB40pk1sdcbuDYul69bkH86PXx3BvLIohrIWpFb0UvhhFwYWt3CG
+HgJPxo0DpxrlWA7sNHHamXxnB1m+Kh8ZwKqKYbh1DoTfbo7EHdRTHQtFJaBxuyvcE0ZBx8/7DNT
PqyF7ICzd8c3sy+hvjQ8jIGshD0gYppZuM1nbuzjNRljHQyM12pu3QAjE3Q6NR+V5THvEbCAqiYU
EM6rZlxsAmb22Hejd1FUvGNNFuuZFSTPRlO4bMq5GQ3Qn0n0gPZ24cv1o4XI8Hg+sA9ZyNsTRcZc
kmc6CDJVhCZOrjtPVPhTppaTZgm4I2TGrh/edx1VFfp78jK1VnGwmhJjlHD165jz3TTs3ub1XRUb
g1lEf7V8NbJPKZqnGGuWdrc81EvBIlR80SIVjGWqma9Gg2d+/zflsQj7VjKUbmjGP5aLU76O1zD1
MPtBv1kMI4R1Ti9L9hfpWlndxRClO7g3v3rErlHQ4pY1p2ydb1WeQPHJe+wYWUX1AE/IOyp2liuq
cthI/RgYiGSlVaqcmvb4R9nCo3e/HAmCTrJYB51kYtZIyGDYM5OpRPHmXBJDaLIyVWuoAwciS51H
g+w2812eJJ2/Q17wh9QOY7Sy2boPbJEXRzjm14Xg4XIELmR1de6zRcPz+87mVDDHCPOxQss4iudj
NwEcLVdYZzZGqNttbtqeVsHIxukNgWFhrMB2STEBG7TX/X00S/KXGXi5Vn1LGtjvjs48qQlHtkjN
eVSOnR1lFZLfSUwbw4EBSEoVrV6L3dq3hOjSDpe1wOfl3sjb/FKe7Hdh2FMIdVi5+gnfZCbhrSKm
fhIYB3KJJz7ewnTXJ6qI+l2IDSlQwOO5CpQ+lxnaNtdz9Zs3NY08KKTPBeSqidtVtb9cfk6XgfXW
vUes3BbJ8XySKoHz0JnfJSLidgOFu83730h540cM0OGfjq7dUAhkko9oQ6KNQJOutbq2syz8K77Q
0cEQBYYzy2BYO40YH47fdlckQXGmx+ePt15ZPv1KjPp2Z34vDUhF0evHH5WNLfFv5IOEQy90z8d5
LhxwRGumNGjTbpR+80ZmXjx96PlJizHwVHKPEkFuI6mooToMuye9vBkDWdQsUTsfP2qY/8hxcGTY
fggub8B1wMH9i5VXYeMVpQ/TnwKdzN4bCG2ioIdKCN2iOrIeiMNi69OtK8U12Ex5xkkIOqjCjs+i
iZKTH/BBaZOyrFJIFOmQ8j8OtMT8UdVPVwHtLtOfktJJTuR+STWszoA7asVs9cmeymAIVW2MEqhz
s63QQtP4mbckbzHrLwsIVWkBjfsh+YwjWqBGEhNPL8F35cpT00ilRIU9MM2MGqhkBLWMKg6ggGih
7GkL3pG6VBZdLqUiIc/i2Ovc1IP03KMglkq/CmhvpVoTfgM4MlP7JIW/NlrVronxpObxRTjzClU1
3pIJFZt+sKVHiPBCB2NltYnqZ5O6roq53EVjElRPClLmAQCNygOuAa4EBcmARMUePCWM7kMtTDre
4IO+2eZcD+JyaPF9kNgB0Ro6apOOIzVu7FYW8G17+VsVDPYfVbMiZsUeP+S8MOEltFggrKuCAe0j
k9bPg4pzZL497wIVAy62PV9fDWo9cnVxtVpq0Smg1EeKJEqmpmD5QCeoMQtOojjM3KNali0R9Mip
yG6MxO0G6lnq5kXjhIm61Od9FWD32CRXXDIeprLijC7hFdIC38B0ra3bHQHmD/UnEeCWdM3j1PkM
i3bFqFfvfDNYhBlHe4ab90/F+36grEAtQT6KeteaPnJk30jMTHPcyTV15cLCJoVoewI6wXTvOhTP
SU8xp94CtKNH8jTYiXxQF3R0BSO9Gyor1tMXo7l9riu5Dcpa24FxPSgS5fFTeNR1ckcPzxWdFdFo
RV7Raso9zsvAWJqbWLydQOrJ75gsLi3u73AbL3K0ryUifUBjcIHU/dACraIFBP8sGly5wtous3Zu
2sWjOwAQghXW5ZDG0TCBB22zXCycOk5BimNd8P3C2TBrCieACWwVvr7SNOPRbDj09K9gPlxVBpSS
KqhgxuzfwI4sQUERktdJNUOKQFXb6ONE/3eGYohC4JBJTe2vXVJwOrou+weBEQONzfpCjVYgF7IL
JwsqvTO7i+ePIy7ZUDNI5HbsfwCy3rj6FVG3HQbXsNZUSSNSlETqO8kesY1kSWoI5v6g1NALWtsL
nMkEBZbeBeLR6DyA5Z93Eqqcy5G0VxDjCLtckvFOYKZjlADxmDb3vSK4/YtgSgfXfc5XC+K3x80N
DhhtD/cnlrWy7DMe7/inDSmhbuQwrt8uKJ/hajA6gb1Uo9j0ouE3P6vIjfYewpP3khuZZ8MOVZcj
2CRQ+0Xj1fHlo6HGEbgqgh5pbyXT/OVtq+PIAXs18O9VFMvMmy2DgGFkqxSBr3a8grN+hktzqSXE
edSoVv2p3OT2njfYldbn3lJuTXOiUc83jVgarS2ADckO/DjplKx7KGuU72RElpnp6Qx59Pz8sijP
XyTpNunXh0nx0NhSCjsUhgUHM/eyb4puTa8vr+wa9kaPH5oGOJszMRnDvwEm5wZ+uy2u0bj6H44z
GwcGHQk/aiND01h1RKt6wWlZAHW+OHnpQcdQrYJeLbaIjFQnk1KOz3XEoCyv+fy7WF+V+N/O4qhn
XpZXOPslInKGsXBPyWIn2nWHFctuzUoWFrjtsgnZqtgMM8za6nSN3rJKwOhrYm27p+1N05aMvT4C
VjmQV2RNoKI6tq73mXwL8veHDiTY4HEjC6OHYvRhc/BNIoGdOzR5tDNQd4gbA8jTPUJKmUN7uhV0
SXwe/F3YE0jNbBbexGlFwlJqqeCCWn/JRlnaHBZFNe2eCEBY3UTbQMt6vZZcxOAAyTXjy3TyLBAF
GZP7mwQVRSCiv36N+CMpSvkObxS9yGuylkLP1Wv9vAv7KjdPKjKEEC3RdI0FgE8I70bFnyom4dZ+
BnGzFVuF8ubTLyuDpJGffNNeQZYSI4nC5/UBMIgi0HooNiIqtBuFTjs4CN2Ndii/ofOY9NLZmkuY
l5l2qrdXWjhmK8rPIoInZG/JlpoomXg9Vvo/86fGdJyIVA6kWrybGTOZY/DOBuPsq7GWPRs/0kJw
4UgjdYy0pT/ARa53BPdLEBflz4/KmcYOwCCHDXD4vRf2XPbyyxugfoeogcnTzz4qulIFGQfohZ75
avSQXKM9woEuKChrAW7hnJtbqPqO81NwEbnqeqBCQNmIXAwRU/JGacPYP40UD1nMBFWH9zZA3nXy
/XxFe/c4TXVdy1o/n05piA7kc9oZdKhG5sMUJXl2Qs3MYvgqOgjkIwGOCFL63bSWbf8ZdUMYFE75
wr2yD0wqtXEXpwoS+uDvJtYosOwNIY929oP1PZFclrKZdHK9Jpgd2vlGx9xNPcbQzNsaIxtq4RBt
pd2N3QGWbK9WrV1Bp17TvmMhkPv5qwN3Fawo7v3pb+yrPb6xhKVlU+1wo8VMTbnKGmLsg2YidJ6U
O2QpeaR8WYRQSfHZu6192OU3MIW2e6l8N5u9RNJVP8Dt7yVEVSaNI735bz4cVoylDuek9evWalkP
fcgmMEKS60OpfGObDtaNREVsVQ9+2d9MYL2lBdxVcW6un7G5Smv/MYyiK4Ug0zRocDGoLWou08Mw
5jJ10t8O69DICBUSvKEesYoBJgHdT3lIi5CRRfH2JeF+yZQj61W1hPNcAf2jWBYz3ifSrTBNPcRa
pFeTOtsVWNAGdAj49zJJybV55++uyIXQ762NNh2Fgeqb8rfz8u1gAVgP/8+cwGXR7CmNp+8naoK0
KVmre2bq20F4K/+znB6m79YB0rR2cAaExxwfWsf29mvF8v7DFlaQ9bVw8Q41nQm0YdzNMmg8zSg7
poVTAKCBM9L+E9tH53xXwHPzm+NYBaXDkY2xpRdBBK+sJdwJuAeHlwZ/9coX1ABkTy8YPHz2DDXg
T54m2venvTQo3nPllc9rOViyJhEfrZKPwgjiUtv1HnGrIgCJlAULlw/rarvefN0eX7nBnlD6dFup
ya7sbYwC6WyT7W8HdYiwBrqhrssHGeeIPRUld4b4Uxe5KYfLFbuwlasuK84EK1cNTVKwenycZMZe
LxmZxPpIe83v2ZyoHPE9s6WRAUI+Z0j/n4A5RzA+cYSQbEnAgb6Z8AmszonWwM9kXTbHYRJntPVF
O8MO4e3He45MAJTKKPeJvFh6eOGEMR/5SIkVYcpMst3iArPPN4Dd3APJxgZli/eOGM2Eokt447Kp
8riOKwmdAz+dHRAYCUrJ5pu6K6FiSLcWADpPtCgp4znDbB+DgfiMIEP+NH/AAfmUfOtwZ6BW7f1x
IMb+SBXynPKKOKFZr4cPhPQ8xgaXH78HmeADbwMHaa2Ac7J8puqEgZTjyn/mvqjGpN86uZ9OHgfA
v9Q8LvRO/P0wGB+9lGmq/tMeJf9hUSW3xYMMBepfpCO8jFq0j+sKv7fkKBAei1uy4yQcw0nd0Zu3
7sem5a3EtD0eeXm8j0JAXjY/6AdV82fDLXZdXJ9AYeA1Ks52g47ffDlq3AV556kdmEUj5vhzEj1u
3vriLqR9/ZqJxQ4bDd6GZ3ZE46FulwnDYDcnAy8DtXjcA32Wiek2Zf6kq40pltch3u8O4h69zMek
ukmtzDw1R9N4brV/UVTALknw4K0/iyZBdIYJ/UwWaMGXPN7bo1U4jRnuJ5gJR690gmSnqtnU0Omj
xHbF+6AsQRwub1G4RKXvrgcHJfSs36isoCHjwvYIHhpCkOkqOP8U7d200JMDI7IADx0j4vly+rgD
gOU+RZ6PqEIMRG5RnAUVKSB3lfbSzAruV1dZG4zb+reTld+8qDToPIivhQlv49V8FZ0UjXjDJm03
ELB8tFBlLIyi8v2uxW5BcshFNp05Qo/FsMw4a4Fosdccqgyj5dFDqCguvO321wXfJx5LgQghqPHo
CpavANOkhEyM8MPsnRLi/sbAUYMLpQnZJCLEPdwo+0d5GH+qnAlri36dH6RTD7y9yDOwOGfl0YiP
OeyV5BW+GSD7gVbL9/a09eEA1jDOSz1Z9xBAqRt0FHDro0fFWWw55CCGkjM5Fc3xewWjsTajBA9I
xrNIxWDPChqOVH99LQFum62ShDCihcDkoZ+KBvyZosk7ZfLb7+EQ6UQmYY1jHzwSLqPn8bYXXT5c
22hms6RiNuLDTF3OctbCfXIx1Mb9EgpEIdSVfmsE7Ivk0nY4acDnZesI+5+ToaNtdlgxaXr35DOZ
d1h0HF14cXELuLJavONMWXUgTPtW6Fx3Mzj54QuK0MOifY8zsE1butIqfnzQbKB+WaqFQPiDFrgZ
rapERiEDwgKcGtjgXWbB97g76h0LZ5ZhAtb9Wjk6+0V/PcNoPxhYwYbyXzm6u/v8/4erOgYpKkb3
J+ZQvLNTCSCswCUiUlJNPPoyYP/IPbHdnrrPyyfTQYsx6Tkb83y8Zxggjaok3pqMYmeUufcpewgm
KRJ7lLebyJVqffZBk+JIB92/wY9+AIlm0Z1VbLsqrApHN+PeLckUUonmfg952In5X34gZk9UKcah
zFpUKr4n5XO2eioWHwyMLHFdU74Db83gd/ji4sIa3U2Z6NwWbNsSGoxGrSBx6ohInkdsJtGD6WLD
QnCxyl8LGx9Sc/0vDBsYdIuRf5scss1ChaCdC48OR+Dmkx2fS6V2Ybf1igugHxTT9BMTvXSdGukY
fq0rtYCVQCL7uhQ0On5IO9BIiPuDhDqkxz4MifwdXm2Hf+6NFc1LzUQRLjeqj4TGES18Fv1m/aBX
WjcS0NlEVmKYjZWhLjZjil+CmFSfMQS6Bkbsnd0XF+45tdwzU2VfwpMUr7jIF2vVUaEpzD1OVRYd
KXWw8JJn6d51dDG5GzGxJ9Ngt3q/0h9MqS62mWHQUyFiKVFD54VSjBYDkdRVdSj1RmBS2YBRHTgi
en6q038Iqhxj3i04RtI0gYTYP+2cNCgzbGmzRgRFB0Cs5alRmant6VMUrmUIde0N9wnHlR+nEVyp
o0pkjOz+F0x9P+GxJ+dUtfNyE4+oYcvKOvTZH/Csw0h2igH19v4/7vzOIK4jPWhrG7pe7+BnAwGf
aTqIYI1DWgyDouhm8MAPE2Ta+9yOFdCR8wm/SoQceynh1wbs8xLUfQ/SCVaJ5rmLyZ3TzV7y/08+
B1BAp61ABUPvy9bnMHfwbSxgOvTbggW69h5Ei5E2fnnbKWSzBjiJq92d7+xHGbBZmzYTZ7zmH8DJ
pL9rsWVD3QBkDczd9Vk54VhiwT9dZ4QdpIArgrB4fHtJVGC3j+wXqEyfjmldZ8X4XGcDr1TYhPww
bVo1bF8Yf8qGutuiIg8vGqTiFSf8IWY8zvI/qxpQajk1eArPfsfJuTM6DvUxk+k/vlFFc2Tc0BDw
IJUwghr58dO1kBaSwalJ2jAWNcQBBJrAnk5cnqx1G48F6RpZxhLpLsZ/sJARNEi0AZb2VpcMDJ32
xUHDkKdBrxC+mTAqCXW5CHKN/TbLadhiPXxPC2bmjOhHeYeluQn9JxqXFrKswS36alii58tG5VM6
rizB6woG6RYZOwba+cOzHsYp/Ocmu9iP9wDs5xPEpvSRgfve0X3f+CjJHmT5ZdvwytWfgLFu7ByA
4YoX7hNECCqNHXsgKcUsQPERb6KFYSRePLEnrv/DwFyimfB38nHNZSqgKiEfPRgnC77ydb1j96yk
0/j9ZcQ/fDoOyAkP+572fYpuHxOg0v9gXrpslJCyRmQHIrKGLuMZmrZlZKjTUCPdDIiN5LJghfUk
LNy/z4Da19T2dwo40eJeglnX0tKZ3AQUN+GQQa1yMADWuxo8hCWw5jNH0mAXDt7/DqtTEg7SfZSE
9Tz+SQaUFPJf6Ridk1XaUWwSsFPIC1EM8uVFSxjHDjJXVHFtAArpJ2597FOfppsdGUhjsI9YCxqN
LZYXsKifnsr+ciBMpRp7dkvcR8/7gZ3oR/YbwfRM8EQ/C4mQ4MH7QNBLT+E305SXOgMQL5kzmyGD
FLisn2FvciGZIcp/CQFln85WvUa523HGyEzURD7lK6pbhNEIFm1HZpbo1ID0/sJzZFY9PDV92H3J
vqBT1rDbjzr//GSik7dWkna1rvZJU5ylaQVO/4GfDp39ic45ocn8fdsfog2ehxeSoyRgVYxDen2/
ZGvimunoEa8fRQcYFjUIBakFeaP4HHQiZK3uRoxcBtp/g+ND4gNCcVxabs6MdHCpoqOkNKfwiqwq
h9OYIYJkxi+EvDq4BtW4/Lda3mfKeTS77e3hi30s4uRbIscJ+BnWc0pdp+vrA/1msHJkNywaIp+I
VA3nsG9bjVghqoEQyoWGeZysc7M5rv5x5N9XQoTJoSvC2OFgVZ4im1JjySu3zKzyXQfG89H96szG
J++Xqyjpo8X+/TywjDcIf/LhPCYR7uZjuK33JUDlYk3/lUp/C7LdElsxxBDRzOBEjFbsySyzzs2R
KDY0y/vmllZ3TdFwDIGKQgXttb37Qrn+9La/kcltN3OcJKBrXkc6OAOpo4Z2Z4BKOfu5iQXpOw9r
nRJT/xwtI4A0J1PoYHu3taH5oapVyp8bJGZtpjomXrBAkuF2IJZ5orBNYk6EpUZwEsJdFMGk0EI4
U+/jTrMMgZnNq+LqUFlUxTepGiLm/I7Hkbx+ImX9TnccXq62aELJJa82pJmrxkSLPUj9U2an1lZi
LA8J2wawd+Adh3j9SrRDyWdkXTMLB/m5X45nd214/qQKPoPHrleu9c+r2ifgUMj/4KNLZ+PgTtGF
i2PyX3PQM9l3LAB5QoQkJMEvxalNv+jiNKvPuJu6wHkMVaDMx4pbcnI5ZTYN4f3o9rR8uoAlevtd
39QBeiYYslWjgOqhEUFlJe75y9oy5VQfmLz+3WL59FCs91AxYqFv6DvE8EEOAupQ6q6IcKd0GU8Z
dMaRSAKCU9ctmS/yucC7JLyXnXohJ3dwUTjkyVQI6VQ+oFJb9SqA6LwlSoPVLouFIOGttyadUCJy
LP+TjocR/Ma8o45yzfCZjkmq3iQWPGGzKVkcBHfo2PiUnFxdjMlpvrVJbGy3A7hSRuRmZ+cn9ZCT
uSaI+t50Wz0MJvO/iX2dIExrHjM45T6Wp8rpYEN1L2ZCfAgVfXM5RWQd9yaPi0N1C3Mmu5tp0ulb
8f1D5tDVOhSSoq/kC587IeAWpDB6tnB50OwajESi46PjlE6u31ytoXGq61EcueN2eG4IKk2sZ+DY
iLk+fx0Dt0QQAL1cl2H9vKLZ0qTYNAAzjotLEKtrK3bBP9o/4HrwD7uw/vd/1gHOg6ghBj6vWDRu
kQV4zQaBI4RLSRC2IdzAMf+dmUiLZoVMyIAmA0aDBtpNlteQFrV8KL+Pux20SaLpTkKT7jE/Ziwu
D7H9wQ+JApR8zKqWhhXEAd/yK/nCN9kliRLYSkzjL6NY5uOGfgh5RVtG9WU4XvMt633/wCdtUJrC
A9eLOe+9fHtydVr0n1rP9TSvJgP+PCQx3FtHP1nAmyxTuk3Hp7Ma8NQ8eBhgHiLj6Vxmzx+NHilJ
z0+qhWiIZblAPqAGJFQvmR5KnqhvaIoQ9Fd4BIk019goT/tGXxiLfwl4/e1wrd9XrcQ2Nj8mqSHs
GdVk0nv7p3s2b5CywMlB5xg2lUGiWcQ4nEzSag/dsUALIsKxB1FwthuV9t8tISvrHa5zBuIgl1JS
YnB6O712OwQqS2g4JuRyGvAeAtuxuPU5+XuqiOb4e6qwN2iUzlbC0z4RBZSzhrzEDRiZupkDryDx
QgMnshHt5+RHfs87fRDUgDdsYT6SoS9wWDgkBYS5uihOgqVZcAkaYZEUtyfvehY3EGNfTNwxd/NN
/tBv+lObGRuDVqRL7GehlUC2xWeA1EiBaSGjXFsm1RkPMf/r/vAutP6p+eIR/4javRGRql1OhE4F
eJ9TOWo/UZqt+pRh3418iyj++jzJTW01PcYx957esdqDQ1yV1WhOWhs4nyI4NWPLbzrvi/O2pY/F
gmC45qtFfNCBM3CKFv8jW4jIvM0JuydsZN3dHmIdEcLNl6K37CCdJdlfEgsIx2dblwoBq1lVO2+/
V7P0ruEXg6f8gzolFK2oiLGhVTcn3jZbnnbDEVfcMsUSqdbdWbpytVL7WqYTO7rlY0XPy8gDDIZh
n/fHklFBjjx95gowyCB9XhmAiM3DqUppTfwZhdzs6t64ilWObzUey8CIKwb+RpQoZSbMZZX0d4wF
SGPOg6lY0lyUvnmoT84lNkx5OP1uSC0pxTa5Q9rJwANz+QN/U5Kcvk0oz0QaJsvA/5LuBJgZOIX6
7/elm/PCySz1yo9ThJ+yz61nsI1qSbrXHeogaI8KJk8G5Lx31gzJnbL9NW5+mHdSnaTntsWvwU5V
2SVXkXHI5m0Fb3Ra2wm1qiVatK0pNo3t+hy1tvl8TT5X6nJiEUbYGr50/YrbABCE6ayqZQMeGHBE
VqLni5JgnLSGNzDOuv5j/lYd8VAxTcuMEcm3wownkQmKE/8W7wbGZ/4ZBTtHQ3Ph8rnFxd9xK537
5B+8tWUXo8vwGxtGmDTQXyUEkrpUZW7VG9ogPUXCi3SFiK6DhrmiLOPBQa/vnW4I8V9xy6YVdD8U
RaCZXXyjEGb0Sqqvfy7dKhQELy/akkNx7BgPEv6XbdJg8RpFXOEJ5ihXQalQ8gVYpwf3egJeoeN/
77DWSgVCMAxWh1N2aAUsHKS3nROoAcdbSRCqgu8kM3siK2Rgg5Xy9p2nTvbMrtNBWPCJdfJTPJuo
6QijjP9gqmjxeNlukwhSOjHtL1WzC0216xUlJ1H0hPrHB1ZKc8HgPJR8FuIi2TCMqGQ//2WvMEPB
Ltk146JoUUz006yNMLGme+6OXfrNVAfz/vG7W+tx3FaVOsCSZkMbOfTTY/P1M2qr/IGCcYsdo9rd
gxnZWDUBWrPVYGAwdyHkhDSpTqTuVw3fVBhpPV2Ce7BbyTVTjVYut18Yrdvr2nPo4fhYjE9E/Cmy
Ljm5jasYrORWAcmr01Tg3U2w/jVyh1wvDYMpuHrGi5BPDo7hH38S542jCcMhi9mG1+5ppQ6Z4nAu
azVcp+Uy9eEbmxuuDGN811EQ1ivhF6JA85RoOxMs03S39vOl+zhPIRLL80RFipqnm8x1KSzU55KX
/4A8KgeXyCiwNT5l6AmGSrdLHoLjKoKAhkgfBlF//eN8BRLiOocLvrNwd+/8h7UfDSRGPUuOssC0
k2c6e28L5pv6amcedBAk/dZjIOTH7yHHcn8OB/KEPsskr2kLHkM6rm0m0SEVXuOQ2uzaezAvar6e
i4kX1rgfE0e65NsIPOTnmyBiY1xs4D3woDHd+IBmn6g2oIKzTFUZIwhRv6VIGn3J3Sg51Jgk7pUF
f6TkgQoATlnjTyNu7PmUqoImBbj6DlxXw/SJw5OACy/0GuX4s3WcNqtjsFcpBufRWodjpXHrrZJb
Gt/qZcj3/wbchqhK+2sLuygDgxUwyMGh3ABGpbEXqMfbwmuJAzyxrAL3P7AygBshguoU3pqCfJ/A
uvytwpim7HwJoZMZGWkPtrQF5zvavfdu7bygRSynHJiEAAZEkDXJA/NePVIX1xEZbECEVmB9G+MM
5FOlUoNQQeKOLmysGBP6UKSzLU3/2E5J46RMMDUDQcYPscXfjBy3g2RTaNkQ9NcUr1ks2q8XGGi3
VmcZYsIFJzDNQQexaS8KJgJrZMoZjsfkX2Ojx8St6PXzPMNByCtCW8Mf6Fy+3IKwiFn3CbwGWAbf
+ASNmKpLWPOx94Fauo6qmL6osHuuvcqhz8uFlPKu2RTBmcEXIocsxACAOMlcAgs+mh662HY+81G6
luElYBfqbkwNEb5us+uw1OJSS2YDhGAKDF94uBlPUB2Zzj7JBB/ms/fnYUnTFtVT959DNqetyWK8
PVe8C9n2NUUi3GD+Yy7wRpmn4wlviH/RivGd5/nTk09E0QG0DBb8Vy1xiD7x0crOLNoWihVpldn6
eYz4J09KvPc1SKz8aOZVHDoyj2/xphCq1Y2c5KTLtvfzgHP8kalk6gDgVGo4oucSM0iw8NCeaNXH
iFvQSVtFbxBhEpNKMnJRh/D0rI5cET8IR1ab5cX9HkRC9xtSpxNl6qmVWfmY0hYIQikAbDwYhMGN
NdhkaE28Jpu3YWYtB/BwCn0QjpW+z32YlsTxKQCHEY95DBLUTtqk+sXgvRYjla74Rg8atjNn4k2I
XeM3qtVCGnKolraAgcmZKmhR748EAI7xq6nK1dCNEbM95B8GFIdNJvLhPJ8JjKTtaxbOSp4ugqwB
3QMbZg7mATBRpIw2loRatg2b7NLJ0/30/dDu24A21fjerDS1vx7wKSbfDUvB9NIZDP3LBP7cBwD3
q/pbsxTlhjOUBDXL2pNE4oO1Utj7817c8WHWe9WxPzr6cDc0YnEq2hgdTHC+ybErb0e/TD2WNrQA
3PBMr3wcxmFOzlhEwDLL5QN2EqZudZrTk9datkGe9HQhGZCp5hvJ4k9zSWRGpQIAXhUTRQPa+16n
owkfWxwrxES/DJMQQD8qdbIkRh3y+bTLrnepW3iVtW4E8Mv8O2/lnWcsUTC33W06pSVCoe0wcHZk
36rJEx/62S0Lvr3WZOpBaWnev2I//kmO6adyGjqPZiWyQi9GmUNTGvsTTCF9yIOkwDBUFKA5znss
V94XLqjZRSiCng+uLL3gYnUXIuZMqVO9iMJ3/0cEoWjJQRG3mGV7Zevt0OiTROSOvvLKW3cIjBfI
lltXdVp7JgVguFqZx9L/MkUI/M4m3Y4LrvSI6AE8uiJ9dfpBrh6ubaogUowSKdmC3RTi64D3YmTC
Za90EzqvgUVcEJpO2ymSkmsDCrLBhOdr7PSjy/FZgW1Mf3RxnHvUEjdQfyS6EoKu88QiJjluq4kN
kDozmD8Mm92ntVEc1Ng7IJP+Q8n0FJlI+IirhwMJMV8bkalxyvUoZ8qdMdfC5obhmXtqNPy4TOxO
VZp7JJdXj065Eh+4E2JzO0LtzvbG0tH7L1vPI7/yIij/WBY59xFAvKBLkUimA5eqwnfsbe8tRTyk
tw+dH0pRA7UCedMQSL4QIwKrqMSEBe0dSlRr3S47YtDL3tjZoHpVVvjH+A+2Sc7/mhIySKyAgaJc
ntkFOyZyl9o/itlY/nyhlQJpHuLQlvy9rhHBYyKcPuBssLXFQvky5Ml46G3glJ+uP4nkCh8vpVBQ
x8rAWiwHNBjGbArIKlgJ2T47USnXWpe/YZKb+1VupdtJBnbH1o9VU9Sl3GsytuShJ0WdU9AfLpt5
znEAKZcOY6XMWl9rs70dL181iN/6xMgPQyb/UZoG/6loC8ZQTlDRztt7EKi/Y+AfIEjKh4z5JPNA
YUIJlzUCDZOS/4M3IuIfwXI3nZLMdBV4Oh6hbnKY9SZB2XK0SNQIVLd5LbpLzP5NSVEcinjsKuQ5
f9lIi0I6512MnYr5LYlXOTDnlyiR7H77IOXxjy4V2rPqRzotPu6n31XfmQYsCubY4SPBt405Md80
AYWZSH0N1WqflUuT7yVHLkTJmdggXmp7cExiFM3qUgHhTZ6Xn3DGw7b6vJ0mxk/KILc60PCKbjTL
HyT/NUVOeL0palBbJQqKwWj80yzaLDu3DxeuNDPRekHb+8lV+Vyni4mMQb3+eV4qXQ835HlCXcbU
5+6s6Id1gVGe+yhCAT++dzHfNs+X6e2Y+DuuUCNmgpLHMxX+uT6/Ga0+4tqGQK1gH5t1KZEkxaAO
a0ncqygdKi5EBpryv7k24aowoa8thbhxgikD3oGEyakEyazRez9IfvZPo6E4k3dqI0FArdjDVELk
FhUiPvFVBFJXOSmWdpDhJfuObI97jyxBgOQ5TNkNLMeWAxsXQnVZVz5OAaR8xTnBpTJLDW0fSIwR
pR+HPO221V5c3Bec1aQnfrnpky1K4qsFjbbiDu4lM+iBRNDKOudpbfIsIKVSd9WyXN3IilT68wyx
Bbq2bQAM7A5Oi/hW3QRHRdq811QkKMPFMosmm9WhkmCAsSBcLDAlobthMoaKK/Sr5+30d1hFj75F
0eRH4q7F1H1E+BpALfBCIoH+eKR6RD4iPy9MbFeJ84Nq5ekvDaBBDpZs18WkyT+muLd+3inpLRrE
WOHN9O707xQLzCMoDrl2GJ3niGnfrMB5FCTiuTg4mY2ddRA2q56BYV3+8R5GYfypdgEf86bf4E1G
aDI9jchjvDRo3xuQxHXqh+04yCNOM0LYUI60foMaRm6b4ibZU4LyAOE3TFRkS39fgz1MUwAAdvVK
ThPc7mKFJWQ9T+n3uMYMdf26v6b1Mx4dfrh42YUqhXTfAZ+TCKLrsq7AIUskN+YSNKftSz03UK7n
PSxeh5e/bIjuF8+14C52AcCqmjanr8d+yM5FwZbKjACi8h+SPt1vPLQrCF3sa0fp5cQQalLXidQk
wYP+L9vTZH/DVyu50k42uIfz94iA6hirRHXrQB95epl9eqIDyAsntpV2YaEUk9jUbDobEhVUJoT5
zSw10OyquL84jbqDWElYuPDvyW2XmHNGk1kf9CaFIwMYWKPxzYvliGB3LeVeTQQMfOypctK4v7B9
2AzeX9vwgpNPzyyVUvrjSs1kSkMYRF/lE23w5Tyjy1WD6ZCm26oLGD+Fz9dONorVYIYdIEimcCw8
GQ653GkUpvAQ0GBys83XwdELzdAP/3tfwLOthSxQl64L4+hYU/pkJaTt8Uwpxhd+R3s8JAKUeX+p
rPdBDip5MUHoBaQx+k2tBFAzCuMRJwLhWb86WSemMj2PkE2WK0dRJW4icf+BjYiq3A8RGsOjawzG
ndj+qaLcFJ4RvVJWiQKxVD77eLOHouSfB3RsyueBbD7J4u8S1Oomcf8Er81pjqJGM1TCZU7CThFF
74ciHKpwKTzDb4+UypqdkaB+c5AbuiHQMmQ4qJy/Fo0yohg0Fu8VJvvKQBUgogMDHboQn8vN0fiC
m7Jfppt69A83UYs5KxofIKdwEbbQ4eTospk26boOzsBZZCLEaW6rPPXCxwIPrBXg4kEsUFquLRIy
VMhN8gSGiJznLt5FK7tHI/ef5gTYIF8w3kcoWd6h/shSmNcf7/eyjjiYSTT9yTy0HM5L4aOSWgym
9PNwXajaWwSbcbQg77DgvResbP2yPkcItazwE+A/gh20fAqMJ9wyZKWT/cTci6daRqyKrd7S7K/s
kfDMu7cbTxBDyTHIYOUUnFi4ymWlnPk3+Ec7hsCOe4nAu2VqyPSs/0sC4R/7dHuiTBRKVi/IdOkn
nYvJt9sQGy7d4443CIZXvjqp/rZNaKGgIOPfxIxxFfcE15uSh85q7/UHl1PiOHIFr9KqcoaYstqX
ObBp4TMNbe117qJ1ijMAl+sKfJc1RbIaT0+Zvn/vEeboWD+DCo+ViABNK2fBCp9377OjEnljVmf9
L9Ln5/Uk2NgOzvO9lQmPLh7jww0viYwWb/TZl0YZjcW1EAk3I5iEBSDf6T1oOkwA6e3Z/MsKTwUp
ugdnOTc4ARTjXcxP4Rgkkybn3A1K504ZHW3Gcpr04+vttnDEX9dWdws6D1yiyyo/grJikbFr4t9B
jwyTpiGAxv3qTQtSbT4zhCxUuRE3SPzkuS5yboAUozb9oLZ5DihY/G9Etz0ZXqH/pPrLaCm6UbMs
RQbDil2DpAt7X2P4Ej4fA88WTUQ0x26R154aXEVDuTEIpzPs0XiDFqeKRLhQx9ooE5cqJSlOmLg+
uizj+BqdsbLEAn99kocHwF0YE/f7RwciClM0Gt63nvCxHqT0CN1I8z3vrPKnr2EbM0UAiSAV7mb0
/1CTHoog5ZVilyPvj0TGsVuM/SKKR9Ipy1S0SSxXcHiuT9aIiamwJm2O6DJutGlzDR3KaDZfuFI5
HappR25QH3cdZehd5ZjlTTP4VwRjsFXgatL+gwYc4RRj547b5fQDJlhsZfENQTGdyCOSF23ZnUbN
MzDz4Ts6j736ykk6aB/Anwh4lL4rx4K3ADeKoOhQDvFEc4y44J3yH5TGBnQt96vBn0oGmVAvXBdv
X64DlRkTC+TpJqGBLMMOYgTId7c6XSxy07GpAFCT7CISSY8jBrzp9HbKqHOx433LOE3bZGOnBCT1
CGo+IUjdoo9pjJXHJjyEy8S6QdHwqzaCtp84MOYMK957/wFI1tIBYDhBj0eq+rFKLQA5s1zXLjpY
C6ODtxfxSWMFFpiM6QSyB/dClwer+hPUib0jc3CTff+stI7mLKTXAqQKfzS2hK7wus/ClMLsYC67
ryQ9ZNC/OgKxWEvYuZH5AX++Xajl2T3YmOQ3enJBIqqyHa3FHzIJwxX4bG2i2Q/UtbWtnGCbTjtK
Tno5/iJbyXBo3tHS25WKNG8wp2S0exqOg2ajLVwvSc30Bs9h5VQZ32kMgshgVtdDMODDGiLCy5Mt
8VBcii4ieLslYBpICTKtr7P7ZySzhV+0rwKJYUlm4NiQH1Gc9VTNv+tnokci5+AQddbKrMRK0JS7
DUez0Tr5G+qnVl2Sf4lNHfQ7OzfV7Ood8Ki8dU8XLmXqO72X5LMqaUjPwHFUXi7p5Ti1A2TeqZ2w
bH33ynRGTkDON6VNhCBkXaJJjGqCWboGWBEOOlLFBZrw1mnskJxIFGBZTcovUUUQ65GcehUZpwfo
OdS4J+AcPUzE08//3BMzYIMlz4542XaNwNowzdxI+X0//+jAURe8GF6VrJnQW0edgxgJejmwrS3d
iALu+0+RTkUs7dsI/YCm8CROCUydMKXbL6LNbeNNt93dYg/AKFRoc57JPYcYeBXj8fDeiZQZUwb8
96CcQaENj0CHMfqy9QnRmMjhG8qwSnvE/GmquYgQPdoAvZNT08KUFtySpIXBfI2hJbkI5efbJ83U
eL4kW1oMP5PtAa7xhGmXY6fOsMbOS9uEnMHjH2oh5mUPaxAqIRAr7WeGB6FG9jRVG3gqSWqeJRHI
SqmtywISYRm9bw8M7BftliIAIi+Kn4x0kV4c/Gyd7VhW4C9fWKZeTGJERzsWxQu3qrmrhV1GzQe7
igwtNT82vtOYSy2UiezsHHWBDfP4GsDDTy1y2iBF1biCyb3He+xh5Ntp3P41YHSW2Repb+6ryYvB
YJ/szDd/HrAPRaCEQqoi7zCuQ7EjCS1YI8IY0K2Jcta9olYCWjKLufh4hChfTWwd5RYW+P3F0hdr
kl+Aaf4y6nhW7Vsp4i1Cc7W4rjcxskWFH8LwqmpEknlnplzmqa++HtUdqIKTO+QaZt3cmReKCg8X
dMclZ6QRgLbVYeetX6rGvS3kCq1YC5N/vk16WLK1yHNeF2a+VYwszhLUjWz7dq2njOTgRXfjaNLf
1I6zGOkOwcnvN4RxybLwAfPC6lqKe1QvbDbL/17gefZBXB7pxqt+eyOr2hzeneYLktxTEeu9qcQT
Bxv5ESlm+wR8Lkl+q7LB5tzISDSWmgoe1Xp1XiV+XDjVexiwWaN4AZdRvh14lPqVHkcLS48tDSRl
rXIaR9vI6kd9nPT69PpaW/tpFrqacWTCaWhYI7p6hw8zvyAjQazNm8cPhQgqXvYAbjjdNdtCJlD+
F1Jn7ToJgJ3F3nV51YdskKzE+3I5g8U2HJgEJlTTTYc3pYdmYkplqk3MZ/7xO8e9Kqr5Neja26EF
71pfA4U1a4MmBC8XPp+t+ZXirLR6W4QigLWLfbWHZrMf4IsbN8c/NjBayRxx7RWSQ85dH6Wf0+36
KieBXagmejvSaPZLVw+6fg8jcIeRKTAdmrE0Xff3ZehNwHD6kaz5i6p+5OYzL7NC/z6xEniTn4RU
GLMOJwvm7TktDhVsfcvz3mYv2hNGM7pcJHkexOV+xLE9OvSVaICHJnGCgt0uvI2aXNkMFabSn3OW
D2SYA/CBleT9+oNSTGTp0Pil+nUmSDixFEol50FIpgVHyvw2NrDXgohIWtdfYgSqxWeX0i0bLk7i
PNTxOCPrPxNgI7LNSwwQesD6MCAyJbof4VtKl1igN6eVQkJNA5RAvt+k8jTiJ8obIcrgfxpmu2ak
MtHsG3i27NNw0JKlhxT7p+jorP85q8WNKr/cPZNbo9LrGAanPQHG/1F+q3W6dtOxtY3oqVkMlyiQ
rxhxTWQv3gXfJO0gca3LwGFLuKN/JoCwrEIS8LsGLGTqw0b0dtGO9jqg1ip7gWStOyYEmGDVdJY3
+8/Ne2TFnMRVmBFmxhhfIpGtVQ3wJIRfonXdGH3Fdw9AI8EV4/GHFJjvV60zZE1Cn3G2dd31LrAl
cLpnRvpavEm3WdVrAG8t88UJAUzdbPQUWVS6w5uHP51FtZKjmjIckfyr66vSWY6NPPf/A8eNBbj1
+fUfuxtS2xHVOA1FAkPYwi19HK3b4j9lyqQ++WQXXyOwGeLKCkD7fHDKSLeA6aBuExXI9dVm1XXQ
DlTdKVS6eC1HFmXi203bj3xBTBBg83nZwKJ903MtRYxTLSJH2DlQokVevrjx4bkI4B876yA31Ev6
+KarkSNqxmD5T1w0BtoL+Ba5P6IV8F84gt9+ueEhqOwCOnJ1Jm8lvq4aO7v8lZq/RxxC48GpnC0v
fkcXgu4EV3I/T4yPbgpv0GdZi2tAALhn1Fcv+lLxZog5Lze9K2qWH+fiKlxYqtCijS9eshZPiz5L
XBBoRPkTRImivn8aTvgUzm9uzqOkFF9DUepzcf6x4WLPDdyIZnfKQRlAe6xVeCR7oX9tJ4hYbRn0
Ht50yjJZNHj6ONpbKTaBdLJI0qhAyQ3vRMHNQ9KZ4fnNd48Pn71kKFdNYF1T6yxhpsR2vt4APIAM
bjWZ81zLA2K2NU4PLX72VG1cbG4bdx+bZkAc+v/8RDKqo4V2C2HV1r5UU2b8T9hN1NlHxsvUnWLl
02WUTTnX23qPGUqAk9vgKG5IgZoCu7kdhVLodxr7KCbflHpP3g4a6Qm06ul0CUE1fX5ImFBD7YYg
GijwV4PXwqcRhU+kD4leJzDxCqeS2oGdvzVC0mLTzBqlT4twsVz3t1CcEBfd6lvvZmyZq8EjprR7
iE2PtAs7MW0YXKUKmdFjuvp07etR5bsdWcjJO2Mfr2dSK+5vs9jiQ56EMR9R2VfrU8P9oSxG9G11
4gOI/Grf8DkrMqm31bt6WJyB9GDbFUew5KF7rryr3PTlvCoq5XgCUvzA54FQBCuW7B373J2+x6ya
oya6DihuH++1yixOXMs1yYliUROXJW2ohTQjVk3Pxf3CgzA5+hKl442omLVovTWMSt5H6y16J+dL
Kdltm97fonA/gB7X09cW4mwmZ7TxErz1dyZX6Iln5hERgnf97rANoKlmMaiuZUkMKdtzK9wKBLqc
FvJY3ncz4sb5BqEgOgNYNWl8hbDG6C6Dmbh5ZjQe5O1OVqqCOQnJCtd/hFMntiTcP4qeXxIbm+d7
B6zd9WnHH0LUKRze/JtKfZxv5gkbO1Y9PYdjnkqeHdUakU31v5ky747eGL9Z6wIE3F8S2+KVEDRr
bwtP2Wm6MECy2/y/XwNwwijJW0QxSlnaikwPhmJS1Iqb1AQqzCq/MtaLhVxxO8KNaOXVpTLP0FxB
h4wXM6BdSk3mq2mJKAM6DLPmYr5FAWzQEUBFtQeyI51xpIZgVXJYaWU2X3TVe3PtGUq4zZqnJoYZ
keS4xWU8S6jApmL5oof6TpiQBGxcAcYiSlQ7CTu0yx2E1aB83xMkdcpdRjaBm4i7mwqOBx2PMiLW
qU8t+7Bj+P2VKSdf52ms7H5vczNHJSId34Lzuxgs2pS6hXJBjb8UYvAuKdT5bF9+OCPDPr2xTIOC
0U/XGdd/StpCXesoJl7i59Cuh+m5UnJ5U9KrnD0oCpS1Ed8E22oc2QpubnqYKcCmuxhYITqEmYHg
mQ4uxwd5orCYN2+6dRsauMLb14KANmorvFvXMM4aZeAr/rYYZzC4kWj2dceYxJu2F9X+5TktdSjm
YFEybP8mmzTSwwP40PxT7gQ6JK+uVEVLlQfI/mLrcSycKg9ZeT5SGayotpWebUNDpkhtsNW8vja3
zOfMVdplankyNSoBsdNp+U4K2rRKPgptVJP2CmvbPQgP9PT+RG/SIE4u0tVSPFK8iB/ZjxaqU+Pw
w6M5QChydaBg8JjiP6Y4Tbtbosp7T/zw/UaACoJVt2aE0Gr9+7mdq6yxrOh9zhV60qF/bw/rC4VD
4rvcrxXjj4JQnz+nLsMhUgGcYT5ppxUBPUYKZa93cRI2d+9ldKXpO/ygZsMAN6+TLVgsZVufRnRw
r1L246tikGZWtMdJP6qkRICxgRCMGM76V7Lvgd9zIA7ZxOSwVyGfzY9EMdtKoJKY/QaV4HrCn70/
1tVYMZDE8xez2mi7tdbG2+ebPxfwvflnTEI2+rH+/lNt+mLtvCJ6uqwVvSPqAphnnPW0Ecbzu/mV
ZRKmazTkxYdRQaeCCI90rhuLSeAAy9gOG91iGHgVP9pQcTL7xAvakMNVa8MMpxR+hwQu1LPODl2T
ElX0/cOrDU9jcPhFY12YO+5rmQavzGtBHQE0dh2t6n0o3oSoMcN3ABfMas9WWuAEPYpa8itBETGT
MZ3E/B0jFsQeKuLEXqP+Ok624xAM8e61u22gf4A1BzMzCjky3fT9LsnLyF2ac9ppfnPtHwD9/CH3
QnW+OGyF8wR2CKpxrKq2VAsXcXr+Md4MCZ3MUhMZjYq+ZdkOdciqAukOWi6Ar88QVaRQrbxC3NeC
Ol73INw8OOK0CKt0u/dz8K1QnDJKnAk3xkNwKz8frS7Ee+lun6OKEixj4dZj047xC5Jq9TdUcOjl
ENRQdQOtykrgN0Vgc1TbFBbzeHwVxrJNu77ksaAZzW6Nhazhdvok61kET8zNZoy+SKDYKziA2L8p
I98Tap9VE6Tx36LM4+YmZRbxZrz25oXoGxcbzEmGVrljMQcvvoY0WaLzCs6JOcz8uXXRsizmqPvQ
dBk54M1oEfY+w6GZYKoHoIaKvYMFIIK+4QSK5WBuJONIAitTdHKahT/o9YE7djMCSjJbcHTqW5xm
jMDBJBvsrAfB9uSQI2ocus+6DellCyTLoeWnRoHPlPTfVlHVTAHWRihoYFfkVxg1e+m7FkbiRX0b
5VjIs2BNm0l6c7BR0iMKZr2e+8f6NuQHxpqW7cjwQohqGAFvkhWTFZEIx8d7u9BwVzK2I0IHhPNJ
LdQCUy1lt08A5gGt6BUqn877dX2N1ABXv1Q2qU8tDyCeKosY06e4AS3t887ReAGrBxGMsN5ygZ/W
OS9j+7w2pHpfTEOCAZlIRrZ5R/QGIeiLeL606WPfHJcdDlge5GEXyjeID8Bb+s1m02m4jfjUArsF
tRrLuoygRNDoEgI5hxf6jgvKmkSD6sv4SzU2syYvVJdF7kC5CguMsQk+peGKTTjbIJ/zktms/Lso
OCCNu6awtUrhpAcgkB1tXczK/6glXo7Ln79Dm/np/1Avtdy9cMxtrbvSIpOr7y39U9yFnyIpmqNM
QhwXPfwhv3uWk2aHNjSIo0rP8VEu7JYgi5T0eFkPA2LeaNc3iN7okZoc5zcqmzp10m3zNvqGFcx9
sR5z5/jLABLJmoQPQEVXYnlqYzVkyNIZVhyjUr3NjBJkhkXYfIv6LzktbCZRHRGh15vUrt/2EJvp
Q3RtbwBWXOXq7Ixb2MsIm1mWxcd6aEfUUEm4EuzqZ4x0EDRz+gvIf6V1Khbdy+4rwRTa/w1UglX9
doBS2M4vuQxKYRpC7AUMWtjiZHcV89xQkCmbFuXNrSGGOkUSQExveWjGFTMOfJTh1RQV8DX7fVrY
bAkpfL8LXyJ1rrdomNyKQ44DKo8eo6eMc1iXMLjpiVsB3NudSqXJZwXFFynPuY0biU/In0iuAHY6
SH9uR8vjx8jFTT+W5JkcKnHxrh102zy8n2xkF8Tk0vrqDJlWhHqAAaPevy5kWv3x3j9SHrjAoxJ2
Kb3Sj5FJXJvBR+IdW8ChoMUfjzTJjg+l77oKKojj8B5Ih78J3DjSWtiTb5h5NJL4zJS+TKT6R5/m
DtMprtn1UqVH49t8Z5qYMGjnVy6waljIApOTWD50FY/G7LJOZx8Esj2/Z0m0vVZvnKIvgniTD3w0
9aK89hNkKPYQ/cDRGF6DyXabseTbEA5/FwKOOGtQKGc/6JNUp2K3w1mk5Q1+PhLomRZdlraTiTQi
ur2cGDVsrQZlfrQsz+vtlM6yo9fF48RlU13WfiKzuhj8G/qPaqeWf1gMsD1cIVpe0fMa2bNInM51
NbJH8OpyF2aaKWDrAM86EtQUuT26XwSmi6r0RLWeT1su4g+7yucawTHDWcYPuuR/rKplUbzhuUAg
+OfskyWKFXIWgdrX1mqdky6FFHF+1yY5RxwrmcaLZG2nqnwQ9UuYYiaJ/kF77bsuNQUZC9MTUfxE
DdQsxMS4MlbJAH8kAqVTB6iME2Nj5xh7b2GMc5OdQe22DfpAVvgz4pq/dlcr5Ng4bU6n91w7BqqX
DtBizy2AqjCaJl4O2fQNFhWTJnRdA3ewx7+pjCTHSAdzoJY0WMpchyBJgP87Ef3f0eNaZpY0ihP9
+VKqybMSkBSn4OtweF7NAsAimXDpE3MW3DZyROLJOLKz4FD7Vs9qZZxnfhUl3j5uVyWCXDuGdl2d
Wan2ASON+jOWb0QU4O2k4SedQlWCjLjlDLoR2soIYvVuwcJxcln7cxRrcF0ciPwpMRmrwvLTMtd6
75LmStQkApFx/MOmIZ7bnhtFs023rtZi8PBuK1TfKlXa0tIjEshgZmeZJdeFvnur8Qukh28fTIwp
oOVUHu9RrH4jXWJJptuy9FVAYzwaZ/02DsQMyOiQnKiRSMOoZXW4fzoVSFL/A4m0bgeEZpqLubHM
Ei8z8YcY5iNh0antrkO3PI3EjiU41V7Dw3BAJalieCO9YmyCMqp+wp85QPbEoNWgxlgK3GCzjsVG
o7WlAk1CFT/rZuI30hmvZSk7KEtlaTAOBpNbLlNJE4Q5FE5jThNxx4rtCBs6HOjtTJ2Kff6Bf/MT
cg18872WA3l/FxBAJAPK04IteacKvhCMZmFrVASZjmPZySFfLPgS8gmbHIw1eBmC0kHIiCia2y3l
tD/mIAPAQ/oWOpn0zKT9iAtfmmWjEivSewbqRfxjJ/HMPFd+2h7f5YTfdgeGkGI5VW52jJGq/rIq
8exGRTqNALr9KT1kmEFO70epUY5LMLSB+2CjYLvUTQMulGOsLJdC/p7dtZaXTR0RuYD5JGB4QISm
ImReh0QzLZONTVI3QerZ6evNlc5PByn5p99Y2jtJgVeRvYKJYPEDwCwXB5RbE9OGuoS3dVYCLXG1
whkkJf+nqJEB8Y9wYzn/KIOj6e8unVZCK9yJ6Xk4opqEosSCBCTRgsR+7ptL0XnzZQDMEWQtSXy9
08+W54jnCZeNd2tWfW2rsgsJIhrGO1A/fe87WjzKcb14+VAPoQQHJxqppggKoq7Nw6Iuqo6w19Wn
aLUr+V4mNYTNzJErCTPHS0XnmZMYjOHWTQLfIxY20V1HGZBcpJTGGPbKGNkkJ+anHBNv1X3ge1fN
shDMV1hMQRSvz02ASFVqSCKyEPpdQkoTu/kbb4Wni4el5gqsZ3n8DSshLszgk4p1LpnuSgnJgBOP
bCUqGOguw2hVgj/006reTm6nHnEVMV2dsleKspac/EAB3XSNMOENMloQoVINlSQoMCN4Cbt6OorJ
tmszl9hJNd45Dyymx/J9rfCpTbB5RPM/M0icGFPRmE7aCS0MqhRbtGNsJtp2JawrGKngUZ1BWpPP
oyJ7TO9dJ03ustKO0T6p6xRZXoNmaZfiHmC6E31CV7HTny8P8fhWm7ymvt3Ev+5nNu9/iHFXIg5s
G+VqyeagN6xLceATXAE5Rdl2WS6oU4UcWwchCKmLWlhGVkVX1J2pqL2d1xWvxlhyCDDufycR190/
QyN/tsaW6rr3tn66BYU2x/DXIV6+gw889A0T1hj9st3hvICpN4hGaSaV5hvuqEv2Zi9csOn+BgGc
W9B1NaL8eMrewFMoGIEpAiG56FLnqVHyJ1IF4TNCRfgTBTky8nIftKkI33E2HsG5uHlln3Ikw4sw
eoFspIqz8A7f69i0jHeV4tZgzKG4XFGRNwYZxnmgfSqVcQ55vSSzxb5/gj15p+84Z8rcLKFT4dJ+
ZrTFudKEMAChi4zaMa904eVAOiNytc2hvrWakXo5dj5ddOhwXEvlHXU56gBrOU6FstC2JLQHkBi7
vIV3Asa3PpqKqACosBnaOJ0iR7ZDb7xx+Y0iAWHibFqRP4pj/znGtAjm58Kdnb7Dvjt8kKu8IT33
qe3I8G2MI2sBKzDcVZJrZxsh8jXdUPMsvsoi1zdddTCr/EBUQKHogoW2RLRwMDwJPVtLA+oAbLan
vGwfrxNuTuKRKhQg9t7iAUsrQuMLY3lgwBJrXc5g/RRrpkj2iUmhIbhLrN5oqsKEKYbzoagP6ui4
mi1Ur2ZB3OhsqoNlJF2FsrRWM7xUWgc8wrCnTtJ8YFxWneadUg3TfUKhI40nsyQdVZg9ipWGWzu/
B0lSca37/xBNYTohClEzmfA6eGPbAPxmV+C/VggqP8mFQe+UkHcnuO7iEn/Qn6Fcm4a8VzhUqGk7
CJy+S//xlMMAm5lylAihwEPwONPU79P8fFt4tXo5GdNP+vgLWtEwn765+pxGQ21k9Mo2M8rIiSu+
M9j81dI7kde6EHPLkkkSiKVDmSPJ/Wv7qnqXCO1efezPu2b8LMb2gEOkO5oN08ridCHAQN7YdDw7
e49Owdek75VCzmdF3rpuSfo+O7ZHu+Ocr8QdnR5AJw1hFPqtyPmSiD9Nbr+18KJRJa6Jm3JMIWR0
0QzjwEa28m227T7qpnX3NbOBnetWn1Rpq/dAF6isvxQ+EyLSLfkbpK1N0TNYGbNMnjv0G9Dvt+6F
8400hQsmu89cE0YS52/X1kw8tdczd8qVDmRW+E4s2ECSEQzLRCNKuah7U4q2UhxFGhsG2EJ9Oifw
MQsF1964Ek0YwWpAOze0xVw237C8vfoRnkby0hL3d1gg1sSsYdbDGxFZM5C7WJ46xTQ1C5nQpDb6
SImh92r6A2hhYV9vw7Z7tUk7Op6eKXJA+Fh8YkT2DLpOJj7JZ1ZFcixeU24QZkhAwRi6qbhBguoC
acWsqTJXNgoLhHLq35esOXJyQGbwvZHxvixMe9FJtpoA9grQUNwD8aOThQPDkOTND0iS05hKyfnu
/5s5pZSw2LK7Cw4ZmstsCit4enIzeH0RaZVdcdCMcxMk+XWbKP8FtRejwwjcGgMxBDlDtwY1zYKp
jc6PYKmQHi1EM88Nb6lfVz0I4UQg5xWldzQFSkZ95KSknysfhY7nerWYGl+HIbz+YWO3bhxl+EW3
QhKjVgBmtj1m06SOuzbqMe3rp/ywYOrIbMqMr4nqxTUtY8bTxs3uFW9t0KKwpnUt0LYUcApM9TKn
2ghrMYtZda/XyZ97FgOoJUptjfRAlASmWGJjQTdgI+tayyJOF4RO55EfmXh6/khP/tC4+zITrYyN
UkA218aQQJRvhROTadfbBFtE+dtelEXWx/IeuMbk01Wj99GPPH/DKwAyS4E2Bu4sTuvkM3Vbs9BR
vCs8bpxWSl2Ib/Ho/dxwl8YYUbx57WujfCvFlvWKg5GTILDDVh9K7OZCo14An3PLQD7LAuS+nEKk
F7559279jSE6TU8c1KkV26hW6Kry0MWirGll+b3j4A+mAPGLLUGvZkx4dXilkGlLXtiKAT+Oo/yF
Mqa6b2H5j/8ma5jOEbuVnwglcQDvHcl0MM/2aoMEW6em8G4pooo4Bc8ny7NJU2AQH/EPo+udg4aX
ldAblFYqcMvrTT1eRUGoKvOHwarjNUnQYLK0MriP0IhiWx/klPq/BfJVkW3ErUO8sXxgb+667EXF
Tjlju/an5UkweBM9Wi8JMPgH5JdIWSlk9nXdJIyEXn92u/4Qu9deIqd6IUa5BYjKkY6M4Tq9dL/D
ccb9pH6mrWnczhgp91iLquqpzd8EepuiJv4a0dP3uDfoWKTrl4RZZJbyWuDxPR0LeunyUrdzGe0x
3G8e7z4M0X7X41qoInttJ6jUrJAB2kwOm8/uTmrp7SQeSC++m1lK6rOg2RcGuUiLX1oumRICdCzv
GWOBjkxfzFxbWBHfU/bmISvqk+MoZHoSkn37loOn1dwK2vXTVkFNWjWR5FLTfG6kM/GosD45PqdM
GJ6W8ZxTesYOeViywA71LL+0D4oAqqUq5BP/yh9+0TQrw14jKBsUDzSugdUdbOaIDu8Ngx2Y1BOy
kTqmUu90zScUl57Uk5/61xrIwBheyQXTCfeofxmHeTATqvagZfgMHDTC254uUAAyrox+BbjZnYmB
LzNfYWV1Nmp40ULD2bTwDJ2Kyvg7kfar0qLax2u9FmAJYkMrZOVlU5fTIzglD0/iERw/DwrFKPG/
aylDDtxmL/hOg159BPPRuh0RnioePB03K/v6puWFnJbZti7gEL3A+PKhClScrmzm4fsJ+ejN7Vq4
Azq9MG+c9vw2HBKPSTUto2/FQ1AmIhiIsOCeaGUGwHvnRLF2WygmshfnY4ryzCPfrSS9K3sTT2og
9eGr4M84MXQzBFzyzshUVeM0rYVTzDyMUiRu99WRqGkXiYawz0aK9q0NVVcVShNEJZX4aOzrpqfx
SCQB7LC6hoXIOdXuWQAL4Ii+xm2IUarvZeibzK64eE/3x7kku3qupXiESWKile1MGzinBVVMjdIF
osdXfVDvfzqg24KlWhGkezZBP1qmlr2Mi0OCaCY9IelVIAa8VsOMX1S2xf89L5D/99UrvkPoAS34
BsdpkAE+1y+ZhZTFRGjIwU7LqcZl9lbF7BwDhW5i0aUvo18XOSB5RFI996Ntb1cD5V4L8Lso+6VW
kpdV4oWHKR0OEbZlGvVWPpzxMpsw6Ja9fVj8t09ZIHQuILjfGS3gvAVFTWx2185NXzVtRlM5Qc1V
m6othHFhvOrzfB5SSBgn0dz7JU7OwiSq6z55p7B/+kdkPh1wjQGOGn7M2JcI5p9Lf25t65bDd5cj
TK1xMu+fJQ3RHqBMpePCiegVjpEwhCaCsHzzCRqi4T5UCt7heLtf1rklIuDxiOpoZNgNaDy6YejY
2vy3rsNkYMxVepvqisJoTffjL64hN+QSSMqVUm81OdYKmGQWbIh+JlYmWb+yB1RSl1tQXcYDPRQO
GWpx6pztVJZItdcPXPN3UBF/0dYyhckfGz9kXGqBP15NpP38RPgtBCS2iuSGOEFEnf2Qh/4DYP/Q
p2XA3tlACFsFg1h7UHa1rSj8DlM07m/iJfVUQON/2KiWtEkwvH25JYnkFdeTfmk9emOTdFA0UkXA
OsOI0KYR7K63N/n/gnnaUuC+YwCrcUlex59UW8T7tYwCGA8Nfq7SRv6l4F/QnH1a6KDpl1HLEnGj
T9Bsm3L8K7gNYsaPq6uZ/TAPVOOG1biiE4l7pHKRAwB6gSh2A1QP+vxj6QKp/UsqnycUcty91IdF
oC7he8z0Ww6eyyk4w4GIBIO5pP06SCBron7itJybuzJiyLdn6tgI0aHtqCz0i3Gxl8elIOiYJWTt
tFxTg4Jtm6uIOz8iyVTzErmqb6j2z0ekeZr4jOuygpX5G6O1zyHVFbT5grk30cznxUJDuerW1E7M
oV3D/j1swOVRKctlbYdXr9gESHwdUymndsYitZHOcYZ60ZukaeNsIPmi53HMhvUn1zuRwljcaS61
3UwMRBI9OzkTkwOTuuO9Qcxl0WCJYKitt6S6wOnFYqZDK5voCE/8WLqQOJmH0lwIiQHhuILbnLnt
mpWRjm3zd6cMh6HCI0NE0x8GoIimwMqEYuoqEupWYd2g0pD5C9q9fxRrGc2GTqdudT+4jl9ntwen
OGU8oI67L/DTtnmhmHJuncpWM/Nph6DYXJc8/U3Jbmn3Iezb/w4JD7q23QYwDY+HX/MSphdaCtJx
iIOCbv1awF8QbrktyCJgv9W7x4Xz1TthA7FLi21vuevcmXQtYQlRwuzlARFGTrpe3Mniol7XbFD4
uflEUX3q2g7rkdImHczDsSYAWC0f8ruFjyG4le/Cmyz3qMqeYzPTuuAsE/8Il6AlB6glB7l2TSKu
kzaE2yhT+jQQk7RJzP/maD5LLMyxMJGi9a24qG5mMb8WyYmpye7w+cyTv26lQsmzka5+mA0NYVia
ZTWk/CqlO5oWY79XGLw0FS7mnu+LlxHEoBRYE+XoymRK82jkX1zGPqnFO5irgikpe0kKe7S6aZjd
QGMdYxruwEPYBb5TnT3UOIBZ0acvbkYtuxUQuXWrcTLBvNNgx58juNnChOr/Xio0S2y/H/bGSx5V
fSlltbixOQfSEqnQ4c7wLPcOiUbkQXWFS+43mwLpTVp87BELvoACOoZ1KlxbOz/+sODqDcoNGxTb
Ex6OBiimq43TvqEFhlMR+oHy4QWMmbPrbFUXwgt4fdhe7Ny4nRS4/jrOQf3QlR8ho7VWZjAWlIIT
2VpuuB/W2iMPSACEhtH976GQwDZjexKxgA/9RqrZbjZSkN7UEmzZKpoGaUCICUHFq7UuKK7y1n/g
bXMQmtgCFfP/DL8+BacLO4YUvVylXvIUHtQ1ZtwGeRrBvmBobMVWpuFeqZNjwXQNdfuom+kShob2
GzBBx23KHBFvBb0jXozzJ581LI83uGfQkGmDmLrl4PvE58So6ixJ1ADXupPFP6ok/aVjVEOSsF33
LX00HzpwfDqx1apiXM56ba9Uj8c7uNaa46Zs3ZNxxRpSz14Z1XQxpNcwaTtibIAWVwRt3sxgjuis
QJOHnboe+E2d8vEMgIh/gcfu0RPifm1+EUVU0FJ+EIdV+4d6wQ9u8NPxH4KOvxiKMvUviRmky2KS
Q6EOlHQit91lmgTsDZTdsgmht1F/n/g1y2oKdYIF9dkkjZ+8ORnMDAZ2jbisuTcC4lH165thcC2Q
PW8KjWP4tTZgaxhl22qL6ILJCFS7jLXP/pSMIehhT7J8gxvTblZbaB1p4sUkyaL+svCMyAfhkxUh
xFo/FwuCdn2tiBoQ1dMpHJgldrW0y1M8l/bH/PXao1868iB9r5KZbw61j9V1uEgPheKgX9zCX/gE
zNX820tT1q/WTy9HIskAEgWQZhHhay5xW+CS61K8XixCsnR8NEsTeDmVwm83LrOCxIXD60vpb0m6
Tdi08hwKbcQiaFYfXkbBFY3vO/XYFm5H+nAwjSRdpTdTtYOH46pR61blwmzAzwguDeXO2SJiygRR
84RYLe1YXw8x1IXMGLvZtjEvCDlTK3yckPLkY9SnaT64PagDJVkNNEpWZPnyqAR5Ik+5isaoSfw6
0NIOKbV4VtfU3wEP8qMlFMIb+htG7Ka3vZ9cd1U7s7FZJ8SdPzwyT02/eL7+QVr5Do6/KYezQiLd
6gUtmfm/u6ft8/t4bO6i1bUBw5Y9y4piBLmZccdguouUsY1Aw1KHXVlS656NOzb8Z5x+/FlwNnif
cDlnKnxcAYSVpEXJsQGkhh8QKzikwbFeNmNqTudtj6tozGc50qIL9THWHb7pWmJtRdt+uTItJfK8
sphO9o93wmY+8ctkrdLqlSHOHA8P+ZEGcbOAcQkv3XsFZFGAyRnaMfhipI7IozxQb+L54QSu1TfI
JhtDhDQ+Kcd5rcTQoMoma1RehNuChc9v1F0teb/bvIi0wdH9PvvdCgTrpYtQdME5sZ83l8W4x59H
soeyo0eyt/89xEvtzrwCyC1vzHUB4NkP/09nTbXL4OTJQKvkht5QTEDqS/Wod9ssqo73oBeR5ruw
SSaPbh81PX4O0KQ1Lkhgs3KasfVBYV2SLj/iLVVCDwRCMQScX2GszwPMPofK+NaVmsqpqMDUijE1
IB5kmtKA4hnbiQMdIyi4zlTOuNm6CW/cLwdiWzeOX7/gGKcIbQzZh+4HPfZGT+oaIL4oxolzMbin
pprtrvmFes9oReP9ObIof8pHiK0czAKzIJB9c5WqGqcjVPni8Pm18fdMCA0RqYQvPxxFK0oGN6H8
V+ym5Zoy6NO9iXk8IcM2ortnqFP8j6cm+TulivtuFoafqEsEI5VqvMi1/m2TLcOVFFF3U5fWIN9s
OFj3sCcxQf9LYamBmT6lEURSsGLDv2Bx1ZcbfvJXr0Ltd019qCwAAn6an6+qybzogSb4gUTmKb5w
/zL3ogFCwMtqreXoox4fKUMB7P3cuPeNAafvPSL48p6NmihpCZ6OvtR0VpCr+jhB47RbxYplBfcg
boFNnpWt/hQg0ATQxP41SOIW3mNZn57GslsUo7nzAss25SJdS38SAbfj3VaWJscMOJ5CifMlSVHB
4h4uWGi6Pwyd0HBnn+sAevWojJDyC41r1zteGDikPOAsg7hYqpJyOkGlNFMWPzAo1enKix6qtDNW
RIOkTADhsyjywAzjNbIxEcbWa7UW9YAYenh9LJyDd9vXDYNAZmCTDvs2y6dVqzPKgSrW4yYHt8wI
YCnXXd688gIcQa4iZIZI0LeMAGg64gyBsidLsaRrnX1fGdL8d0WlSjPQbQcEMrBxMtg9CfwEXKJw
h2wyyb3eYq9dLJ38vtYJqwhgRTX382fRC4DW5++x42uZpPpMA+0U6jkWWkS/iFfhIiqRRYOAtkmM
/KgypCo15vVI8oEN8CijuRyaqtiIuSbHJIyN3POGrZz5zrytZVG14wv9sQsC4M8F5P0OCLg0wZXW
zNfiq8CINSQtWlumB796ubx39xuuL5iubsZufO7+gukSFt24OOZglkfp+pRhy7You0p2F4OPcG9M
7Q/ad9kMG21yWD/5V/nxBdXtkSPcHmqfSFhJjVQ0hB/8XqIrfst76CRBslhqJm0ut6Y3TfqHmCin
xT3/ryLhb4b4TUE8wK1/ouRCtFQnd8HLyKB0+xWFP5SaIsiN6W2A7c/umm/bWpqkEkj7JHPaOnEb
cMPqocuKf3bllghzB7+75WbfOKLnzB80nnql7qQshBMfreZEsjOZNk/0yW/kYAXuDmJPoqFMy+aA
mp6L8hE3AZno6SScJU79IB7aGsjEjro8Hp/HeIEmg1D2RHMUCinjIHimwRTwQXs19In7FNzPofSP
hFRb8FFzy1Z5KptH7vaj1TkOHHLcoQZOHtivUq82LjIsyVag1Up0ZbjVxTSQDHoI/UBg+l/V33+J
9s9AH+NxywJUHHdaogEZACkvEnX/phWY4BPM12JByBUUAQQyUJvpYNjmLj39DKop3CsuVyo/iRZg
STWBbQQbm7F1NEnyB7IQBSIiqhZ34IK24AgsbxMoRoVd/0v6xb1rOoLOJV9FLl+d52KrSgFgXhoI
Dee5KRXhtmTLV6JJDtOPr+9mBVjTCtM2KmC6YRROHO95Kla8NVQBF8r4n7OR5Ob1ysTuPo5UkLi4
TdSRzPUXRpFWsreELjyaN0RiBbv6GLyFlGCRCwOsKtRbZRBIknv5HrlQ2Lwe7EQqJYdiGrFFoZ0+
yLNwv2P0EErchWvwE4+ZX6ty89DNG2wPfUgbyC5DlQfO4dp0Lu0nGGaYdGqz79XeK9ShzTouh73R
+447jNH8nvEcss8uahO0QAFXp6PTAjrWTtquJzMZu1uuE8v78en5+WKCXL51pkxhb7zW/S8gqXfE
ShIAwHFyCR+xdV3eh5S9abN/t0p42EPHg/OK59LbxeZsnKb1bhaDFf1u5whPicTJGuOfJwHdwa+s
CgxoPJA+Wik6VoPAXnnDtWC/quJ5LMdlspSJbDiC9ygfFe1kJG/4f9z4gznY+P6byPLdbB5pYGy+
F/lNhX32P/8CSmWVVo8CHDaHLgsog+vZwpJaXbIYIyFf68xoDh82hXpRQztUztLKPccmVu3YSm9+
IDzlqySTifazsZER6gL+d4B8kMXjaN4X1wXOgjbPXELbi0ka+WwQQT7+H5WSUowJCTjZcnl/hFVz
O/HPceyKc3YO9M20WeeLqlhi1UfARDQmhP31IZ6Tphgay8dPYzuJE2q8qu9cioSVtxdWtt2raFmG
nu6s/2ArJyEnTm7OwlHqtYaNk9k2yZTvxZcuUzxh2R3HrTMlp4iOGVHGN668xYqv6d8EX/DEUe4+
V3f6kNNNGvWXPR1uowurZo9c09tTAVop7M3oYLHHgOWpRD9bpEZFbm9NBoq/nCzc+C0i5sb1uxXV
PaLk0lBlU6yoh88Rsd/8xJdhuXYf55QdRhWlOZBX5KyiL6lViEv3bFaeDDOMRol6TrKz9zaZZc8j
vUt5ZQtEMk2dKW1RqMc4BSsIdjejcZuYzN3NeAVodhLubFvlYYK4XoyNIADa1tsg3jyNSXKJj+Bb
aQeZ8ptCy8lc9gqIPwFmSdxecMJqrlcFxKQtkUTyuEWwlbnCO53Kfr0Zn/T1KxTQr9rrQqVGXUfj
N6dJ6LIOIDcNdwoC8hWjPix7UnbWrf6uQiGNQJt7dqrTuNcrg0fhe45RKL02Dc2R+eSuIiNFc4Bx
XNZC2sFqnw9v4cBGempfvE8OkWDpxKeWlqN1g4Nl4ud9iXNKpxOx0Xw9+PIvK8r9Mw+t8T7QACSu
yN2ptbzSnRuFZECgaZmzWaTPmyWmuzHs5YA40XgBWJNdn0K6WXvbiaS2obXulxZOksRFAQODlIEG
sTAYEnbEmkG2y6Nb+OtFJ6dvWd2gGfS+Vd/9uCpTZazX6+7gYj+gmpfKzUXqAihALBTEuh8cjkxI
nNZdVw0Jifh6Mu/b21azkj3C3nFLjM5UFRoDaqu7jAPghwWyeK65QMcLaUSBaNDRO0ktrSSLlL5y
AF98mXDILG+V3iH6k8kyW/BM5VvLc8JJX/xsyKgcb+/aiMAqUC0AwHrGcTgOcEX9LJfRn7KaKoE2
JycOLBmHZa6hxW7CWuoJB0TmH0i1Gz2YE5GNVOQPfcMy6GftMCBI1LOIWgV2VYyk3pfLF0p8y97t
ARy7nG36KYa0YBssKsXa2ezx+gpQHKNeFRkxu/L1WnsCJRTQSpafeRAd1NudsG1hiM8IN6v2JnRj
5pMnAP7K1E2m6QYfBFDdqJnZ/EcyF+DfkCA+KJALOVRgpU2KMQu8qTavOTVPpBVY9xHcxsXwslMI
9DLMpKJDMvmiaof/KEGrNuGH9eVdsre5w0taCcbiXWs5rKh69D3BLJzKk6/jLvDJO4uG/wuL34t8
oU55f32g/0zrHNDn4LiWpFwC1K/flwdoIUdaWG5brGN3MldQy9wJqvKrg1P7m4osG+uBJJcqTATI
2Rz21Nrwp+5CQb2gy0Mx6Ggu9FTF3rG4tLwj+Eil0FWnU+sEodcOlL/3O7cOZlaHYhl6pXAhaesU
KEZD5b1OFxQ1lmeCGSDS6zT1OVMo7JpwsqD6Gp0vV0Yf1x9zLQpg7sQe7ISCSA3TulhPRhW9Xf/y
19U6uELErbWdtqiznsjJO45cx2KdITAdoIrUqWsICnFqIbk2iT6vDupixWzBV/oJ6qltnHfn8ivC
IEeKAxKaVIjdDgMTOn2zWwrKYPHOBIYE5lYGqpkkS4ik16ki5ovRlH0eRelCNr+guvviHqv1LWZb
YHAfI4zBC1I1sC4jCsOG+dqFVgnwnjjKT86KOgLoPC6S128z0BLX2CkgDJ09Uttw6hqoOCZ8t6lo
FnZM5cKjpw+e87k6YzqaRXqlO9rYR95JnKzq+pIGuDxngfvCXMMmzUzQwhhWjw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
