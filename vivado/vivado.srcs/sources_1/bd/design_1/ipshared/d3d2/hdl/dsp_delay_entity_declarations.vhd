-------------------------------------------------------------------
-- System Generator version 2018.2 VHDL source file.
--
-- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_f6cb58e566 is
  port (
    op : out std_logic_vector((18 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_f6cb58e566;
architecture behavior of sysgen_constant_f6cb58e566
is
begin
  op <= "011111111111111111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

--$Header: /devl/xcs/repo/env/Jobs/sysgen/src/xbs/blocks/xlconvert/hdl/xlconvert.vhd,v 1.1 2004/11/22 00:17:30 rosty Exp $
---------------------------------------------------------------------
--
--  Filename      : xlconvert.vhd
--
--  Description   : VHDL description of a fixed point converter block that
--                  converts the input to a new output type.

--
---------------------------------------------------------------------


---------------------------------------------------------------------
--
--  Entity        : xlconvert
--
--  Architecture  : behavior
--
--  Description   : Top level VHDL description of fixed point conver block.
--
---------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity convert_func_call_dsp_delay_xlconvert is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        result : out std_logic_vector (dout_width-1 downto 0));
end convert_func_call_dsp_delay_xlconvert ;

architecture behavior of convert_func_call_dsp_delay_xlconvert is
begin
    -- Convert to output type and do saturation arith.
    result <= convert_type(din, din_width, din_bin_pt, din_arith,
                           dout_width, dout_bin_pt, dout_arith,
                           quantization, overflow);
end behavior;


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity dsp_delay_xlconvert  is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        en_width     : integer := 1;
        en_bin_pt    : integer := 0;
        en_arith     : integer := xlUnsigned;
        bool_conversion : integer :=0;           -- if one, convert ufix_1_0 to
                                                 -- bool
        latency      : integer := 0;             -- Ouput delay clk cycles
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        en  : in std_logic_vector (en_width-1 downto 0);
        ce  : in std_logic;
        clr : in std_logic;
        clk : in std_logic;
        dout : out std_logic_vector (dout_width-1 downto 0));

end dsp_delay_xlconvert ;

architecture behavior of dsp_delay_xlconvert  is

    component synth_reg
        generic (width       : integer;
                 latency     : integer);
        port (i       : in std_logic_vector(width-1 downto 0);
              ce      : in std_logic;
              clr     : in std_logic;
              clk     : in std_logic;
              o       : out std_logic_vector(width-1 downto 0));
    end component;

    component convert_func_call_dsp_delay_xlconvert 
        generic (
            din_width    : integer := 16;            -- Width of input
            din_bin_pt   : integer := 4;             -- Binary point of input
            din_arith    : integer := xlUnsigned;    -- Type of arith of input
            dout_width   : integer := 8;             -- Width of output
            dout_bin_pt  : integer := 2;             -- Binary point of output
            dout_arith   : integer := xlUnsigned;    -- Type of arith of output
            quantization : integer := xlTruncate;    -- xlRound or xlTruncate
            overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
        port (
            din : in std_logic_vector (din_width-1 downto 0);
            result : out std_logic_vector (dout_width-1 downto 0));
    end component;


    -- synthesis translate_off
--    signal real_din, real_dout : real;    -- For debugging info ports
    -- synthesis translate_on
    signal result : std_logic_vector(dout_width-1 downto 0);
    signal internal_ce : std_logic;

begin

    -- Debugging info for internal full precision variables
    -- synthesis translate_off
--     real_din <= to_real(din, din_bin_pt, din_arith);
--     real_dout <= to_real(dout, dout_bin_pt, dout_arith);
    -- synthesis translate_on

    internal_ce <= ce and en(0);

    bool_conversion_generate : if (bool_conversion = 1)
    generate
      result <= din;
    end generate; --bool_conversion_generate

    std_conversion_generate : if (bool_conversion = 0)
    generate
      -- Workaround for XST bug
      convert : convert_func_call_dsp_delay_xlconvert 
        generic map (
          din_width   => din_width,
          din_bin_pt  => din_bin_pt,
          din_arith   => din_arith,
          dout_width  => dout_width,
          dout_bin_pt => dout_bin_pt,
          dout_arith  => dout_arith,
          quantization => quantization,
          overflow     => overflow)
        port map (
          din => din,
          result => result);
    end generate; --std_conversion_generate

    latency_test : if (latency > 0) generate
        reg : synth_reg
            generic map (
              width => dout_width,
              latency => latency
            )
            port map (
              i => result,
              ce => internal_ce,
              clr => clr,
              clk => clk,
              o => dout
            );
    end generate;

    latency0 : if (latency = 0)
    generate
        dout <= result;
    end generate latency0;

end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


-------------------------------------------------------------------
-- System Generator version 8.1.01 VHDL source file.
--
-- Copyright(C) 2006 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2006 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
entity generatePowerEfficientEnable_dsp48e2 is
  generic (
    use_reg : integer := 0
  );
  port (
    cereg : in std_logic;
    ce : in std_logic;
    en : in std_logic;
    internal_cereg : out std_logic
  );
end  generatePowerEfficientEnable_dsp48e2;
architecture structural of generatePowerEfficientEnable_dsp48e2 is
begin
using_reg : if (use_reg = 1)
generate
  internal_cereg <= cereg and ce and en;
end generate;
not_using_reg : if (use_reg /= 1)
generate
  internal_cereg <= '0';
end generate;
end structural;

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

-- synthesis translate_off
library unisim;
use unisim.vcomponents.all;
-- synthesis translate_on
-- synthesis translate_off
--library simprim;
--use simprim.VPACKAGE.all;
-- synthesis translate_on
entity dsp_delay_xldsp48e2  is
  generic (
        carryout_width  : integer               := 1;
        alumodereg      : integer               := 1;
        areg            : integer               := 1;
        use_c_port      : integer               := 1;
        use_op          : integer               := 0;
        autoreset_pattern_detect                : string  :=  "NO_RESET";
        a_input         : string                := "DIRECT";
        acascreg        : integer               := 0;
        adreg           : integer               := 1;
        bcascreg        : integer               := 1;
        breg            : integer               := 1;
        b_input         : string                := "DIRECT";
        carryinreg      : integer               := 1;
        carryinselreg   : integer               := 1;
        creg            : integer               := 1;
        dreg            : integer               := 1;
        inmodereg       : integer               := 1;
        mask            : std_logic_vector      := X"3FFFFFFFFFFF";
        mreg            : integer               := 1;
        opmodereg       : integer               := 1;
        pattern         : std_logic_vector      := X"000000000000";
        preg            : integer               := 1;
        sel_mask        : string                := "MASK";
        sel_pattern     : string                := "PATTERN";
        use_mult        : string                := "MULTIPLY";
        use_pattern_detect : string             := "NO_PATDET";
        use_simd        : string                := "ONE48";
        amultsel        : string                := "A";
        bmultsel        : string                := "B";
        rnd             : std_logic_vector      := X"000000000000";
        xorsimd         : string                := "XOR12";
        use_widexor     : string                := "FALSE";
        preaddinsel     : string                := "A";
        autoreset_priority : string             := "RESET";
        is_alumode_inverted : std_logic_vector (3 downto 0)  := "0000";
        is_carryin_inverted : bit               := '0';
        is_clk_inverted     : bit               := '0';
        is_inmode_inverted  : std_logic_vector (4 downto 0)  := "00000";
        is_opmode_inverted  : std_logic_vector (8 downto 0)  := "000000000";
        is_rstallcarryin_inverted :bit          := '0';
        is_rstalumode_inverted    : bit         := '0';
        is_rsta_inverted          : bit         := '0';
        is_rstb_inverted          : bit         := '0';
        is_rstctrl_inverted       : bit         := '0';
        is_rstc_inverted          : bit         := '0';
        is_rstd_inverted          : bit         := '0';
        is_rstinmode_inverted     : bit         := '0';
        is_rstm_inverted          : bit         := '0';
        is_rstp_inverted          : bit         := '0'
        );
 port (
        acout              : out std_logic_vector(29 downto 0);
        bcout              : out std_logic_vector(17 downto 0);
        carrycascout       : out std_logic_vector(0 downto 0);
        carryout           : out std_logic_vector(carryout_width-1 downto 0);
        multsignout        : out std_logic_vector(0 downto 0);
        overflow           : out std_logic_vector(0 downto 0);
        p                  : out std_logic_vector(47 downto 0);
        patternbdetect     : out std_logic_vector(0 downto 0);
        patterndetect      : out std_logic_vector(0 downto 0);
        pcout              : out std_logic_vector(47 downto 0);
        xorout             : out std_logic_vector(7 downto 0);
        underflow          : out std_logic_vector(0 downto 0);
        a                  : in  std_logic_vector(29 downto 0) := (others => '0');
        acin               : in  std_logic_vector(29 downto 0) := (others => '0');
        alumode            : in  std_logic_vector(3 downto 0)  := (others => '0');
        b                  : in  std_logic_vector(17 downto 0) := (others => '0');
        bcin               : in  std_logic_vector(17 downto 0) := (others => '0');
        c                  : in  std_logic_vector(47 downto 0) := (others => '0');
        carrycascin        : in  std_logic_vector(0 downto 0) := (others => '0');
        carryin            : in  std_logic_vector(0 downto 0) := (others => '0');
        carryinsel         : in  std_logic_vector(2 downto 0) := (others => '0');
        cea1               : in  std_logic_vector(0 downto 0) := (others => '1');
        cea2               : in  std_logic_vector(0 downto 0) := (others => '1');
        cead               : in  std_logic_vector(0 downto 0) := (others => '1');
        cealumode          : in  std_logic_vector(0 downto 0) := (others => '1');
        ceb1               : in  std_logic_vector(0 downto 0) := (others => '1');
        ceb2               : in  std_logic_vector(0 downto 0) := (others => '1');
        cec                : in  std_logic_vector(0 downto 0) := (others => '1');
        cecarryin          : in  std_logic_vector(0 downto 0) := (others => '1');
        cectrl             : in  std_logic_vector(0 downto 0) := (others => '1');
        ced                : in  std_logic_vector(0 downto 0) := (others => '1');
        ceinmode           : in  std_logic_vector(0 downto 0) := (others => '1');
        cem                : in  std_logic_vector(0 downto 0) := (others => '1');
        cemultcarryin      : in  std_logic_vector(0 downto 0) := (others => '1');
        cep                : in  std_logic_vector(0 downto 0) := (others => '1');
        multsignin         : in  std_logic_vector(0 downto 0) := (others => '0');
        opmode             : in  std_logic_vector(8 downto 0) := (others => '0');
        pcin               : in  std_logic_vector(47 downto 0) := (others => '0');
        rsta               : in  std_logic_vector(0 downto 0) := (others => '0');
        rstcarryin         : in  std_logic_vector(0 downto 0) := (others => '0');
        rstalumode         : in  std_logic_vector(0 downto 0) := (others => '0');
        rstb               : in  std_logic_vector(0 downto 0) := (others => '0');
        rstc               : in  std_logic_vector(0 downto 0) := (others => '0');
        rstctrl            : in  std_logic_vector(0 downto 0) := (others => '0');
        rstd               : in  std_logic_vector(0 downto 0) := (others => '0');
        rstinmode          : in  std_logic_vector(0 downto 0) := (others => '0');
        rstm               : in  std_logic_vector(0 downto 0) := (others => '0');
        rstp               : in  std_logic_vector(0 downto 0) := (others => '0');
        op                 : in  std_logic_vector(21 downto 0) := (others => '0');
        clk                : in  std_ulogic;
        d                  : in  std_logic_vector(26 downto 0) := (others => '0');
        inmode             : in  std_logic_vector(4 downto 0) := (others => '0');
        en                 : in  std_logic_vector(0 downto 0) := (others => '1');
        rst                : in  std_logic_vector(0 downto 0) := (others => '0');
        ce                 : in  std_logic
      );
end dsp_delay_xldsp48e2 ;
architecture behavior of dsp_delay_xldsp48e2 is
component DSP48E2
 generic(
        ACASCREG        : integer;
        ADREG           : integer;
        ALUMODEREG      : integer;
        AREG            : integer;
        AUTORESET_PATDET : string;
        A_INPUT         : string;
        BCASCREG        : integer;
        BREG            : integer;
        B_INPUT         : string;
        CARRYINREG      : integer;
        CARRYINSELREG   : integer;
        CREG            : integer;
        DREG            : integer := 1;
        INMODEREG       : integer := 1;
        MASK            : std_logic_vector;
        MREG            : integer;
        OPMODEREG       : integer;
        PATTERN         : std_logic_vector;
        PREG            : integer;
        SEL_MASK        : string;
        SEL_PATTERN     : string;
        USE_MULT        : string;
        USE_PATTERN_DETECT : string;
        USE_SIMD        : string;
        PREADDINSEL     : string;
        RND             : std_logic_vector;
        USE_WIDEXOR     : string;
        XORSIMD         : string;
        AMULTSEL        : string;
        BMULTSEL        : string;
        AUTORESET_PRIORITY : string;
        IS_ALUMODE_INVERTED : std_logic_vector;
        IS_CARRYIN_INVERTED : bit;
        IS_CLK_INVERTED : bit;
        IS_INMODE_INVERTED : std_logic_vector;
        IS_OPMODE_INVERTED : std_logic_vector;
        IS_RSTALLCARRYIN_INVERTED : bit;
        IS_RSTALUMODE_INVERTED : bit;
        IS_RSTA_INVERTED : bit;
        IS_RSTB_INVERTED : bit;
        IS_RSTCTRL_INVERTED : bit;
        IS_RSTC_INVERTED  : bit;
        IS_RSTD_INVERTED  : bit;
        IS_RSTINMODE_INVERTED : bit;
        IS_RSTM_INVERTED : bit;
        IS_RSTP_INVERTED : bit
        );
  port(
        ACOUT                   : out std_logic_vector(29 downto 0);
        BCOUT                   : out std_logic_vector(17 downto 0);
        CARRYCASCOUT            : out std_ulogic;
        CARRYOUT                : out std_logic_vector(3 downto 0);
        MULTSIGNOUT             : out std_ulogic;
        OVERFLOW                : out std_ulogic;
        P                       : out std_logic_vector(47 downto 0);
        PATTERNBDETECT          : out std_ulogic;
        PATTERNDETECT           : out std_ulogic;
        PCOUT                   : out std_logic_vector(47 downto 0);
        XOROUT                  : out std_logic_vector(7 downto 0);
        UNDERFLOW               : out std_ulogic;
        A                       : in  std_logic_vector(29 downto 0);
        ACIN                    : in  std_logic_vector(29 downto 0);
        ALUMODE                 : in  std_logic_vector(3 downto 0);
        B                       : in  std_logic_vector(17 downto 0);
        BCIN                    : in  std_logic_vector(17 downto 0);
        C                       : in  std_logic_vector(47 downto 0);
        CARRYCASCIN             : in  std_ulogic;
        CARRYIN                 : in  std_ulogic;
        CARRYINSEL              : in  std_logic_vector(2 downto 0);
        CEA1                    : in  std_ulogic;
        CEA2                    : in  std_ulogic;
        CEAD                    : in  std_ulogic;
        CEALUMODE               : in  std_ulogic;
        CEB1                    : in  std_ulogic;
        CEB2                    : in  std_ulogic;
        CEC                     : in  std_ulogic;
        CECARRYIN               : in  std_ulogic;
        CECTRL                  : in  std_ulogic;
        CED                     : in  std_ulogic;
        CEINMODE                : in  std_ulogic;
        CEM                     : in  std_ulogic;
        CEP                     : in  std_ulogic;
        CLK                     : in  std_ulogic;
        D                       : in  std_logic_vector(26 downto 0);
        INMODE                  : in  std_logic_vector(4 downto 0);
        MULTSIGNIN              : in  std_ulogic;
        OPMODE                  : in  std_logic_vector(8 downto 0);
        PCIN                    : in  std_logic_vector(47 downto 0);
        RSTA                    : in  std_ulogic;
        RSTALLCARRYIN           : in  std_ulogic;
        RSTALUMODE              : in  std_ulogic;
        RSTB                    : in  std_ulogic;
        RSTC                    : in  std_ulogic;
        RSTCTRL                 : in  std_ulogic;
        RSTD                    : in  std_ulogic;
        RSTINMODE               : in  std_ulogic;
        RSTM                    : in  std_ulogic;
        RSTP                    : in  std_ulogic
      );
   end component;
  signal internal_cea1: std_logic;
  signal internal_cea2: std_logic;
  signal internal_cead: std_logic;
  signal internal_ceb1: std_logic;
  signal internal_ceb2: std_logic;
  signal internal_cec:  std_logic;
  signal internal_cep:  std_logic;
  signal internal_cem:  std_logic;
  signal internal_cecarryin: std_logic;
  signal internal_cectrl:    std_logic;
  signal internal_ced:       std_logic;
  signal internal_ceinmode:  std_logic;
  signal internal_rsta : std_logic;
  signal internal_rstb : std_logic;
  signal internal_rstc : std_logic;
  signal internal_rstalumode : std_logic;
  signal internal_rstcarryin : std_logic;
  signal internal_rstctrl : std_logic;
  signal internal_rstd :     std_logic;
  signal internal_rstinmode : std_logic;
  signal internal_rstm : std_logic;
  signal internal_cecinsub : std_logic;
  signal internal_rstp : std_logic;
  signal internal_opmode : std_logic_vector(8 downto 0);
  signal internal_alumode : std_logic_vector(3 downto 0);
  signal internal_cealumode : std_logic;
  signal internal_carryin : std_logic;
  signal internal_cemultcarryin : std_logic;
  signal internal_inmode : std_logic_vector(4 downto 0);
  signal internal_carryinsel : std_logic_vector(2 downto 0);
  signal internal_carryout : std_logic_vector(3 downto 0);

begin
  using_c_port: if (use_c_port = 1)
  generate
      generate_power_efficient_creg_enable : entity work.generatePowerEfficientEnable_dsp48e2
        generic map(
          use_reg => creg
        )
        port map(
          cereg => cec(0),
          ce => ce,
          en => en(0),
          internal_cereg => internal_cec
        );
      internal_rstc <= (rstc(0) or rst(0)) and ce;
  end generate;
  not_using_c_port: if (use_c_port = 0)
  generate
      internal_cec <= '0';
      internal_rstc <= '1';
  end generate;

  generate_power_efficient_mreg_enable : entity work.generatePowerEfficientEnable_dsp48e2
    generic map(
      use_reg => mreg
    )
    port map(
      cereg => cem(0),
      ce => ce,
      en => en(0),
      internal_cereg => internal_cem
    );
  generate_power_efficient_preg_enable : entity work.generatePowerEfficientEnable_dsp48e2
    generic map(
      use_reg => preg
    )
    port map(
      cereg => cep(0),
      ce => ce,
      en => en(0),
      internal_cereg => internal_cep
    );

  internal_cecarryin <= cecarryin(0) and ce and en(0);
  internal_cectrl <= cectrl(0) and ce and en(0);
  internal_ced <= ced(0) and ce and en(0);
  internal_cead <= cead(0) and ce and en(0);
  internal_ceinmode <= ceinmode(0) and ce and en(0);
  internal_cealumode <= cealumode(0) and ce and en(0);

  internal_rsta <= (rsta(0) or rst(0)) and ce;
  internal_rstb <= (rstb(0) or rst(0)) and ce;
  internal_rstcarryin <= (rstcarryin(0) or rst(0)) and ce;
  internal_rstctrl <= (rstctrl(0) or rst(0)) and ce;
  internal_rstd <= (rstd(0) or rst(0)) and ce;
  internal_rstinmode <= (rstinmode(0) or rst(0)) and ce;
  internal_rstalumode <= (rstalumode(0) or rst(0)) and ce;
  internal_rstm <= (rstm(0) or rst(0)) and ce;
  internal_rstp <= (rstp(0) or rst(0)) and ce;

  internal_cemultcarryin <= cemultcarryin(0) and ce and en(0);

  ceacontrol_1: if(areg = 1)
  generate
    internal_cea1 <= '1';
    internal_cea2 <= cea1(0) and ce and en(0);
  end generate;

  ceacontrol_2: if(areg = 2)
  generate
    internal_cea1 <= cea1(0) and ce and en(0);
    internal_cea2 <= cea2(0) and ce and en(0);
  end generate;

  ceacontrol_0: if(areg = 0)
  generate
    internal_cea1 <= '1';
    internal_cea2 <= '1';
  end generate;


  cebcontrol_1: if(breg = 1)
  generate
    internal_ceb1 <= '1';
    internal_ceb2 <= ceb1(0) and ce and en(0);
  end generate;

  cebcontrol_2: if(breg = 2)
  generate
    internal_ceb1 <= ceb1(0) and ce and en(0);
    internal_ceb2 <= ceb2(0) and ce and en(0);
  end generate;

  cebcontrol_0: if(breg = 0)
  generate
    internal_ceb1 <= '1';
    internal_ceb2 <= '1';
  end generate;


  opmode_0: if(use_op = 0)
  generate
        internal_opmode <= opmode;
  end generate;
  opmode_1: if(use_op = 1)
  generate
        internal_opmode <= op(8 downto 0);
  end generate;

  sub_0: if(use_op = 0)
  generate
        internal_alumode <= alumode;
  end generate;
  sub_1: if(use_op = 1)
  generate
        internal_alumode <= op(12 downto 9);
  end generate;

  carryin_0: if(use_op = 0)
  generate
        internal_carryin <= carryin(0);
  end generate;
  carryin_1: if(use_op = 1)
  generate
        internal_carryin <= op(13);
  end generate;

  carryinsel_0: if(use_op = 0)
  generate
        internal_carryinsel <= carryinsel;
  end generate;
  carryinsel_1: if(use_op = 1)
  generate
        internal_carryinsel <= op(16 downto 14);
  end generate;

  inmode_0: if(use_op  = 0)
  generate
   internal_inmode <= inmode;
  end generate;
  inmode_1: if(use_op  = 1)
  generate
   internal_inmode <= op(21 downto 17);
  end generate;


  dsp48e2_inst: DSP48E2
  generic map(
        ACASCREG       => acascreg,
        ADREG          => adreg,
        ALUMODEREG     => alumodereg,
        AREG           => areg,
        AUTORESET_PATDET => autoreset_pattern_detect,
        A_INPUT        => a_input,
        BCASCREG       => bcascreg,
        BREG           => breg,
        B_INPUT        => b_input,
        CARRYINREG     => carryinreg,
        CARRYINSELREG  => carryinselreg,
        CREG           => creg,
        DREG           => dreg,
        INMODEREG      => inmodereg,
        MASK           => mask,
        MREG           => mreg,
        OPMODEREG      => opmodereg,
        PATTERN        => pattern,
        PREG           => preg,
        SEL_MASK       => sel_mask,
        SEL_PATTERN    => sel_pattern,
        USE_MULT       => use_mult,
        USE_PATTERN_DETECT => use_pattern_detect,
        USE_SIMD       => use_simd,
        AMULTSEL       => amultsel,
        BMULTSEL       => bmultsel,
        PREADDINSEL    => preaddinsel,
        RND            => rnd,
        USE_WIDEXOR    => use_widexor,
        XORSIMD        => xorsimd,
        AUTORESET_PRIORITY  => autoreset_priority,
        IS_ALUMODE_INVERTED => is_alumode_inverted,
        IS_CARRYIN_INVERTED => is_carryin_inverted,
        IS_CLK_INVERTED => is_clk_inverted,
        IS_INMODE_INVERTED => is_inmode_inverted,
        IS_OPMODE_INVERTED => is_opmode_inverted,
        IS_RSTALLCARRYIN_INVERTED => is_rstallcarryin_inverted,
        IS_RSTALUMODE_INVERTED    => is_rstalumode_inverted,
        IS_RSTA_INVERTED => is_rsta_inverted,
        IS_RSTB_INVERTED => is_rstb_inverted,
        IS_RSTCTRL_INVERTED => is_rstctrl_inverted,
        IS_RSTC_INVERTED => is_rstc_inverted,
        IS_RSTD_INVERTED => is_rstd_inverted,
        IS_RSTINMODE_INVERTED => is_rstinmode_inverted,
        IS_RSTM_INVERTED => is_rstm_inverted,
        IS_RSTP_INVERTED => is_rstp_inverted
        )
  port map(
        ACOUT           => acout,
        BCOUT           => bcout,
        CARRYCASCOUT    => carrycascout(0),
        CARRYOUT        => internal_carryout,
        MULTSIGNOUT     => multsignout(0),
        OVERFLOW        => overflow(0),
        P               => p,
        PATTERNBDETECT  => patternbdetect(0),
        PATTERNDETECT   => patterndetect(0),
        PCOUT           => pcout,
        XOROUT          => xorout,
        UNDERFLOW       => underflow(0),
        A               => a,
        ACIN            => acin,
        ALUMODE         => internal_alumode,
        B               => b,
        BCIN            => bcin,
        C               => c,
        CARRYCASCIN     => carrycascin(0),
        CARRYIN         => internal_carryin,
        CARRYINSEL      => internal_carryinsel,
        CEA1            => internal_cea1,
        CEA2            => internal_cea2,
        CEAD            => internal_cead,
        CEALUMODE       => internal_cealumode,
        CEB1            => internal_ceb1,
        CEB2            => internal_ceb2,
        CEC             => internal_cec,
        CECARRYIN       => internal_cecarryin,
        CECTRL          => internal_cectrl,
        CED             => internal_ced,
        CEINMODE        => internal_ceinmode,
        CEM             => internal_cem,
        CEP             => internal_cep,
        CLK             => clk,
        D               => d,
        INMODE          => internal_inmode,
        MULTSIGNIN      => multsignin(0),
        OPMODE          => internal_opmode,
        PCIN            => pcin,
        RSTA            => internal_rsta,
        RSTALLCARRYIN   => internal_rstcarryin,
        RSTALUMODE      => internal_rstalumode,
        RSTB            => internal_rstb,
        RSTC            => internal_rstc,
        RSTCTRL         => internal_rstctrl,
        RSTD            => internal_rstd,
        RSTINMODE       => internal_rstinmode,
        RSTM            => internal_rstm,
        RSTP            => internal_rstp
      );

--Map Carryout port as needed
  one48_mode : if (  use_simd = "ONE48") generate
      carryout(0) <= internal_carryout(3);
  end generate;
  two24_mode : if ( use_simd = "TWO24" ) generate
   carryout(1) <= internal_carryout(3);
   carryout(0) <= internal_carryout(1);
  end generate;
  four12_mode : if ( use_simd = "FOUR12" ) generate
   carryout <= internal_carryout;
  end generate;

end behavior;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_2eda9975a6 is
  port (
    d : in std_logic_vector((48 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((48 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_2eda9975a6;
architecture behavior of sysgen_delay_2eda9975a6
is
  signal d_1_22: std_logic_vector((48 - 1) downto 0);
  signal en_1_25: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((48 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((48 - 1) downto 0) := "000000000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_8_24_en: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((48 - 1) downto 0);
  signal op_mem_0_join_10_5_en: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  en_1_25 <= en(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "000000000000000000000000000000000000000000000000";
      elsif ((ce = '1') and (op_mem_0_8_24_en = '1')) then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, en_1_25, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_0_join_10_5_rst <= '0';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_0_join_10_5_en <= '1';
    else 
      op_mem_0_join_10_5_en <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_0_8_24_en <= op_mem_0_join_10_5_en;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_13814cb9b3 is
  port (
    d : in std_logic_vector((48 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((48 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_13814cb9b3;
architecture behavior of sysgen_delay_13814cb9b3
is
  signal d_1_22: std_logic_vector((48 - 1) downto 0);
  signal en_1_25: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((48 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((48 - 1) downto 0) := "000000000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_8_24_en: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((48 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((48 - 1) downto 0) := "000000000000000000000000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_1_8_24_en: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((48 - 1) downto 0);
  signal op_mem_1_join_10_5_en: std_logic;
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((48 - 1) downto 0);
  signal op_mem_0_join_10_5_en: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  en_1_25 <= en(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "000000000000000000000000000000000000000000000000";
      elsif ((ce = '1') and (op_mem_0_8_24_en = '1')) then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "000000000000000000000000000000000000000000000000";
      elsif ((ce = '1') and (op_mem_1_8_24_en = '1')) then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_if_10_5: process (d_1_22, en_1_25, op_mem_0_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_1_join_10_5_rst <= '0';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_1_join_10_5_en <= '1';
    else 
      op_mem_1_join_10_5_en <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_0_join_10_5_rst <= '0';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_0_join_10_5_en <= '1';
    else 
      op_mem_0_join_10_5_en <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_0_8_24_en <= op_mem_0_join_10_5_en;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_1_8_24_en <= op_mem_1_join_10_5_en;
  q <= op_mem_1_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_opmode_cb47ba9e33 is
  port (
    op : out std_logic_vector((22 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_opmode_cb47ba9e33;
architecture behavior of sysgen_opmode_cb47ba9e33
is
begin
  op <= "0000000000000000000101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_8f84bb3709 is
  port (
    input_port : in std_logic_vector((30 - 1) downto 0);
    output_port : out std_logic_vector((30 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_8f84bb3709;
architecture behavior of sysgen_reinterpret_8f84bb3709
is
  signal input_port_1_40: signed((30 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port <= signed_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_f9221eebd3 is
  port (
    input_port : in std_logic_vector((18 - 1) downto 0);
    output_port : out std_logic_vector((18 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_f9221eebd3;
architecture behavior of sysgen_reinterpret_f9221eebd3
is
  signal input_port_1_40: signed((18 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port <= signed_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_c6d11883da is
  port (
    input_port : in std_logic_vector((48 - 1) downto 0);
    output_port : out std_logic_vector((48 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_c6d11883da;
architecture behavior of sysgen_reinterpret_c6d11883da
is
  signal input_port_1_40: signed((48 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port <= signed_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_counter_3f6b639172 is
  port (
    rst : in std_logic_vector((1 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_counter_3f6b639172;
architecture behavior of sysgen_counter_3f6b639172
is
  signal rst_1_40: boolean;
  signal count_reg_20_23: unsigned((1 - 1) downto 0) := "1";
  signal count_reg_20_23_rst: std_logic;
  signal bool_44_4: boolean;
  signal count_reg_join_44_1: unsigned((2 - 1) downto 0);
  signal count_reg_join_44_1_rst: std_logic;
  signal rst_limit_join_44_1: boolean;
begin
  rst_1_40 <= ((rst) = "1");
  proc_count_reg_20_23: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (count_reg_20_23_rst = '1')) then
        count_reg_20_23 <= "1";
      elsif (ce = '1') then 
        count_reg_20_23 <= count_reg_20_23 + std_logic_vector_to_unsigned("1");
      end if;
    end if;
  end process proc_count_reg_20_23;
  bool_44_4 <= rst_1_40 or false;
  proc_if_44_1: process (bool_44_4, count_reg_20_23)
  is
  begin
    if bool_44_4 then
      count_reg_join_44_1_rst <= '1';
    else 
      count_reg_join_44_1_rst <= '0';
    end if;
    if bool_44_4 then
      rst_limit_join_44_1 <= false;
    else 
      rst_limit_join_44_1 <= false;
    end if;
  end process proc_if_44_1;
  count_reg_20_23_rst <= count_reg_join_44_1_rst;
  op <= unsigned_to_std_logic_vector(count_reg_20_23);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

entity xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e is 
  port(
    a:in std_logic_vector(15 downto 0);
    b:in std_logic_vector(17 downto 0);
    ce:in std_logic;
    clk:in std_logic;
    en:in std_logic;
    p:out std_logic_vector(33 downto 0);
    rst:in std_logic
  );
end xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e; 

architecture behavior of xldsp48_macro_b7e1ad34d235e9bc5003f4ce36ab0d5e  is
  component dsp_delay_xbip_dsp48_macro_v3_0_i0
    port(
      a:in std_logic_vector(15 downto 0);
      b:in std_logic_vector(17 downto 0);
      ce:in std_logic;
      clk:in std_logic;
      p:out std_logic_vector(33 downto 0);
      sclr:in std_logic
    );
end component;
signal ce_net: std_logic := '0';
signal sclr_net: std_logic := '0';
begin
  ce_net <= en and ce;
  sclr_net <= rst and ce;
  dsp_delay_xbip_dsp48_macro_v3_0_i0_instance : dsp_delay_xbip_dsp48_macro_v3_0_i0
    port map(
      a=>a,
      b=>b,
      ce=>ce_net,
      clk=>clk,
      p=>p,
      sclr=>sclr_net
    );
end behavior;


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

entity xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2 is 
  port(
    a:in std_logic_vector(15 downto 0);
    b:in std_logic_vector(17 downto 0);
    ce:in std_logic;
    cea3:in std_logic;
    cea4:in std_logic;
    ceb3:in std_logic;
    ceb4:in std_logic;
    cem:in std_logic;
    cep:in std_logic;
    clk:in std_logic;
    p:out std_logic_vector(33 downto 0);
    rst:in std_logic
  );
end xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2; 

architecture behavior of xldsp48_macro_930c211ec669929c7a68a7638e7ff7f2  is
  component dsp_delay_xbip_dsp48_macro_v3_0_i1
    port(
      a:in std_logic_vector(15 downto 0);
      b:in std_logic_vector(17 downto 0);
      cea3:in std_logic;
      cea4:in std_logic;
      ceb3:in std_logic;
      ceb4:in std_logic;
      cem:in std_logic;
      cep:in std_logic;
      clk:in std_logic;
      p:out std_logic_vector(33 downto 0);
      sclr:in std_logic
    );
end component;
signal sclr_net: std_logic := '0';
begin
  sclr_net <= rst and ce;
  dsp_delay_xbip_dsp48_macro_v3_0_i1_instance : dsp_delay_xbip_dsp48_macro_v3_0_i1
    port map(
      a=>a,
      b=>b,
      cea3=>cea3,
      cea4=>cea4,
      ceb3=>ceb3,
      ceb4=>ceb4,
      cem=>cem,
      cep=>cep,
      clk=>clk,
      p=>p,
      sclr=>sclr_net
    );
end behavior;



