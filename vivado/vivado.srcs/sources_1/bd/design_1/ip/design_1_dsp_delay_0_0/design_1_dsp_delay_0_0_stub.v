// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Wed Jun 12 10:17:37 2019
// Host        : PCBE15327 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/work/dsp-delay/vivado/vivado.srcs/sources_1/bd/design_1/ip/design_1_dsp_delay_0_0/design_1_dsp_delay_0_0_stub.v
// Design      : design_1_dsp_delay_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku040-ffva1156-1-i
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "dsp_delay,Vivado 2018.2" *)
module design_1_dsp_delay_0_0(data_i, rst_i, clk, data_direct_globalen_o, 
  data_direct_indven_o, data_macro_globalen_o, data_macro_indven_o)
/* synthesis syn_black_box black_box_pad_pin="data_i[29:0],rst_i[0:0],clk,data_direct_globalen_o[47:0],data_direct_indven_o[47:0],data_macro_globalen_o[33:0],data_macro_indven_o[33:0]" */;
  input [29:0]data_i;
  input [0:0]rst_i;
  input clk;
  output [47:0]data_direct_globalen_o;
  output [47:0]data_direct_indven_o;
  output [33:0]data_macro_globalen_o;
  output [33:0]data_macro_indven_o;
endmodule
