-- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: CERN:SPSCavityLoops:dsp_delay:0.1
-- IP Revision: 191239416

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY xil_defaultlib;
USE xil_defaultlib.dsp_delay;

ENTITY design_1_dsp_delay_0_0 IS
  PORT (
    data_i : IN STD_LOGIC_VECTOR(29 DOWNTO 0);
    rst_i : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    clk : IN STD_LOGIC;
    data_direct_globalen_o : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    data_direct_indven_o : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    data_macro_globalen_o : OUT STD_LOGIC_VECTOR(33 DOWNTO 0);
    data_macro_indven_o : OUT STD_LOGIC_VECTOR(33 DOWNTO 0)
  );
END design_1_dsp_delay_0_0;

ARCHITECTURE design_1_dsp_delay_0_0_arch OF design_1_dsp_delay_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_dsp_delay_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT dsp_delay IS
    PORT (
      data_i : IN STD_LOGIC_VECTOR(29 DOWNTO 0);
      rst_i : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      clk : IN STD_LOGIC;
      data_direct_globalen_o : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      data_direct_indven_o : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      data_macro_globalen_o : OUT STD_LOGIC_VECTOR(33 DOWNTO 0);
      data_macro_indven_o : OUT STD_LOGIC_VECTOR(33 DOWNTO 0)
    );
  END COMPONENT dsp_delay;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_dsp_delay_0_0_arch: ARCHITECTURE IS "dsp_delay,Vivado 2018.2";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_dsp_delay_0_0_arch : ARCHITECTURE IS "design_1_dsp_delay_0_0,dsp_delay,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF design_1_dsp_delay_0_0_arch: ARCHITECTURE IS "sysgen";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF data_macro_indven_o: SIGNAL IS "XIL_INTERFACENAME data_macro_indven_o, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF data_macro_indven_o: SIGNAL IS "xilinx.com:signal:data:1.0 data_macro_indven_o DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF data_macro_globalen_o: SIGNAL IS "XIL_INTERFACENAME data_macro_globalen_o, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF data_macro_globalen_o: SIGNAL IS "xilinx.com:signal:data:1.0 data_macro_globalen_o DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF data_direct_indven_o: SIGNAL IS "XIL_INTERFACENAME data_direct_indven_o, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF data_direct_indven_o: SIGNAL IS "xilinx.com:signal:data:1.0 data_direct_indven_o DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF data_direct_globalen_o: SIGNAL IS "XIL_INTERFACENAME data_direct_globalen_o, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF data_direct_globalen_o: SIGNAL IS "xilinx.com:signal:data:1.0 data_direct_globalen_o DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clk: SIGNAL IS "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF data_direct_globalen_o:data_direct_indven_o:data_i:data_macro_globalen_o:data_macro_indven_o, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_clk_0";
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF rst_i: SIGNAL IS "XIL_INTERFACENAME rst_i, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} " & 
"value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}";
  ATTRIBUTE X_INTERFACE_INFO OF rst_i: SIGNAL IS "xilinx.com:signal:data:1.0 rst_i DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF data_i: SIGNAL IS "XIL_INTERFACENAME data_i, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF data_i: SIGNAL IS "xilinx.com:signal:data:1.0 data_i DATA";
BEGIN
  U0 : dsp_delay
    PORT MAP (
      data_i => data_i,
      rst_i => rst_i,
      clk => clk,
      data_direct_globalen_o => data_direct_globalen_o,
      data_direct_indven_o => data_direct_indven_o,
      data_macro_globalen_o => data_macro_globalen_o,
      data_macro_indven_o => data_macro_indven_o
    );
END design_1_dsp_delay_0_0_arch;
