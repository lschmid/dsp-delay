--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Wed Jun 12 10:16:41 2019
--Host        : PCBE15327 running 64-bit major release  (build 9200)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    rst_i : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    data_i : in STD_LOGIC_VECTOR ( 29 downto 0 );
    clk : in STD_LOGIC;
    data_direct_globalen_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_direct_indven_o : out STD_LOGIC_VECTOR ( 47 downto 0 );
    data_macro_globalen_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    data_macro_indven_o : out STD_LOGIC_VECTOR ( 33 downto 0 );
    rst_i : in STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      clk => clk,
      data_direct_globalen_o(47 downto 0) => data_direct_globalen_o(47 downto 0),
      data_direct_indven_o(47 downto 0) => data_direct_indven_o(47 downto 0),
      data_i(29 downto 0) => data_i(29 downto 0),
      data_macro_globalen_o(33 downto 0) => data_macro_globalen_o(33 downto 0),
      data_macro_indven_o(33 downto 0) => data_macro_indven_o(33 downto 0),
      rst_i => rst_i
    );
end STRUCTURE;
