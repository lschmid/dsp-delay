restart
add_force {/design_1_wrapper/clk} -radix bin {1 0ns} {0 4000ps} -repeat_every 8000ps
add_force {/design_1_wrapper/rst_i} -radix bin {1 0ns}
add_force {/design_1_wrapper/data_i} -radix dec {0 0ns}
run 24ns
add_force {/design_1_wrapper/rst_i} -radix bin {0 0ns}
run 56ns
add_force {/design_1_wrapper/data_i} -radix hex {0x04000000 0ns}
run 16ns
add_force {/design_1_wrapper/data_i} -radix dec {0 0ns}
run 704ns
